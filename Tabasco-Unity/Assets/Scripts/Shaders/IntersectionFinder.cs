﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntersectionFinder : MonoBehaviour
{
    public GameObject intersectingObject;
    public Renderer meshRenderer;
    public Material instancedMaterial;

    public Vector4 IntersectObjectPosition = new Vector4(0f,0f,0f,1);



    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = gameObject.GetComponent<Renderer>();
        //instancedMaterial = meshRenderer.material; //Gets the material from the current object the script is applied to if active
    }

    // Update is called once per frame
    void Update()
    {
        IntersectObjectPosition = intersectingObject.transform.position;
        instancedMaterial.SetVector("_IntersectPosition", IntersectObjectPosition);
    }
}
