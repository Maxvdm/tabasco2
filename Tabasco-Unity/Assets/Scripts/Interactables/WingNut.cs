﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingNut : MonoBehaviour
{
    public float screwValue;
    public float minimumScrewValue;
    public float maximumScrewValue = 360f;
    public Animator gateAnimator;
    private Transform _transform;
    private Quaternion _previousFrameRotation;

    private void Start()
    {
        _transform = transform;
    }

    private void Update()
    {
        screwValue += Mathf.Rad2Deg * 2f * (Quaternion.Inverse(_transform.rotation) * _previousFrameRotation).y;
        _previousFrameRotation = _transform.rotation;
        gateAnimator.Play(0,0, Mathf.Clamp01(TabascoMaths.RemapFloat(screwValue,minimumScrewValue,maximumScrewValue,0f,1f)));

    }

    private void Fixedupdate()
    {
        
    }
}
