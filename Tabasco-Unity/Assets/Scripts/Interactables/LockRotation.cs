﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockRotation : MonoBehaviour
{
    private Quaternion _rotation;
    private Transform _transform;
    
    void Awake()
    {
        _transform = transform;
        _rotation = _transform.rotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.rotation = _rotation;
    }
}
