﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockRigidbodyPosition : MonoBehaviour
{
    public PlayerCharacter playerCharacter;
    public bool lockPosition = true;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player Trigger")
        {
            playerCharacter.ConstrainMovementToXyPlane(lockPosition);
        }
    }
}