﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LineRope : MonoBehaviour
{
    public Transform anchor;
    private LineRenderer _lineRenderer;
    private Transform _transform;
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.positionCount = 2;
        _transform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        _lineRenderer.SetPosition(0, _transform.position);
        
        _lineRenderer.SetPosition(1, anchor.position);
    }
}
