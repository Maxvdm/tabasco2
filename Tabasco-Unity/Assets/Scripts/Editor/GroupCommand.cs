﻿using UnityEditor;
using UnityEngine;

public static class GroupCommand
{
    [MenuItem("GameObject/Group Selected %g")]
    private static void GroupSelected()
    {
        if (!Selection.activeTransform)
        {
            return;
        }

        var groupGameObject = new GameObject(Selection.activeTransform.name + " Group");

        groupGameObject.transform.position = CalculateMidpoint(Selection.transforms);

        Undo.RegisterCreatedObjectUndo(groupGameObject, "Group Selected");
        groupGameObject.transform.SetParent(Selection.activeTransform.parent, false);

        foreach (var t in Selection.transforms)
        {
            Undo.SetTransformParent(t, groupGameObject.transform, "Group Selected");
        }

        Selection.activeGameObject = groupGameObject;
    }

    private static Vector3 CalculateMidpoint(Transform[] positions)
    {
        var addition = Vector3.zero;
        foreach (var t in positions)
        {
            addition += t.position;
        }
        var result = addition / positions.Length;
        return result;
    }
}