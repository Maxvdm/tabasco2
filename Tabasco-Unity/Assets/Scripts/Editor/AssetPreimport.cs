﻿using UnityEngine;
using UnityEditor;

// Disable import of materials if the file contains
// the @ sign marking it as an animation.
public class AssetPreimport : AssetPostprocessor
{
    void OnPreprocessModel()
    {
        if (assetPath.Contains(".fbx") || assetPath.Contains(".blend"))
        {
            ModelImporter modelImporter = assetImporter as ModelImporter;
            if (modelImporter != null)
            {
                modelImporter.generateSecondaryUV = true;
                modelImporter.importLights = false;
                modelImporter.importCameras = false;
            }
        }
    }
}