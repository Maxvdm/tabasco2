﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(TerrainTemplate))]
[CanEditMultipleObjects]
public class TerrainTemplateEditor : Editor
{
    private Material _material = null;

    public override void OnInspectorGUI()
    {
        
        var terrainTemplate = (TerrainTemplate) target;
        if (_material == null) _material = terrainTemplate.gameObject.GetComponent<MeshRenderer>().sharedMaterial;

        terrainTemplate.softness = _material.GetFloat("_Softness");
        DrawDefaultInspector();
        
        
        
        _material.SetFloat("_Softness", terrainTemplate.softness);
        
        if (GUILayout.Button("Make Additive")) 
        {
            terrainTemplate.SetAsAdditive();
        }
        if (GUILayout.Button("Make Subtractive")) 
        {
            
            terrainTemplate.SetAsSubtractive();
        }
        if (GUILayout.Button("Reinitialise (careful with this one)"))
        {
            terrainTemplate.Init();
        } 
}

}
