﻿
using UnityEngine;
using UnityEditor;
using System.Collections;
using MogulTech.Utilities;

public class TerrainTemplateWindow : EditorWindow
{
    public bool templateVisibility = true;
    [MenuItem ("Tabasco/Terrain Templates")]
    public static void  ShowWindow () {
        EditorWindow.GetWindow(typeof(TerrainTemplateWindow));
    }
    
    void OnGUI () {
        //GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
        
        if (GUILayout.Button("Project To Terrain"))
        {
            SetTemplateVisibility(true);
            var templateArray = Resources.FindObjectsOfTypeAll(typeof(MeshToTerrainProjector)) as MeshToTerrainProjector[];
            if (templateArray != null)
                foreach (var item in templateArray)
                {
                    EditorCoroutines.Execute(item.ProjectTerrain(templateVisibility));
                }
        }        
        
        if (GUILayout.Button("Update Terrain"))
        {
            SetTemplateVisibility(true);
            var templateArray = Resources.FindObjectsOfTypeAll(typeof(MeshToTerrainProjector)) as MeshToTerrainProjector[];
            if (templateArray != null)
                foreach (var item in templateArray)
                {
                    EditorCoroutines.Execute(item.UpdateTerrain(templateVisibility));
                }
        }       
        
        if (GUILayout.Button("Reveal Templates"))
        {
            templateVisibility = true;
            SetTemplateVisibility(true);
        }         
        if (GUILayout.Button("Hide Templates"))
        {
            
            templateVisibility = false;
            SetTemplateVisibility(false);
        }

        if (GUILayout.Button("Convert Selected Objects to Terrain Templates"))
        {
            TerrainTemplateTools.ConvertSelectedObjectsToTerrainTemplates();
        }
    }
    
    public void SetTemplateVisibility(bool value)
    {
        var templateArray = Resources.FindObjectsOfTypeAll(typeof(TerrainTemplate)) as TerrainTemplate[];
        if (templateArray != null)
            foreach (var item in templateArray)
            {
                item.gameObject.SetActive(value);
            }
    }
}