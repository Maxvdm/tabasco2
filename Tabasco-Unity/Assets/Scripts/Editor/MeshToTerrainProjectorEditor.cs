﻿
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using MogulTech.Utilities;

[CustomEditor(typeof(MeshToTerrainProjector))]
[CanEditMultipleObjects]
public class MeshToTerrainProjectorEditor : Editor
{
    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();
        
        var meshToTerrainProjector = (MeshToTerrainProjector) target;
        if (GUILayout.Button("Project To Terrain"))
        {
            EditorCoroutines.Execute(meshToTerrainProjector.ProjectTerrain(true));
        }

    }
}