﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class NPCMovementBallRolling : MonoBehaviour
{

    public float forwardMovement = 0f;
    public float leftMovement = 0f;
    public float rightMovement = 0f;
    private Transform _transform;
    public Vector3 facingDirection = Vector3.forward;

    private Rigidbody _rigidbody;

    // Start is called before the first frame update
    private void Start()
    {
        _transform = transform;
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _rigidbody.AddForce(facingDirection * forwardMovement, ForceMode.Force);

        facingDirection = Quaternion.AngleAxis(-leftMovement, Vector3.up) * facingDirection;
        facingDirection = Quaternion.AngleAxis(rightMovement, Vector3.up) * facingDirection;
    }
}
