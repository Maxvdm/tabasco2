﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PhysicalNavMeshAgentConnector : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;
    private Rigidbody _rigidbody;
    private Transform _transform;
    // Start is called before the first frame update
    void Start()
    {
        _transform = transform;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _rigidbody = GetComponent<Rigidbody>();
        _navMeshAgent.updatePosition = false;
    }

    private void OnCollisionEnter(Collision other)
    {
//        _rigidbody.isKinematic = false;
//        _navMeshAgent.enabled = false;
    }
}
