﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShovelMovement : MonoBehaviour
{
   	public float playerTargetDistance = 100f;
	public float enemyLookDistance = 100f;
	public float attackDistance = 100f;
	public float enemyMovementSpeed = 100f;
	public float damping = 10f;
	public Transform playerTarget;
	private Transform _transform;
	private Rigidbody _enemyRigidbody;
	private Renderer _renderer;

	// Use this for initialization
	private void Start () {
		_renderer = GetComponent<Renderer>();
		_enemyRigidbody = GetComponent<Rigidbody>();
		_transform = transform;
	}
	
	// Update is called once per frame
	private void Update () {
		playerTargetDistance = Vector3.Distance(playerTarget.position, _transform.position);

		if(playerTargetDistance < enemyLookDistance) {
			_renderer.material.color = Color.yellow;
			LookAtPlayer();
		}

		if(playerTargetDistance<attackDistance)
		{
			_renderer.material.color = Color.red;
			MoveTowardsPlayer();
		}

		if(playerTargetDistance > enemyLookDistance)
		{
			_renderer.material.color = Color.white;
		}
		
	}

	private void LookAtPlayer()
	{
		var rotation = Quaternion.LookRotation(playerTarget.position-_transform.position);
		_transform.rotation = Quaternion.Slerp (_transform.rotation, rotation, Time.deltaTime*damping);
	}

	private void MoveTowardsPlayer()
	{
		_enemyRigidbody.AddForce(_transform.forward * enemyMovementSpeed);
	}
}
