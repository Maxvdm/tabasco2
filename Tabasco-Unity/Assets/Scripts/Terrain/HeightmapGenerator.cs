﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RSG;
using UnityEngine.Experimental.TerrainAPI;

#if UNITY_EDITOR
public static class HeightmapGenerator
{
    static HeightmapGenerator()
    {
        
    }
    public static IEnumerator GenerateHeightmapFromSelectedGeometry(string path)
    {
        var cameraObject = new GameObject("DepthCamera");
        cameraObject.transform.position = new Vector3(0f, 300f, 0f);
        cameraObject.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        var camera = cameraObject.AddComponent<Camera>();
        camera.orthographic = true;
        camera.orthographicSize = 50f;

        var renderTexture = new RenderTexture(1024, 1024, 32, RenderTextureFormat.DefaultHDR);

        camera.targetTexture = renderTexture;
        camera.SetReplacementShader(Shader.Find("Utility/WorldHeight"),"");

        for (int i = 0; i < 100; i++)
        {
            yield return new WaitForSeconds(1f);
        }
        
        var texture = new Texture2D(1024, 1024, TextureFormat.RGBAFloat, false);
        
        RenderTexture.active = renderTexture;
        texture.ReadPixels(new Rect(0, 0, 1024, 1024), 0, 0);
        
        for (int i = 0; i < 100; i++)
        {
            yield return new WaitForSeconds(1f);
        }

        texture.Apply();
        RenderTexture.active = null;
        
        
        var bytes = texture.EncodeToEXR();
            
        System.IO.File.WriteAllBytes(path, bytes);
        AssetDatabase.ImportAsset(path);
        Debug.Log("Saved to " + path);

        Object.DestroyImmediate(cameraObject);
        Object.DestroyImmediate(camera);
        Object.DestroyImmediate(renderTexture);
        Object.DestroyImmediate(texture);
        
        
    }
}
#endif
