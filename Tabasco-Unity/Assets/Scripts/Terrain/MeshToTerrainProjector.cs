﻿using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

using MogulTech.Utilities;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Experimental.TerrainAPI;

[ExecuteInEditMode]
[RequireComponent(typeof(Terrain))]
public class MeshToTerrainProjector : MonoBehaviour
{
    public float blurRadius = 1f;
    private Material _blurMaterial;
    private Material _templateMaterial;
    private Terrain _terrain;
    private TerrainData _terrainData;
    private GameObject _depthRenderCamera;
    private GameObject _subtractiveDepthRenderCamera;
    
    public IEnumerator ProjectTerrain(bool templateVisibility)
         {
             InitializeTerrainProjector();
             
             var additiveHeightRendertexture = GetAdditiveHeightRenderTexture();
     
             var subtractiveHeightRendertexture = GetSubtractiveHeightRenderTexture();
     
             var finalHeightRenderTexture = CombineAdditiveAndSubtractiveRenderTextures(additiveHeightRendertexture,
                 subtractiveHeightRendertexture, _blurMaterial);
             
             yield return new WaitForSeconds(1f);
     
             finalHeightRenderTexture = BlitBlurRenderTexture(finalHeightRenderTexture, _blurMaterial, blurRadius);
     
             yield return new WaitForSeconds(1f);
             
             var heightTexture2D = ConvertRenderTextureToTexture2D(finalHeightRenderTexture);
             
             yield return new WaitForSeconds(1f);
             
             ApplyHeightToTerrain(heightTexture2D);
             
             DestroyImmediate(_depthRenderCamera);
             DestroyImmediate(_subtractiveDepthRenderCamera);
             
             SetTemplateVisibility(templateVisibility);
         }
    
    public IEnumerator UpdateTerrain(bool templateVisibility)
    {
        InitializeTerrainProjector();

        var existingHeightRendertexture = _terrainData.heightmapTexture;

        System.IO.File.WriteAllBytes("Assets/SavedScreen.png",ConvertRenderTextureToTexture2D(existingHeightRendertexture).EncodeToEXR());
             
        var additiveHeightRendertexture = GetAdditiveHeightRenderTexture();
     
        var combinedBaseRenderTexture = CombineAdditiveAndTerrainRenderTextures(additiveHeightRendertexture,
            existingHeightRendertexture, _blurMaterial);
        
        var subtractiveHeightRendertexture = GetSubtractiveHeightRenderTexture();

        var finalHeightRenderTexture = CombineAdditiveAndSubtractiveRenderTextures(combinedBaseRenderTexture,
            subtractiveHeightRendertexture, _blurMaterial);
             
        yield return new WaitForSeconds(1f);
     
        finalHeightRenderTexture = BlitBlurRenderTexture(finalHeightRenderTexture, _blurMaterial, blurRadius);
     
        yield return new WaitForSeconds(1f);
             
        var heightTexture2D = ConvertRenderTextureToTexture2D(finalHeightRenderTexture);
             
        yield return new WaitForSeconds(1f);
             
        ApplyHeightToTerrain(heightTexture2D);
             
        DestroyImmediate(_depthRenderCamera);
        DestroyImmediate(_subtractiveDepthRenderCamera);
             
        SetTemplateVisibility(templateVisibility);
    }


    private void InitializeTerrainProjector()
    {
        _terrain = gameObject.GetComponent<Terrain>();
        _terrainData = _terrain.terrainData;
        _blurMaterial = new Material(Shader.Find("Utility/Blur"));
    }

    private RenderTexture GetAdditiveHeightRenderTexture()
    {
        
        _depthRenderCamera = new GameObject("DepthCamera");
        
        _depthRenderCamera.transform.position = new Vector3(_terrain.GetPosition().x + (_terrainData.size.x / 2f), _terrainData.size.y + _terrain.GetPosition().y, _terrain.GetPosition().z + (_terrainData.size.z / 2f));
        _depthRenderCamera.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        var cameraComponent = _depthRenderCamera.AddComponent<Camera>();
        cameraComponent.orthographic = true;
        cameraComponent.orthographicSize = _terrainData.size.x/2f;
        cameraComponent.farClipPlane = _terrainData.size.y + 1f;
        cameraComponent.cullingMask = LayerMask.GetMask("Terrain Template");
        cameraComponent.clearFlags = CameraClearFlags.Color;
        cameraComponent.backgroundColor = new Color(_terrain.GetPosition().y,0.5f,0f,0f);
        
        var renderTexture = RenderTexture.GetTemporary(_terrainData.heightmapResolution, _terrainData.heightmapResolution, 32, RenderTextureFormat.DefaultHDR);

        cameraComponent.SetReplacementShader(Shader.Find("Utility/WorldHeight"),"TerrainTemplate");
        cameraComponent.targetTexture = renderTexture;
        cameraComponent.Render();
        
        return renderTexture;
    }
    
    private RenderTexture GetSubtractiveHeightRenderTexture()
    {
        
        _subtractiveDepthRenderCamera = new GameObject("SubtractiveDepthCamera");
        
        _subtractiveDepthRenderCamera.transform.position = new Vector3(_terrain.GetPosition().x + (_terrainData.size.x / 2f), _terrain.GetPosition().y, _terrain.GetPosition().z + (_terrainData.size.z / 2f));
        _subtractiveDepthRenderCamera.transform.rotation = Quaternion.Euler(-90f, 0f, 180);
        var cameraComponent = _subtractiveDepthRenderCamera.AddComponent<Camera>();
        cameraComponent.orthographic = true;
        cameraComponent.orthographicSize = _terrainData.size.x/2f;
        cameraComponent.farClipPlane = _terrainData.size.y + 1f;
        cameraComponent.cullingMask = LayerMask.GetMask("Terrain Template Subtractive");
        cameraComponent.clearFlags = CameraClearFlags.Color;
        cameraComponent.backgroundColor = new Color(10000f,10000f,10000f,10000f);
        
        var renderTexture = RenderTexture.GetTemporary(_terrainData.heightmapResolution, _terrainData.heightmapResolution, 32, RenderTextureFormat.ARGBFloat);

        cameraComponent.SetReplacementShader(Shader.Find("Utility/WorldHeight"),"TerrainTemplate");
        cameraComponent.targetTexture = renderTexture;
        cameraComponent.Render();
        
        return renderTexture;
    }
    
    

    private static Texture2D ConvertRenderTextureToTexture2D(RenderTexture inputRenderTexture)
    {
        var texture = new Texture2D(inputRenderTexture.width, inputRenderTexture.width, TextureFormat.RFloat, false);
        RenderTexture.active = inputRenderTexture;
        texture.ReadPixels(new Rect(0, 0, inputRenderTexture.width, inputRenderTexture.height), 0, 0);
        texture.Apply();
        RenderTexture.active = null;
        return texture;

    }

    private void ApplyHeightToTerrain(Texture2D inputTexture)
    {
        
        var heightArray = new float [inputTexture.height, inputTexture.width];
        for (var y = 0; y < inputTexture.height; y++)
        {
            for (var x = 0; x < inputTexture.width; x++)
            {
                heightArray[y, x] = (inputTexture.GetPixel(x, y).r - _terrain.GetPosition().y) / _terrainData.size.y;
            }
        }
        _terrainData.SetHeights(0,0, heightArray);
    }
    
    private void ApplyAlphaToTerrain(Texture2D inputTexture)
    {
        float[,,] alphaArray = new float[_terrainData.alphamapWidth, _terrainData.alphamapHeight, 2];
        for (var y = 0; y < inputTexture.height; y++)
        {
            for (var x = 0; x < inputTexture.width; x++)
            {
                alphaArray[y, x, 0] = inputTexture.GetPixel(x, y).b;
                alphaArray[y, x, 0] = 1 - inputTexture.GetPixel(x, y).b;
                alphaArray[y, x, 0] = 1 - inputTexture.GetPixel(x, y).b;
                alphaArray[y, x, 0] = 1 - inputTexture.GetPixel(x, y).b;
            }
        }
        _terrainData.SetAlphamaps(0,0, alphaArray);
    }

    private RenderTexture BlitBlurRenderTexture(RenderTexture inputTexture, Material material, float blurRadius)
    {
        RenderTexture blur0 = RenderTexture.GetTemporary(inputTexture.width, inputTexture.height, 0, RenderTextureFormat.ARGBFloat);
        RenderTexture blur1 = RenderTexture.GetTemporary(inputTexture.width, inputTexture.height, 0, RenderTextureFormat.ARGBFloat);
        
        _blurMaterial.SetFloat("_BlurRadius", blurRadius);
        var outputRenderTexture = new RenderTexture(inputTexture.width,inputTexture.height,32,RenderTextureFormat.ARGBFloat);
        Graphics.Blit(inputTexture,blur0, material, 0);
        Graphics.Blit(blur0,blur1, material,0);
        Graphics.Blit(blur1,outputRenderTexture, material,0);
        return outputRenderTexture;
    }

    private RenderTexture CombineAdditiveAndSubtractiveRenderTextures(RenderTexture additiveInputTexture, RenderTexture subtractiveInputTexture, Material material)
    {
        var outputRenderTexture = RenderTexture.GetTemporary(additiveInputTexture.width,additiveInputTexture.height,32,RenderTextureFormat.DefaultHDR);
        _blurMaterial.SetTexture("_SubtractiveTexture", subtractiveInputTexture);
        Graphics.Blit(additiveInputTexture,outputRenderTexture, material, 1);
        return outputRenderTexture;
    }    
    
    private RenderTexture CombineAdditiveAndTerrainRenderTextures(RenderTexture additiveInputTexture, RenderTexture terrainInputTexture, Material material)
    {
        var outputRenderTexture = RenderTexture.GetTemporary(additiveInputTexture.width,additiveInputTexture.height,32,RenderTextureFormat.DefaultHDR);
        _blurMaterial.SetTexture("_TerrainTexture", terrainInputTexture);
        Graphics.Blit(additiveInputTexture,outputRenderTexture, material, 2);
        return outputRenderTexture;
    }
    
    public void SetTemplateVisibility(bool value)
    {
       
        var templateArray = Resources.FindObjectsOfTypeAll(typeof(TerrainTemplate)) as TerrainTemplate[];
        if (templateArray != null)
            foreach (var item in templateArray)
            {
                item.gameObject.SetActive(value);
            }
    }
    
    public void SetTemplateMaterial()
    {
        _templateMaterial = new Material(Shader.Find("Utility/TerrainTemplatePreview"));
    }



    private void Update()
    {
        //EditorCoroutines.Execute(ProjectTerrain());
    }
}
