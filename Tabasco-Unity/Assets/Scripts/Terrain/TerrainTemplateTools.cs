﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
public class TerrainTemplateTools
{
    public static void ConvertSelectedObjectsToTerrainTemplates()
    {
        var selectedGameObjects = Selection.gameObjects;
        var layerMask = LayerMask.NameToLayer("Terrain Template");
        
        foreach (var item in selectedGameObjects)
        {
            TerrainTemplate terrainTemplate;
            
            if (item.GetComponent<TerrainTemplate>() != null) continue;
            terrainTemplate = item.AddComponent<TerrainTemplate>();
            terrainTemplate.Init();
        }
        
    }
}
#endif
