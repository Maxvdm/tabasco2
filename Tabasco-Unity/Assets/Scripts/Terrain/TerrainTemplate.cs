﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTemplate : MonoBehaviour
{
    private Mesh _mesh;
    private Transform _transform;
    
    [Range(0f,1f)]
    public float softness;

    
    
//    private void OnDrawGizmosSelected()
//    {
//        if (_transform == null) _transform = transform;
//        if (gameObject.GetComponent<MeshFilter>() !=null && _mesh == null) _mesh = gameObject.GetComponent<MeshFilter>().sharedMesh;
//        Gizmos.DrawMesh(_mesh,_transform.position,_transform.rotation,_transform.lossyScale);
//    }

/*    public void SetBlurValue()
    {
        
        var mesh = GetComponent<MeshFilter>().sharedMesh;
        var vertices = mesh.vertices;

        var colors = new Color[vertices.Length];

        for (var i = 0; i < vertices.Length; i++)
            colors[i] = new Color(blurAmount, 0, 0,1f);

        mesh.colors = colors;
    }*/

    public void Init()
    {
        if (transform.gameObject.GetComponent<MeshRenderer>() != null)
        {
            transform.gameObject.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Utility/TerrainTemplatePreview"));
        }

        transform.tag = "EditorOnly";
        SetAsAdditive();
    }

    public void SetAsAdditive()
    {
        var layer = LayerMask.NameToLayer("Terrain Template");
        transform.gameObject.layer = layer;
        transform.gameObject.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Subtractive", 0f);
    }

    public void SetAsSubtractive()
    {
        var layer = LayerMask.NameToLayer("Terrain Template Subtractive");
        transform.gameObject.layer = layer;
        transform.gameObject.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Subtractive", 1f);
    }
    
    
}
