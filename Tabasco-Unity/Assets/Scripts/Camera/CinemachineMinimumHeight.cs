﻿using System.Globalization;
using UnityEngine;
using Cinemachine;
using Springs;

[AddComponentMenu("")] // Hide in menu
[SaveDuringPlay]
#if UNITY_2018_3_OR_NEWER
[ExecuteAlways]
#else
[ExecuteInEditMode]
#endif
public class CinemachineMinimumHeight : CinemachineExtension
{
    public LayerMask cameraLayerMask;
    private RaycastHit[] _rayCastArray = new RaycastHit[4];
    
    public float targetCameraHeight = 1;
    private SpringDamper _springdamper;
    private SpringDamperParameters _springDamperParameters;
    public float highestRaycastHitPosition;
    public string[] raycastColliderName = new string[6];
    public string[] raycastColliderHeight = new string[6];


    private void OnValidate()
    {
        //minimumCameraHeight = Mathf.Max(0, minimumCameraHeight);
    }
 
    protected override void PostPipelineStageCallback(
        CinemachineVirtualCameraBase virtualCameraBase,
        CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (stage != CinemachineCore.Stage.Body) return;
        
        var cameraPosition = state.CorrectedPosition;
        var rayCastHits = Physics.RaycastNonAlloc(cameraPosition + Vector3.up * 100f, Vector3.down, _rayCastArray,
            2000f, cameraLayerMask);
        if ( rayCastHits > 0)
        {                 
            //Debug.DrawRay(cameraPosition + Vector3.up * 100f, Vector3.down * 2000f);
            highestRaycastHitPosition = Mathf.NegativeInfinity;

            for (var index = 0; index < rayCastHits; index++)
            {
                var rayCastHit = _rayCastArray[index];
                if (_rayCastArray[index].collider != null)
                {
                    raycastColliderName[index] = rayCastHit.collider.name;
                    raycastColliderHeight[index] = rayCastHit.point.y.ToString(CultureInfo.CurrentCulture);

                    highestRaycastHitPosition = Mathf.Max(highestRaycastHitPosition, rayCastHit.point.y);
                }
            }
            //HighestRaycastHitPosition = Mathf.Max(HighestRaycastHitPosition, virtualCameraBase.LookAt.position.y);

            if (cameraPosition.y  < highestRaycastHitPosition + targetCameraHeight)
            {
                cameraPosition.y = highestRaycastHitPosition + targetCameraHeight;
            }
            state.PositionCorrection = cameraPosition - state.CorrectedPosition;


        }
    }
}