﻿// (c) Copyright HutongGames, LLC 2010-2018. All rights reserved.
// Author jean@hutonggames.com
// This code is licensed under the MIT Open source License

using Cinemachine;
using InControl;
using UnityEngine;

/// <summary>
/// This Component takes control of the CinemachineCore input source, and provides support for Input.Touch
///
/// This is needed if you use for example "CineMachine Free Look" component and want Touch to control the Free Look behaviour.
///
/// "Mouse X" and "Mouse Y" are implemented, if Touches are detected, they are used, else the regular Unity InputManager is used
///
/// For Touch based input, you can control sensitivity, it refers to the number of pixels of a touch deltaPosition that would equate to 1 if it was a regular Input
/// So for a TouchSensitivity of 10, this means an Input value of 1 if the deltaPosition was 10 for that given frame
///
/// For regular Mouse, there is an option to Require a click to active Input, so you click and drag to have Input values.
/// </summary>
public class CinemachineCoreInControlRightStick : MonoBehaviour {

    [Tooltip("If true, you need to click the left mouse button for Input to be working. This is not affecting Touch based Input")]
    public bool InputRequiresClick;
    [Tooltip("Touch based Input Sensitivity for the X axis. Of 10, this means an Input value of 1 if the deltaPosition was 10 pixels for that given frame")]
    public float TouchSensitivity_x = 10f;
    [Tooltip("Touch based Input Sensitivity for the Y axis. Of 10, this means an Input value of 1 if the deltaPosition was 10 pixels for that given frame")]
    public float TouchSensitivity_y = 10f;

    private InputDevice _inputDevice;

	// Use this for initialization
	void Start () {
        CinemachineCore.GetInputAxis = HandleAxisInputDelegate;
	}

    float HandleAxisInputDelegate(string axisName)
    {
        
        _inputDevice = InputManager.ActiveDevice;
        switch(axisName)
        {
            case "Right Stick X":
                return _inputDevice.RightStick.X;

            case "Right Stick Y":
                
                return _inputDevice.RightStick.Y;


            default:
                Debug.LogError("CinemachineCoreInControlRightStick : Input <"+axisName+"> not recognized.",this);
                break;
        }

        return 0f;
    }
}