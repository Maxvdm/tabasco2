﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SidewaysCamParenting : MonoBehaviour
{
    //public GameObject followThis;
    public CinemachineVirtualCamera cinemachineCam;
    private Transform _transform;

    
        // Start is called before the first frame update
    void Start()
    {
        _transform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        _transform.position = cinemachineCam.State.FinalPosition;
    }
}
