﻿using UnityEngine;
using Cinemachine;
using Springs;

[AddComponentMenu("")] // Hide in menu
[SaveDuringPlay]
#if UNITY_2018_3_OR_NEWER
[ExecuteAlways]
#else
[ExecuteInEditMode]
#endif
public class CinemachineSoftTerrainHugger : CinemachineExtension
{
    public LayerMask cameraLayerMask;
    private RaycastHit[] _raycastArray = new RaycastHit[4];
    public float upwardCameraMovementSpeed = 1f;
    public float downwardCameraMovementSpeed = 1f;
    
    public float targetCameraHeight = 1;
    public bool useInputAxis;
    public string inputAxisName = "Right Stick Y";
    public float maxCameraHeight = 10f;
    public float minCameraHeight = 1f;
    public float cameraYPosition;
    public float cameraYSpeed = 1f;
    private SpringDamper _springdamper;
    private SpringDamperParameters _springDamperParameters;
    private float _cameraHeight;

    public bool invertYAxis;
    
 
    private void OnValidate()
    {
        //minimumCameraHeight = Mathf.Max(0, minimumCameraHeight);
    }
 
    protected override void PostPipelineStageCallback(
        CinemachineVirtualCameraBase virtualCameraBase,
        CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (stage != CinemachineCore.Stage.Body) return;
        
        var cameraPosition = state.CorrectedPosition;
        
        //if (Physics.SphereCastNonAlloc(cameraPosition + Vector3.up * maximumCameraHeight, 5f, Vector3.down, _raycastArray, maximumCameraHeight * 2f, cameraLayerMask) > 0)

        if (useInputAxis)
        {
            if(invertYAxis) 
                cameraYPosition += TabascoMaths.RemapFloat(Input.GetAxis(inputAxisName), 0.1f, -0.1f, -cameraYSpeed, cameraYSpeed) * Time.deltaTime;
            else 
                cameraYPosition += TabascoMaths.RemapFloat(Input.GetAxis(inputAxisName), -0.1f, 0.1f, -cameraYSpeed, cameraYSpeed) * Time.deltaTime;

            cameraYPosition = Mathf.Clamp01(cameraYPosition);
        }

        if ( Physics.RaycastNonAlloc(cameraPosition + Vector3.up * 1000f, Vector3.down, _raycastArray, 2000f, cameraLayerMask) > 0)
        {
            var highestRaycastHitPosition = Mathf.NegativeInfinity;
            foreach (var raycastHit in _raycastArray)
            {
                highestRaycastHitPosition = Mathf.Max(highestRaycastHitPosition, raycastHit.point.y);
            }

            highestRaycastHitPosition = Mathf.Max(highestRaycastHitPosition, virtualCameraBase.LookAt.position.y);
            
            if (_cameraHeight  < highestRaycastHitPosition + (targetCameraHeight/2f))
            {
                _cameraHeight = highestRaycastHitPosition + (targetCameraHeight/2f);
            }
            else if (_cameraHeight < highestRaycastHitPosition + targetCameraHeight)
            {
                _cameraHeight += ((highestRaycastHitPosition + targetCameraHeight) - _cameraHeight) * upwardCameraMovementSpeed * Time.deltaTime;
                _cameraHeight = Mathf.Min(_cameraHeight, highestRaycastHitPosition + targetCameraHeight);
            }
            else
            {
                _cameraHeight += ((highestRaycastHitPosition + targetCameraHeight) - _cameraHeight) * downwardCameraMovementSpeed * Time.deltaTime;
                _cameraHeight = Mathf.Max(_cameraHeight, highestRaycastHitPosition + targetCameraHeight);
            }
            
            cameraPosition.y = _cameraHeight + Mathf.Lerp(minCameraHeight,maxCameraHeight,cameraYPosition);
            state.PositionCorrection += cameraPosition - state.CorrectedPosition;


        }
    }
}