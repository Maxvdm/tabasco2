﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
    

[ExecuteInEditMode]
public class RenderBlurredNormalPass : MonoBehaviour
{
    private CommandBuffer commandBuffer;
    public float blurAmount = 2f;
    public Shader blurShader;
    public float depthThreshold = 10f;

    private void Awake()
    {
        var blurMaterial = new Material(blurShader);
        commandBuffer = new CommandBuffer();
        commandBuffer.SetRenderTarget(BuiltinRenderTextureType.GBuffer2);
        
        
        var blurredID = Shader.PropertyToID("_Temp1");
        var blurredID2 = Shader.PropertyToID("_Temp2");
        commandBuffer.GetTemporaryRT (blurredID, 0, 0, 0, FilterMode.Bilinear);
        commandBuffer.GetTemporaryRT (blurredID2, 0, 0, 0, FilterMode.Bilinear);
        commandBuffer.SetGlobalFloat("depthThreshold", depthThreshold);
        
        commandBuffer.Blit (BuiltinRenderTextureType.GBuffer2, blurredID);
        // horizontal blur
        commandBuffer.SetGlobalVector("offsets", new Vector4(blurAmount/Screen.width,0,0,0));
        commandBuffer.Blit (blurredID, blurredID2, blurMaterial);
        // vertical blur
        commandBuffer.SetGlobalVector("offsets", new Vector4(0,blurAmount/Screen.height,0,0));
        commandBuffer.Blit (blurredID2, blurredID, blurMaterial);
        // horizontal blur
        commandBuffer.SetGlobalVector("offsets", new Vector4(blurAmount*2f/Screen.width,0,0,0));
        commandBuffer.Blit (blurredID, blurredID2, blurMaterial);
        // vertical blur
        commandBuffer.SetGlobalVector("offsets", new Vector4(0,(blurAmount*2f)/Screen.height,0,0));
        commandBuffer.Blit (blurredID2, blurredID, blurMaterial);
        
        
        commandBuffer.Blit (blurredID,BuiltinRenderTextureType.GBuffer2);
        
        Camera.main.AddCommandBuffer(CameraEvent.BeforeReflections,commandBuffer);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
