﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Springs
{
    public class SpringDamper
    {
        private readonly SpringDamperParameters _springDamperParameters;
        public float position;
        public float velocity;

        public SpringDamper(SpringDamperParameters springDamperParameters)
        {
            _springDamperParameters = springDamperParameters;
        }


        public void updateSpring(float targetPosition)
        {
            var oldPos = position - targetPosition; // update in equilibrium relative space
            var oldVel = velocity;

            position = oldPos * _springDamperParameters.positionPositionCoefficient +
                       oldVel * _springDamperParameters.positionVelocityCoefficient +
                       targetPosition;
            velocity = oldPos * _springDamperParameters.velocityPositionCoefficient +
                       oldVel * _springDamperParameters.velocityVelocityCoefficient;
        }
    }

    public class SpringDamper2D
    {
        private readonly SpringDamperParameters _springDamperParameters;
        public Vector2 position;
        public Vector2 velocity;
        
        public SpringDamper2D(SpringDamperParameters springDamperParameters)
        {
            _springDamperParameters = springDamperParameters;
        }
        
        public void UpdateSpring(Vector2 targetPosition)
        {
            var oldPos = position - targetPosition; // update in equilibrium relative space
            var oldVel = velocity;

            position = oldPos * _springDamperParameters.positionPositionCoefficient +
                       oldVel * _springDamperParameters.positionVelocityCoefficient +
                       targetPosition;
            velocity = oldPos * _springDamperParameters.velocityPositionCoefficient +
                       oldVel * _springDamperParameters.velocityVelocityCoefficient;
        }
    }
    
    public class SpringDamper3D
    {
        private readonly SpringDamperParameters _springDamperParameters;
        public Vector3 position;
        public Vector3 velocity;
        
        public SpringDamper3D(SpringDamperParameters springDamperParameters)
        {
            _springDamperParameters = springDamperParameters;
        }
        
        public void UpdateSpring(Vector3 targetPosition)
        {
            var oldPos = position - targetPosition; // update in equilibrium relative space
            var oldVel = velocity;

            position = oldPos * _springDamperParameters.positionPositionCoefficient +
                       oldVel * _springDamperParameters.positionVelocityCoefficient +
                       targetPosition;
            velocity = oldPos * _springDamperParameters.velocityPositionCoefficient +
                       oldVel * _springDamperParameters.velocityVelocityCoefficient;
        }
        
        public void UpdateSpring(Vector3 targetPosition, Vector3 overrideVelocity)
        {
            var oldPos = position - targetPosition; // update in equilibrium relative space
            var oldVel = overrideVelocity;

            position = oldPos * _springDamperParameters.positionPositionCoefficient +
                       oldVel * _springDamperParameters.positionVelocityCoefficient +
                       targetPosition;
            velocity = oldPos * _springDamperParameters.velocityPositionCoefficient +
                       oldVel * _springDamperParameters.velocityVelocityCoefficient;
        }
        
        public void UpdateSpring(Vector3 targetPosition, Vector3 overrideVelocity, Vector3 overridePosition)
        {
            var oldPos = overridePosition - targetPosition; // update in equilibrium relative space
            var oldVel = overrideVelocity;

            position = oldPos * _springDamperParameters.positionPositionCoefficient +
                       oldVel * _springDamperParameters.positionVelocityCoefficient +
                       targetPosition;
            velocity = oldPos * _springDamperParameters.velocityPositionCoefficient +
                       oldVel * _springDamperParameters.velocityVelocityCoefficient;
        }
    }
    
    public class SpringDamperParameters
    {
        public readonly float positionPositionCoefficient;
        public readonly float positionVelocityCoefficient;
        public readonly float velocityPositionCoefficient;
        public readonly float velocityVelocityCoefficient;

        public SpringDamperParameters(float deltaTime, float angularFrequency, float dampingRatio)
        {
            const float epsilon = 0.0001f;

            // force values into legal range
            if (dampingRatio < 0.0f) dampingRatio = 0.0f;
            if (angularFrequency < 0.0f) angularFrequency = 0.0f;

            // if there is no angular frequency, the spring will not move and we can
            // return identity
            if (angularFrequency < epsilon)
            {
                positionPositionCoefficient = 1f;
                positionVelocityCoefficient = 0f;
                velocityPositionCoefficient = 0f;
                velocityVelocityCoefficient = 1f;
            }

            if (dampingRatio > 1.0f + epsilon)
            {
                // over-damped
                var za = -angularFrequency * dampingRatio;
                var zb = angularFrequency * Mathf.Sqrt(dampingRatio * dampingRatio - 1.0f);
                var z1 = za - zb;
                var z2 = za + zb;

                var e1 = Mathf.Exp(z1 * deltaTime);
                var e2 = Mathf.Exp(z2 * deltaTime);

                var invTwoZb = 1.0f / (2.0f * zb); // = 1 / (z2 - z1)

                var e1OverTwoZb = e1 * invTwoZb;
                var e2OverTwoZb = e2 * invTwoZb;

                var z1E1OverTwoZb = z1 * e1OverTwoZb;
                var z2E2OverTwoZb = z2 * e2OverTwoZb;

                positionPositionCoefficient = e1OverTwoZb * z2 - z2E2OverTwoZb + e2;
                positionVelocityCoefficient = -e1OverTwoZb + e2OverTwoZb;

                velocityPositionCoefficient = (z1E1OverTwoZb - z2E2OverTwoZb + e2) * z2;
                velocityVelocityCoefficient = -z1E1OverTwoZb + z2E2OverTwoZb;
            }
            else if (dampingRatio < 1.0f - epsilon)
            {
                // under-damped
                var omegaZeta = angularFrequency * dampingRatio;
                var alpha = angularFrequency * Mathf.Sqrt(1.0f - dampingRatio * dampingRatio);

                var expTerm = Mathf.Exp(-omegaZeta * deltaTime);
                var cosTerm = Mathf.Cos(alpha * deltaTime);
                var sinTerm = Mathf.Sin(alpha * deltaTime);

                var invAlpha = 1.0f / alpha;

                var expSin = expTerm * sinTerm;
                var expCos = expTerm * cosTerm;
                var expOmegaZetaSinOverAlpha = expTerm * omegaZeta * sinTerm * invAlpha;

                positionPositionCoefficient = expCos + expOmegaZetaSinOverAlpha;
                positionVelocityCoefficient = expSin * invAlpha;

                velocityPositionCoefficient =
                    -expSin * alpha - omegaZeta * expOmegaZetaSinOverAlpha;
                velocityVelocityCoefficient = expCos - expOmegaZetaSinOverAlpha;
            }
            else
            {
                // critically damped
                var expTerm = Mathf.Exp(-angularFrequency * deltaTime);
                var timeExp = deltaTime * expTerm;
                var timeExpFreq = timeExp * angularFrequency;

                positionPositionCoefficient = timeExpFreq + expTerm;
                positionVelocityCoefficient = timeExp;

                velocityPositionCoefficient = -angularFrequency * timeExpFreq;
                velocityVelocityCoefficient = -timeExpFreq + expTerm;
            }
        }
    }
}


