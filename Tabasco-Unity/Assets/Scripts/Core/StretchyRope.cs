﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Springs;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class StretchyRope : MonoBehaviour
{
    public Transform anchor;
    public float frequency = 20f;
    public float damping = 6f;
    public float gravity = 9.8f;
    public int numberOfSegments = 10;
    public int middleSegment = 5;
    
    
    private LineRenderer _lineRenderer;
    private RopeSegment[] _ropeSegments;
    private SpringDamperParameters _springDamperParameters;
    private Vector3[] _positionArray;
    
    // Start is called before the first frame update
    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.positionCount = numberOfSegments;
        _positionArray = new Vector3[numberOfSegments];
        _ropeSegments = new RopeSegment[numberOfSegments];
        _springDamperParameters = new SpringDamperParameters(Time.fixedDeltaTime, frequency, damping);

        for (var i = 0; i < _ropeSegments.Length; i++)
        {
            _ropeSegments[i] = new RopeSegment(_springDamperParameters);
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        
        _ropeSegments[0].position = _positionArray[0] = transform.position;
        _ropeSegments[_ropeSegments.Length - 1].position = _positionArray[_ropeSegments.Length - 1] = anchor.position;
        
        for (var i = 1; i < middleSegment; i++)
        {
            _ropeSegments[i].UpdateRopeSegment(_ropeSegments[i-1].position,_ropeSegments[i+1].position,gravity);
            
            _positionArray[i] = _ropeSegments[i].position;
        }
        for (var i = _ropeSegments.Length - 1 - 1; i >= middleSegment; i--)
        {
            _ropeSegments[i].UpdateRopeSegment(_ropeSegments[i-1].position,_ropeSegments[i+1].position,gravity);
            
            _positionArray[i] = _ropeSegments[i].position;
        }
        
        _lineRenderer.SetPositions(_positionArray);
    }

    private class RopeSegment
    {
        public Vector3 position;
        private readonly SpringDamper3D _springDamper0;
        private readonly SpringDamper3D _springDamper1;

        public RopeSegment(SpringDamperParameters springDamperParameters)
        {
            _springDamper0 = new SpringDamper3D(springDamperParameters);
            _springDamper1 = new SpringDamper3D(springDamperParameters);
        }

        public void UpdateRopeSegment(Vector3 target0, Vector3 target1, float gravity)
        {
            
            _springDamper0.UpdateSpring(target0,_springDamper1.velocity + (Vector3.down * gravity), position);
            _springDamper1.UpdateSpring(target1,_springDamper0.velocity, _springDamper0.position);
            position = _springDamper1.position;
        }

    }
}
