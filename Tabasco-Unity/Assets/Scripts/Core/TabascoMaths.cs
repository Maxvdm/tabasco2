﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TabascoMaths
{
    public static float RemapFloat(float value, float inputMin, float inputMax, float outputMin, float outputMax)
    {
        return outputMin + (value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin);
    }

    public static float RemapFloatClamped(float value, float inputMin, float inputMax, float outputMin, float outputMax)
    {
        return Mathf.Clamp(outputMin + (value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin),outputMin,outputMax);
    }
    
}
