using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SolvedSpring : MonoBehaviour
{
       public static float RawSimpleSolvedSpring(float decay, float frequency, float time)
       {
              return  Mathf.Exp(-decay * time) * Mathf.Cos(2 * Mathf.PI * frequency * time);
       }

       public static float RawSolvedSpring(float spring, float occilations, float initialPosition, float initialVelocity, float time)
       {
              var a = 0f;
              var b = 0f;
              float output = Mathf.Pow(
                                  Mathf.Epsilon, -spring * 2 * Mathf.PI * occilations * time) * 
                           (a * Mathf.Cos(Mathf.Sqrt(1-Mathf.Pow(spring,2)))) +
                     b;
              return 1;
       }
       
}