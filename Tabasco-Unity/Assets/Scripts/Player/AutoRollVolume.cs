﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class AutoRollVolume : MonoBehaviour
{
    private Transform _transform;
    private bool _isActive;
    void Start()
    {
        _transform = transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("ENTERED TRIGGER");
        Debug.Log(other.name);
        ActivateAutoRoll(other, true);
    }

    private void OnTriggerStay(Collider other)
    {
        ActivateAutoRoll(other, true);
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("LEFT TRIGGER");
        Debug.Log(other.name);
        ActivateAutoRoll(other, false);
    }

    private void ActivateAutoRoll(Collider other, bool isActivating)
    {
        if (other.tag == "Player Trigger")
        {
            PlayerCharacter playerCharacter =  other.GetComponentInParent<PlayerCharacter>();
            if (playerCharacter !=null && playerCharacter.autoRoll != isActivating)
            {
                playerCharacter.autoRollTargetDirection = _transform.forward;
                playerCharacter.autoRoll = isActivating;
                _isActive = isActivating;
            }
        }
    }
}

