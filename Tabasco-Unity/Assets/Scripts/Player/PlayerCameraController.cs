﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    public CinemachineMixingCamera mixingCamera;
    public CinemachineFreeLook freeLook;
    public float changingForce = 0.005f;
    public float restoringForce = 0.5f;
    private Rigidbody _rigidBody;
    private float _verticalCameraPosition;
    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        var velocity = _rigidBody.velocity;
        _verticalCameraPosition += (-velocity.y * Mathf.Abs(velocity.y)) * changingForce * Time.deltaTime;
        _verticalCameraPosition = Mathf.Clamp01(_verticalCameraPosition - (restoringForce * Time.deltaTime )); 
        mixingCamera.m_Weight1 = _verticalCameraPosition;
        mixingCamera.m_Weight0 = 1 - _verticalCameraPosition;
    }
}
