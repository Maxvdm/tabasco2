﻿using System;
using System.Collections.Generic;
using Cinemachine;
using HutongGames.PlayMaker;
using HutongGames.PlayMaker.Actions;
using InControl;
using UnityEngine;
using MonsterLove.StateMachine;
using UnityEngine.Tilemaps;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(Rigidbody))]
public class PlayerCharacter : MonoBehaviour
{

    public PlayerPhysicsSettings playerPhysics;

    public float downForce = 50f;
    public float downForceDecay = 2f;
    public PhysicMaterial playerPhysicMaterial;
    public float defaultFrictionValue;
    public float defaultBouncinessValue;
    public bool autoRoll;
    
    public Animator visAnimator;

    public Vector3 autoRollTargetDirection = Vector3.forward;
    private Vector3 _autoRollDirection = Vector3.forward;

    public TrailRenderer[] contrailRenderers;
    public ParticleSystem downSlamParticles;
    public ParticleSystem shockwaveParticles;
    public CinemachineImpulseSource cinemachineImpulseSource;
    

    public Transform implicitTransform;
    public Transform vis;
    public GameObject sphereCollider;
    public GameObject complexCollider;

    private float _gearValue;
    
    private float _tiltAmount;
    private Vector3 _movementDirection;
    private Vector3 _playerUpVector;
    private Vector3 _playerForwardVector;
    private float _jumpGas;
    private bool _hasJumped;
    private float _gyroscopeGas;
    private float _downForceGas;
    private float _wobbleProgress;
    private float _jumpTrigger;
    private bool _jumpTriggerInUse;
    private float _brakeTrigger;
    private bool _brakeTriggerInUse;
    
    private bool _2dxyMovement;

    private Transform _cameraTransform;
    private Transform _transform;
    private Rigidbody _rigidBody;
    private Vector3 _rigidBodyVelocity;
    private RaycastHit[] _raycastArray = new RaycastHit[4];
    private LayerMask _raycastLayerMask = new LayerMask();
    private Vector3 _previousFrameVelocity;
    private float _touchingGroundTimer;
    private List<Vector3> _previousVelocities = new List<Vector3>(5);
    
    private Vector3 _groundNormal;
    private Vector3 _collisionNormal;
    private Vector3 _groundContactPosition;
    private float _jumpTimer;
    private Material _playerMaterial;

    private InputDevice _inputDevice;

    private Vector2 _leftStickInput;

    private enum States
    {
        Grounded,
        Airborne,
        Downward,
        Stopped
    }

    private StateMachine<States> _fsm;

    private void Start()
    {
        if (Camera.main != null) _cameraTransform = Camera.main.transform;
        _rigidBody = GetComponent<Rigidbody>();
        _transform = transform;
        _fsm = StateMachine<States>.Initialize(this);
        _fsm.ChangeState(States.Grounded);
        _rigidBody.maxAngularVelocity = playerPhysics.maxAngularVelocity;
        playerPhysicMaterial.bounciness = defaultBouncinessValue;
        _raycastLayerMask = LayerMask.GetMask("Default", "Terrain");

       

         
    }

    private void FixedUpdate()
    {
        _playerUpVector = GetPlayerUpVector();
        _playerForwardVector = GetPlayerForwardVector();
        _movementDirection = GetMovementDirection();
        _rigidBodyVelocity = _rigidBody.velocity;
        _touchingGroundTimer -= Time.fixedDeltaTime * 25f;
        _collisionNormal = Vector3.down;
    }

    private void Update()
    {
        UpdateInputValues();
        UpdateImplicitTransform();
        UpdateGyro();
        UpdatePreviousVelocities();
        UpdateVis();
        

        if (_transform.position.y < -150f) _transform.position = new Vector3(_transform.position.x, 800f, _transform.position.z);
        
        
        if (_jumpTrigger < 0.1f) _jumpTriggerInUse = false;

        if (_brakeTrigger > 0.1f) _fsm.ChangeState(States.Downward);

        _autoRollDirection = (_autoRollDirection + (autoRollTargetDirection - _autoRollDirection) * Time.deltaTime).normalized;
        
        _jumpTimer -= Time.deltaTime;
    }
    
    private void Grounded_Enter()
    {
        _jumpGas = 0f;
        _rigidBody.velocity = new Vector3(_rigidBodyVelocity.x,Mathf.Clamp(_rigidBodyVelocity.y,-50f,50f),_rigidBodyVelocity.z);
    }
    
    private void Grounded_FixedUpdate()
    {
        var tractionDirection = Vector3.ProjectOnPlane(_movementDirection * _movementDirection.sqrMagnitude, _groundNormal);
        
        MoveTowards(tractionDirection.normalized,playerPhysics.accelerationForce.GetValue(_gearValue) * tractionDirection.magnitude,playerPhysics.accelerationHeightOffset.GetValue(_gearValue) );
        SelfRight(playerPhysics.selfRightingForce.GetValue(_gearValue), playerPhysics.selfRightingTorque.GetValue(_gearValue));
        RotateTowards(tractionDirection, playerPhysics.turnForce.GetValue(_gearValue) * _movementDirection.sqrMagnitude);
        AddDragForce(playerPhysics.dragForce.GetValue(_gearValue));
        AddCorneringForce(playerPhysics.corneringStiffness.GetValue(_gearValue));

        if (!IsTouchingGround())
        {
            _touchingGroundTimer = 0f;
            _fsm.ChangeState(States.Airborne);
        }

    }

    private void Grounded_Update()
    {
        DisableFrictionAtHighSpeeds();
        _gearValue = GetGearValue();
        AddJumpForce(playerPhysics.initialJumpForce.GetValue(_gearValue));
        
        _tiltAmount += Mathf.Clamp((Mathf.Clamp(Vector3.Dot(_movementDirection, Vector3.Cross(_groundNormal, _rigidBodyVelocity.normalized)) * _rigidBodyVelocity.magnitude * playerPhysics.tiltStrength.GetValue(_gearValue),-45f,45f) - _tiltAmount),-3f,3f) * 0.8f; 
        TiltVis(_tiltAmount, playerPhysics.wobbleFrequency.GetValue(_gearValue), playerPhysics.wobbleMagnitude.GetValue(_gearValue));
        
        _downForceGas += Time.deltaTime;
        
        if (_movementDirection.magnitude < 0.1f && _gyroscopeGas < 0.1f)
        {
            _fsm.ChangeState(States.Stopped);
        }
    }

    private void Stopped_Enter()
    {
        EnableFriction();

        ToggleComplexCollider();
        _rigidBody.AddTorque(_playerForwardVector * 10f, ForceMode.Impulse);

    }

    private void ToggleComplexCollider()
    {
        complexCollider.SetActive(true);
        sphereCollider.SetActive(false);
    }

    private void Stopped_FixedUpdate()
    {
        var tractionDirection = Vector3.ProjectOnPlane(_movementDirection * _movementDirection.sqrMagnitude, _groundNormal);
        
        MoveTowards(tractionDirection.normalized,playerPhysics.accelerationForce.GetValue(_gearValue) * tractionDirection.magnitude,playerPhysics.accelerationHeightOffset.GetValue(_gearValue) / 20f );
        RotateTowards(tractionDirection, playerPhysics.turnForce.GetValue(_gearValue) * _movementDirection.sqrMagnitude);
        AddDragForce(playerPhysics.dragForce.GetValue(_gearValue));

        if (!IsTouchingGround())
        {
            _touchingGroundTimer = 0f;
            _fsm.ChangeState(States.Airborne);
        }
    }
    private void Stopped_Update()
    {
        _gearValue = GetGearValue();
        AddJumpForce(playerPhysics.initialJumpForce.GetValue(_gearValue));
        
        _downForceGas += Time.deltaTime;
        
        if (_movementDirection.magnitude > 0.1f && _gyroscopeGas > 0.1f)
        {
            _fsm.ChangeState(States.Grounded);
        }
    }
    private void Stopped_Exit()
    {
        
        complexCollider.SetActive(false);
        sphereCollider.SetActive(true);
    }

    private void Airborne_Enter()
    {
            _touchingGroundTimer = 0f;
    }
    private void Airborne_FixedUpdate()
    {
        
        MoveTowards(_movementDirection.normalized,playerPhysics.accelerationForce.GetValue(_gearValue) * _movementDirection.sqrMagnitude, playerPhysics.accelerationHeightOffset.GetValue(_gearValue) );
        AddDragForce(playerPhysics.dragForce.GetValue(Mathf.Clamp(_gearValue,1f,2f)));

        _rigidBody.AddForce(playerPhysics.sustainedJumpForce.GetValue(_gearValue) * _jumpTrigger * _jumpGas * Vector3.up, ForceMode.Acceleration);
        _jumpGas -= _jumpTrigger * _jumpGas * playerPhysics.jumpDecay.GetValue(_gearValue) * Time.fixedDeltaTime;
        _jumpGas *= _jumpTrigger;
        _jumpGas = Mathf.Clamp01(_jumpGas);

        
        if (_rigidBodyVelocity.y < 0f){
             SelfRight(playerPhysics.selfRightingForce.GetValue(_gearValue), playerPhysics.selfRightingTorque.GetValue(_gearValue));
        }
      
        if (IsTouchingGround() && _jumpGas < 0.1f)
        {
            _fsm.ChangeState(States.Grounded);
        }
        
    }
    private void Airborne_Update()
    {
        DisableFrictionAtHighSpeeds();
        TiltVis(0, playerPhysics.wobbleFrequency.GetValue(_gearValue), playerPhysics.wobbleMagnitude.GetValue(_gearValue));
        _downForceGas += Time.deltaTime;
    }

    private void Airborne_Exit()
    {
    }

    
    private void Downward_Enter()
    {
         
        playerPhysicMaterial.bounciness = 0f;
    }
    
    private void Downward_Update()
    {

        DisableFrictionAtHighSpeeds();
        
        _gearValue = GetGearValue();
        
        visAnimator.SetFloat("Heaviness", _downForceGas);
        
        _downForceGas = Mathf.Clamp01(_downForceGas - (Time.deltaTime * downForceDecay));
        if ( _rigidBodyVelocity.y > 0f && IsTouchingGround())  _downForceGas = Mathf.Clamp01(_downForceGas - Time.deltaTime * 10f);
        if (_rigidBodyVelocity.y < 0f) _downForceGas += Time.deltaTime;
        
        if ( _downForceGas <= 0f) _fsm.ChangeState(States.Grounded);
        if(_brakeTrigger < 0.1f && IsTouchingGround()) _fsm.ChangeState(States.Grounded);

    }
    private void Downward_FixedUpdate()
    {
        var downwardGearValue = Mathf.Clamp(_gearValue,1f,2f);
        if (_brakeTrigger > 0.1f) 
        {
            _rigidBody.AddForce(downForce * _brakeTrigger * _downForceGas * Vector3.down, ForceMode.Force);
        }

        var tractionDirection = Vector3.ProjectOnPlane(_movementDirection * _movementDirection.sqrMagnitude, _groundNormal);
        
        MoveTowards(tractionDirection.normalized,playerPhysics.accelerationForce.GetValue(downwardGearValue) * tractionDirection.magnitude, playerPhysics.accelerationHeightOffset.GetValue(downwardGearValue) );
        RotateTowards(tractionDirection, playerPhysics.turnForce.GetValue(downwardGearValue) * _movementDirection.sqrMagnitude);
        AddDragForce(playerPhysics.dragForce.GetValue(2));
        
        if(IsTouchingGround()) 
            AddCorneringForce(playerPhysics.corneringStiffness.GetValue(_gearValue));
    }

    private void Downward_Exit()
    {
        playerPhysicMaterial.bounciness = defaultBouncinessValue;
        visAnimator.SetFloat("Heaviness", 0f);
    }

    private void OnCollisionEnter(Collision other)
    {
        var contactVelocity = Mathf.Abs(_rigidBodyVelocity.y) * TabascoMaths.RemapFloatClamped(Mathf.Clamp01(Vector3.Dot(Vector3.up, other.contacts[0].normal)), 0.9f,1f,0f,1f);
        
        if (contactVelocity > 30f && _fsm.State == States.Downward) 
            DownSlam(TabascoMaths.RemapFloatClamped(contactVelocity, 10f, 60f, 0f, 1f));
        else if (contactVelocity > 10f &&_hasJumped && _fsm.State != States.Downward)
        {
            PlayMakerFSM.BroadcastEvent("Player Jumped");
            _hasJumped = false;
        }
    }

    private void OnCollisionStay(Collision other) 
    {
        _groundNormal = GetGroundNormal(other);
        _touchingGroundTimer = 1f;

        AddRollingResistance(playerPhysics.rollingResistanceForce.GetValue(_gearValue));
    }

    private Vector3 GetGroundNormal(Collision other)
    {
        foreach (var contactInstance in other.contacts)
        {
            if (Vector3.Angle(Vector3.up, contactInstance.normal) < Vector3.Angle(Vector3.up, _collisionNormal))
                _collisionNormal = contactInstance.normal;
        }

        if (Physics.RaycastNonAlloc(_transform.position, Vector3.down, _raycastArray, 1.5f,_raycastLayerMask) > 0)
        {
            _collisionNormal = _raycastArray[0].normal;
        }

        return _collisionNormal;
    }

    public Vector3 GetGroundContactPosition()
    {
        return _transform.position - _playerUpVector.normalized * 0.5f;
    }
    
    public void UpdateVis()
    {
        var position = _transform.position;
        vis.position = position;
        vis.rotation =_transform.rotation;
    }
    
    public void TiltVis(float tiltAmount, float wobbleFrequency, float wobbleMagnitude)
    {
        _wobbleProgress += Time.deltaTime * wobbleFrequency ;
        var wobbleAmount = Mathf.Sin(_wobbleProgress) * Mathf.Sin(_wobbleProgress * 0.456f) * wobbleMagnitude;
        vis.RotateAround(_transform.position - (_playerUpVector * 0.5f), _playerForwardVector, tiltAmount + wobbleAmount);
    }


    private void UpdateGyro()
    {
        if (_movementDirection.sqrMagnitude > 0.2f && _movementDirection.magnitude < 70f) _gyroscopeGas += _movementDirection.magnitude * 0.5f * Time.deltaTime;
        else _gyroscopeGas -= (1 - _movementDirection.sqrMagnitude) * Mathf.Clamp(TabascoMaths.RemapFloat(_rigidBodyVelocity.magnitude, playerPhysics.stoppedGearThreshold, 70f, 5f, 0f), 1f, 5f) * Time.deltaTime;

        _gyroscopeGas = Mathf.Clamp(_gyroscopeGas, 0, 2f);
    }

    private void AddJumpForce(float force)
    {
        if (_jumpTrigger > 0.1f && IsTouchingGround() && !_jumpTriggerInUse && _jumpTimer <= 0f)
        {
            _rigidBody.AddForce(force * (1- TabascoMaths.RemapFloatClamped(_rigidBodyVelocity.y,0f, 20f, 0f, 1f)) *  Vector3.up, ForceMode.Impulse);
            //Debug.Log(1- TabascoMaths.RemapFloatClamped(_rigidBodyVelocity.y,0f, 20f, 0f, 1f));
            _rigidBody.AddTorque(_playerUpVector * force/20f, ForceMode.Impulse);
            _jumpGas = 1f;
            _jumpTriggerInUse = true;
            _fsm.ChangeState(States.Airborne);
            _jumpTimer = 0.25f;
            _hasJumped = true;
        }
    }

    private void DisableFrictionAtHighSpeeds()
    {
        if (_rigidBodyVelocity.magnitude * 2f > playerPhysics.maxAngularVelocity * 0.99f)
        {
            DisableFriction();
        }
        else
        {
            EnableFriction();
        }
    }

    private void EnableFriction()
    {
        playerPhysicMaterial.dynamicFriction = defaultFrictionValue;
        playerPhysicMaterial.staticFriction = defaultFrictionValue;
    }

    private void DisableFriction()
    {
        playerPhysicMaterial.dynamicFriction = 0.05f;
        playerPhysicMaterial.staticFriction = 0.05f;
    }
    
    private void MoveTowards(Vector3 direction, float amount, float heightOffset)
    {
        var pushPosition = _transform.position + (_groundNormal * heightOffset);
        
        if (Vector3.Dot(direction.normalized, _rigidBodyVelocity.normalized) > 0f) _rigidBody.AddForceAtPosition((amount * direction), pushPosition, ForceMode.Force);
        else _rigidBody.AddForceAtPosition(amount * 3f * direction, pushPosition, ForceMode.Force);
    }

    private void SelfRight(float inputForce, float inputTorque)
    {
        _rigidBody.AddForceAtPosition(_groundNormal * inputForce, _transform.position + GetPlayerUpVector() * inputTorque);
    }

    private void RotateTowards(Vector3 desiredDirection, float force)
    {
        var right = _transform.right;
        var directionFacing = Vector3.Dot(-_transform.forward, Vector3.Cross(right, _playerUpVector));
        var amountToTurn = Vector3.Dot(desiredDirection, right) * directionFacing;

        _rigidBody.AddTorque(amountToTurn * force * _playerUpVector);

        var relativeAngularVelocity = Vector3.Project(_rigidBody.angularVelocity,_playerUpVector).y;
        var dragForce = -relativeAngularVelocity * playerPhysics.turnDrag.GetValue(_gearValue);
        _rigidBody.AddTorque(dragForce * Vector3.up);
    }

    private void AddDragForce(float inputDrag)
    {
        _rigidBody.AddForce(inputDrag *_rigidBodyVelocity.sqrMagnitude * -_rigidBodyVelocity.normalized );
    }

    private void AddRollingResistance(float inputDrag)
    {
        var velocityRelativeToGround = Vector3.ProjectOnPlane(_rigidBodyVelocity, _groundNormal);
        velocityRelativeToGround = new Vector3(velocityRelativeToGround.x,0,velocityRelativeToGround.z);
        _rigidBody.AddForce(inputDrag * -velocityRelativeToGround);
    }

    private void AddCorneringForce( float stiffness)
    {
        var right = _transform.right;
        var corneringAlpha = Vector3.Dot(right, _rigidBodyVelocity.normalized);
        
        _rigidBody.AddForce(stiffness * corneringAlpha * _rigidBodyVelocity.magnitude * right);
    }
    private void  UpdateInputValues()
    {
        
        _inputDevice = InputManager.ActiveDevice;
        _leftStickInput = _inputDevice.LeftStick.Value;
        
        if (_inputDevice.Action1.IsPressed || _inputDevice.RightTrigger.IsPressed) _jumpTrigger = 1;
        else _jumpTrigger = 0f;
        if (_inputDevice.Action3.IsPressed || _inputDevice.LeftTrigger.IsPressed) _brakeTrigger = 1;
        else _brakeTrigger = 0f;
    }
    private Vector3 GetPlayerUpVector()
    {
        var leftVector = _transform.TransformVector(Vector3.left);
        var upVector = (_groundNormal - Vector3.Dot(_groundNormal, leftVector) / leftVector.sqrMagnitude * leftVector).normalized;
        return upVector;
    }
    private Vector3 GetPlayerForwardVector()
    {
        var upVector = _playerUpVector;
        var direction = _movementDirection;
        return (direction - Vector3.Dot(direction, upVector) / upVector.sqrMagnitude * upVector).normalized;
    }
    private Vector3 GetMovementDirection()
    {
        var isFacingDown = Mathf.Clamp01(Vector3.Dot(Vector3.down,_cameraTransform.forward));
        var movementDirection = Vector3.ProjectOnPlane(_cameraTransform.TransformDirection(new Vector3(_leftStickInput.x , _leftStickInput.y * isFacingDown, _leftStickInput.y * (1f - isFacingDown))), Vector3.up);
        if (autoRoll) movementDirection = Mathf.Clamp(Vector3.Dot(movementDirection, _autoRollDirection) + 1f,0,1f) * movementDirection;
        if(_2dxyMovement) movementDirection = movementDirection.x * Vector3.right;

        return movementDirection;
    }
    private bool IsTouchingGround()
    {
        return _touchingGroundTimer > 0f || Physics.RaycastNonAlloc(_transform.position, Vector3.down, _raycastArray, 1.5f, _raycastLayerMask) > 0;
    }

    private float GetGearValue()
    {
        var lowGearValue = TabascoMaths.RemapFloatClamped(_rigidBodyVelocity.magnitude, playerPhysics.stoppedGearThreshold, playerPhysics.lowGearThreshold, 0f, 1f);
        var highGearValue = TabascoMaths.RemapFloatClamped(_rigidBodyVelocity.magnitude,playerPhysics.lowGearThreshold,playerPhysics.highGearThreshold,0f,1f);
        if (_rigidBodyVelocity.magnitude > playerPhysics.lowGearThreshold) return 1f + highGearValue;
        else return Mathf.Clamp01((_gyroscopeGas + lowGearValue) / 2f) + highGearValue;
    }
    private void SetContrailVisibility(bool isActive)
    {
        foreach (var contrailRenderer in contrailRenderers )
        {
            contrailRenderer.emitting = isActive;
        }
    }
    private void UpdateImplicitTransform()
    {
        Vector3 velocityForwardDirection;
        if (_rigidBody.velocity.magnitude > 10f)
        {
            velocityForwardDirection = _rigidBodyVelocity.normalized;
            _previousFrameVelocity = _rigidBodyVelocity;
        }
        else velocityForwardDirection = _previousFrameVelocity.normalized;
        implicitTransform.position = _transform.position;
        //implicitTransform.rotation = Quaternion.LookRotation(velocityForwardDirection.normalized,Vector3.up);
        
    }
    private void UpdatePreviousVelocities()
    {
        var newVelocity = Time.deltaTime * 1000f * _movementDirection;
        if(_previousVelocities.Count > 4) _previousVelocities.RemoveAt(_previousVelocities.Count - 1);
        if(newVelocity.sqrMagnitude > 1f && _movementDirection.sqrMagnitude > 0.1f)  _previousVelocities.Insert(0, newVelocity);
    }
    
    private void SetDownwardVis(bool enable)
    {
        SetContrailVisibility(enable);
    }

    private void DownSlam(float strength)
    {
        PlayMakerFSM.BroadcastEvent("Down Slam");
        shockwaveParticles.Emit(1);
        downSlamParticles.Emit(Mathf.RoundToInt(strength * 80f));
        cinemachineImpulseSource.GenerateImpulse(Vector3.down * strength);
        AddExplosionForce(_transform.position, 15f, 125f);
        
    }

    private static void AddExplosionForce(Vector3 explosionCenterPosition, float explosionRadius, float explosionForce)
    {
        Collider[] objects = Physics.OverlapSphere(explosionCenterPosition, explosionRadius);
        foreach (var targetCollider in objects)
        {
            var rigidBody = targetCollider.GetComponent<Rigidbody>();
            if (rigidBody != null)
            {
                rigidBody.AddExplosionForce(explosionForce, explosionCenterPosition, explosionRadius);
            }
        }
    }
    public void ConstrainMovementToXyPlane(bool enable)
    {

        _2dxyMovement = enable;
        if (enable)
        { 
            _rigidBody.constraints = RigidbodyConstraints.FreezePositionZ;
        }
        else 
        {
            _rigidBody.constraints =  RigidbodyConstraints.None;
        }
    }

    public void AddExternalForce(Vector3 forceVector, bool consistent = true)
    {
        _gearValue = 2f;
        _fsm.ChangeState(States.Airborne);
        _rigidBody.AddForce(forceVector, ForceMode.VelocityChange);
    }
    
    
    private void OnGUI()
    {
        var playerDebugGuiStyle = new GUIStyle();
        playerDebugGuiStyle.fontSize = 30;
        playerDebugGuiStyle.normal.textColor = Color.white;
        
        GUI.Label(new Rect(10,10,400,100),"current state = " + _fsm.State,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,50,400,100), "gear changeValue = " +  _gearValue ,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,90,400,100),"velocity = " + _rigidBody.velocity.magnitude,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,140,400,100),"Jump gas = " + _jumpGas,playerDebugGuiStyle);

        GUI.Label(new Rect(10, 190, 400, 100), "Slope amount = " + TabascoMaths.RemapFloatClamped(1 - Vector3.Dot(_groundNormal,Vector3.up), 0.01f, 0.4f,0f,1f), playerDebugGuiStyle);
        
       // GUI.Label(new Rect(10,240,400,100),"Friction on = " + (_rigidBody.velocity.magnitude * 2f < maxAngularVelocity * 0.99f).ToString(),playerDebugGuiStyle);

        GUI.Label(new Rect(10,290,400,100),"AngularVelocity = " + _rigidBody.angularVelocity.magnitude,playerDebugGuiStyle);

        GUI.Label(new Rect(10,340,400,100),"CorneringAlpha = " + Vector3.Dot(_transform.right, _rigidBody.velocity.normalized),playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,390,400,100),"Player Forward Vector = " + _playerForwardVector,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,440,400,100),"Brake Value = " + _brakeTrigger, playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,490,400,100),"Jump Value = " + _jumpTrigger, playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,540,400,100),"turn amount = " + Vector3.Dot(_playerForwardVector, Vector3.Cross(_transform.right, _playerUpVector)), playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,590,400,100),"Gyro gas= " + _gyroscopeGas, playerDebugGuiStyle);

        GUI.Label(new Rect(10,640,400,100),"Jump Timer = " + Mathf.Clamp01(_jumpTimer), playerDebugGuiStyle);

        GUI.Label(new Rect(10,690,400,100),"Down Force = " + Mathf.Clamp01(_downForceGas), playerDebugGuiStyle);

        GUI.Label(new Rect(10,740,400,100),"Is Touching Ground = " + IsTouchingGround() + " or: " + _touchingGroundTimer, playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,790,400,100),"Movement Direction = " + _movementDirection, playerDebugGuiStyle);

        
    }

    private void DrawDebugForceGuides()
    {
        
        //Debug.DrawRay(_transform.position,_playerUpVector,Color.blue);
        //Debug.DrawRay(_transform.position,Vector3.Cross(_transform.right, _playerUpVector), Color.cyan);
        
        //Debug.DrawRay(_transform.position, _playerForwardVector.normalized, Color.yellow);
    }

}