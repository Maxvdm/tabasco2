﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;
using HutongGames.PlayMaker.Actions;
using UnityEngine;
using MonsterLove.StateMachine;
using Quaternion = UnityEngine.Quaternion;
using Random = System.Random;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(Rigidbody))]
public class PlayerCharacterNonPhysical : MonoBehaviour
{

    public PlayerPhysicsSettings playerPhysics;

    public float maxAngularVelocity = 30f;
    public float characterRadius = 0.5f;
    
    
    public float lowGearThreshold = 20f;
    public float highGearThreshold = 45f;
    public float downForce = 50f;
    public PhysicMaterial playerPhysicMaterial;
    public float defaultFrictionValue;
    public float defaultBouncinessValue;

    public TrailRenderer[] contrailRenderers;

    public Transform turnPivot;
    public Transform tiltPivot;
    public Transform rollPivot;

    private float _gearChangeValue;

    private Vector3 _movementDirection;
    private Vector3 _previousFrameMovementDirection;
    private Vector3 _playerUpVector;
    private Vector3 _playerForwardVector;
    private bool _hasDoubleJump;
    private float _jumpGas;
    
    private float _jumpTrigger;
    private bool _jumpTriggerInUse;
    private float _brakeTrigger;
    private bool _brakeTriggerInUse;

    private Transform _cameraTransform;
    private Transform _transform;
    private Rigidbody _rigidBody;
    private SphereCollider _collider;
    private RaycastHit[] _raycastArray = new RaycastHit[4];
    
    
    private Vector3 _groundNormal;

    private enum States
    {
        Grounded,
        Airborne,
        Downward
    }

    private StateMachine<States> _fsm;
    
    
    
    private void Start()
    {
        if (Camera.main != null) _cameraTransform = Camera.main.transform;
        _rigidBody = GetComponent<Rigidbody>();
        _transform = transform;
        _collider = GetComponent<SphereCollider>();
        _collider.radius = characterRadius;
        _fsm = StateMachine<States>.Initialize(this);
        _fsm.ChangeState(States.Grounded);
        _rigidBody.maxAngularVelocity = maxAngularVelocity;

    }

    private void FixedUpdate()
    {
        _playerUpVector = GetPlayerUpVector();
        _playerForwardVector = GetPlayerForwardVector();
        _previousFrameMovementDirection = _movementDirection;
        _movementDirection = GetMovementDirection();
    }

    private void Update()
    {

        UpdatePlayerVis();
        UpdateTriggerValues();
        
        if (_jumpTrigger > 0.1f && IsTouchingGround() && !_jumpTriggerInUse)
        {
                _rigidBody.AddForce(playerPhysics.initialJumpForce.GetValue(_gearChangeValue) * Vector3.up, ForceMode.Impulse);
                _jumpGas = 1f;
                _hasDoubleJump = true;
                _jumpTriggerInUse = true;
        }

        if (_jumpTrigger < 0.1f)
        {
            _jumpTriggerInUse = false;
        }

        if (_brakeTrigger > 0.1f) _fsm.ChangeState(States.Downward);
    }

    private void Grounded_Enter()
    {
        _jumpGas = 0f;
    }
    

    private void Grounded_FixedUpdate()
    {
        
        var tractionDirection = Vector3.ProjectOnPlane(_movementDirection * _movementDirection.sqrMagnitude, _groundNormal);
        
        MoveTowards(tractionDirection.normalized,playerPhysics.accelerationForce.GetValue(_gearChangeValue) * tractionDirection.magnitude );
        AddDragForce(playerPhysics.dragForce.GetValue(_gearChangeValue));
        AddCorneringForce(playerPhysics.corneringStiffness.GetValue(_gearChangeValue));
        

        AddDownForceOnButton("Jump", downForce);

            if (!IsTouchingGround())
        {
            _fsm.ChangeState(States.Airborne);
        }
    }

    private void Grounded_Update()
    {
        _gearChangeValue = Mathf.Clamp01(TabascoMaths.RemapFloatClamped(_rigidBody.velocity.magnitude,lowGearThreshold,highGearThreshold,0f,1f));
        
        if (_rigidBody.velocity.magnitude > 70f) ToggleContrails(true);
        if (_rigidBody.velocity.magnitude < 60f) ToggleContrails(false);
    }

    private void Airborne_FixedUpdate()
    {
        if (IsTouchingGround())
        {
            _fsm.ChangeState(States.Grounded);
        }
        
        MoveTowards(_movementDirection.normalized,playerPhysics.accelerationForce.GetValue(_gearChangeValue) * _movementDirection.sqrMagnitude );


            _rigidBody.AddForce(playerPhysics.sustainedJumpForce.GetValue(_gearChangeValue) * _jumpTrigger * _jumpGas * Vector3.up, ForceMode.Acceleration);
            _jumpGas -= _jumpTrigger * _jumpGas * playerPhysics.jumpDecay.GetValue(_gearChangeValue) * Time.fixedDeltaTime;
            _jumpGas = Mathf.Clamp01(_jumpGas);

            if (_jumpTrigger > 0.1f && !_jumpTriggerInUse && _hasDoubleJump)
            { 
                var rigidbodyVelocity = _rigidBody.velocity;
                _rigidBody.velocity = new Vector3(rigidbodyVelocity.x, 0f, rigidbodyVelocity.z);
                _rigidBody.AddForce(playerPhysics.initialJumpForce.GetValue(_gearChangeValue) * Vector3.up, ForceMode.Impulse);
                _rigidBody.AddTorque(_transform.forward * 200f, ForceMode.Impulse);
                _jumpGas = 1f; 
                _hasDoubleJump = false; 
                ToggleContrails(true);
        }
        
    }

    private void Downward_Enter()
    {
        playerPhysicMaterial.bounciness = 0f;
        ToggleContrails(true);
        if (!IsTouchingGround()) _rigidBody.AddForce(playerPhysics.initialJumpForce.GetValue(_gearChangeValue) * Vector3.up, ForceMode.Impulse);
        

         
    }

    private void Downward_Update()
    {
        _gearChangeValue = Mathf.Clamp01(TabascoMaths.RemapFloatClamped(_rigidBody.velocity.magnitude,lowGearThreshold,highGearThreshold,0f,1f));
        if (_brakeTrigger < 0.1f) _fsm.ChangeState(States.Grounded);
    }
    private void Downward_FixedUpdate()
    {
        if (_brakeTrigger > 0.1f) 
        {
            if (! IsTouchingGround() || _rigidBody.velocity.y < -1f)
                _rigidBody.AddForce(downForce * _brakeTrigger * Vector3.down, ForceMode.Force);
        }
        
        var tractionDirection = Vector3.ProjectOnPlane(_movementDirection * _movementDirection.sqrMagnitude, _groundNormal);
        MoveTowards(tractionDirection.normalized,playerPhysics.accelerationForce.GetValue(_gearChangeValue) * tractionDirection.magnitude );
        AddDragForce(playerPhysics.dragForce.GetValue(_gearChangeValue));
        AddCorneringForce(playerPhysics.corneringStiffness.GetValue(_gearChangeValue));
    }

    private void Downward_Exit()
    {
        playerPhysicMaterial.bounciness = defaultBouncinessValue;
        ToggleContrails(false);
    }
    
    private void MoveTowards(Vector3 direction, float amount)
    {
        var pushPosition = _transform.position + (_groundNormal * playerPhysics.accelerationHeightOffset.GetValue(_gearChangeValue));
        Debug.DrawRay(pushPosition, direction * amount, Color.red);
        var rollingResistance = -_rigidBody.velocity * playerPhysics.rollingResistanceForce.GetValue(_gearChangeValue);
        _rigidBody.AddForceAtPosition((amount * direction) + rollingResistance, pushPosition, ForceMode.Force);
    }
    

    private void RotateTowards(Vector3 desiredDirection, float force)
    {
        //is player facing forwards (1) or backwards (-1)?
        var directionFacing = Vector3.Dot(_playerForwardVector, Vector3.Cross(_transform.right, _playerUpVector));
        var amountToTurn = Vector3.Dot(desiredDirection, _transform.right) * directionFacing;

        _rigidBody.AddTorque(amountToTurn * force * _playerUpVector);

    }

    private void AddDragForce(float inputDrag)
    {
        _rigidBody.AddForce(inputDrag * _rigidBody.velocity.sqrMagnitude * -_rigidBody.velocity.normalized );
        Debug.DrawRay(_transform.position,inputDrag * _rigidBody.velocity.sqrMagnitude * -_rigidBody.velocity.normalized, Color.magenta );
    }

    private void AddCorneringForce( float stiffness)
    {
        var corneringAlpha = Vector3.Dot(_transform.right, _rigidBody.velocity.normalized);
        _rigidBody.AddForce(stiffness * corneringAlpha * _rigidBody.velocity.magnitude * _transform.right);
        Debug.DrawRay(_transform.position, stiffness * corneringAlpha * _rigidBody.velocity.magnitude * _transform.right);
    }

    private void AddDownForceOnButton(string button, float amount)
    {
        if (Input.GetButtonDown(button)) playerPhysicMaterial.bounciness = 0f;
        if (Input.GetButton(button))
        {
            if (! IsTouchingGround() || _rigidBody.velocity.y < -1f)
                _rigidBody.AddForce(Vector3.down * downForce, ForceMode.Force);
        }
        if (Input.GetButtonUp(button)) playerPhysicMaterial.bounciness = defaultBouncinessValue;

    }
    private void  UpdateTriggerValues()
    {
        _jumpTrigger = TabascoMaths.RemapFloat(Input.GetAxis("Jump"), -1f, 1f, 0f, 1f);
        _brakeTrigger = TabascoMaths.RemapFloat(Input.GetAxis("Brake"), -1f, 1f, 0f, 1f);

        if(Input.GetButton("Jump")) _jumpTrigger = 1;
        if(Input.GetButton("Brake")) _brakeTrigger = 1;
    }

    private void UpdatePlayerVis()
    {
        var targetRotation = Quaternion.LookRotation(GetPlayerForwardVector(), Vector3.up);
        turnPivot.rotation = targetRotation;
    }
    private Vector3 GetPlayerUpVector()
    {
        var leftVector = _transform.TransformVector(Vector3.left);
        return (_groundNormal - Vector3.Dot(_groundNormal, leftVector)/leftVector.sqrMagnitude * leftVector).normalized;
    }

    private Vector3 GetPlayerForwardVector()
    {
        var upVector = _playerUpVector;
        var velocity = _rigidBody.velocity.normalized;
        return (velocity - Vector3.Dot(velocity, upVector) / upVector.sqrMagnitude * upVector).normalized;
    }

    private Vector3 GetMovementDirection()
    {
        return _cameraTransform.TransformDirection(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));
    }

    private bool IsTouchingGround()
    {
        return Physics.RaycastNonAlloc(_transform.position, Vector3.down, _raycastArray, 1f) > 0;
    }

    private void ToggleContrails(bool isActive)
    {
        foreach (var contrailRenderer in contrailRenderers )
        {
            contrailRenderer.emitting = isActive;
        }

        
    }
    
    private void OnCollisionStay(Collision other)
    {
        _groundNormal = Vector3.zero;
        foreach (var contactInstance in other.contacts)
        {
           _groundNormal += contactInstance.normal;
        }
        _groundNormal.Normalize();
    }
    

    private void OnGUI()
    {
        var playerDebugGuiStyle = new GUIStyle();
        playerDebugGuiStyle.fontSize = 30;
        playerDebugGuiStyle.normal.textColor = Color.white;
        
        GUI.Label(new Rect(10,10,400,100),"current state = " + _fsm.State,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,50,400,100), "gear changeValue = " +  _gearChangeValue ,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,90,400,100),"velocity = " + _rigidBody.velocity.magnitude,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,140,400,100),"Jump gas = " + _jumpGas,playerDebugGuiStyle);

        GUI.Label(new Rect(10, 190, 400, 100), "Slope amount = " + 
                                               
                                               TabascoMaths.RemapFloatClamped(1 - Vector3.Dot(_groundNormal,Vector3.up), 0.01f, 0.4f,0f,1f)
            ,playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,240,400,100),"Friction on = " + (_rigidBody.velocity.magnitude * 2f < maxAngularVelocity * 0.99f).ToString(),playerDebugGuiStyle);

        GUI.Label(new Rect(10,290,400,100),"AngularVelocity = " + _rigidBody.angularVelocity.magnitude,playerDebugGuiStyle);

        GUI.Label(new Rect(10,340,400,100),"CorneringAlpha = " + Vector3.Dot(_transform.right, _rigidBody.velocity.normalized),playerDebugGuiStyle);
        
        
        GUI.Label(new Rect(10,390,400,100),"Downforce = " + (! IsTouchingGround() || _rigidBody.velocity.y < -1f),playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,440,400,100),"Brake Value = " + _brakeTrigger, playerDebugGuiStyle);
        
        GUI.Label(new Rect(10,490,400,100),"Jump Value = " + _jumpTrigger, playerDebugGuiStyle);
    }

    private void DrawDebugForceGuides()
    {
        
        Debug.DrawRay(_transform.position,_playerUpVector,Color.blue);
        Debug.DrawRay(_transform.position, _groundNormal, Color.green);
        Debug.DrawRay(_transform.position,Vector3.Cross(_transform.right, _playerUpVector), Color.cyan);
        
        Debug.DrawRay(_transform.position, _playerForwardVector);
    }
    
}
