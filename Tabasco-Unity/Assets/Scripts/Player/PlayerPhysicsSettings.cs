﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
[CreateAssetMenu(fileName = "PlayerPhysics", menuName = "Tabasco/PlayerPhysics", order = 1)]
public class PlayerPhysicsSettings : ScriptableObject
{
    public PlayerPhysicsFloat accelerationForce;
    public PlayerPhysicsFloat accelerationHeightOffset;
    public PlayerPhysicsFloat airAccelerationForce;
    public PlayerPhysicsFloat turnForce;
    public PlayerPhysicsFloat turnDrag;
    public PlayerPhysicsFloat selfRightingForce;
    public PlayerPhysicsFloat selfRightingTorque;
    public PlayerPhysicsFloat dragForce;
    public PlayerPhysicsFloat rollingResistanceForce;
    public PlayerPhysicsFloat initialJumpForce;
    public PlayerPhysicsFloat sustainedJumpForce;   
    public PlayerPhysicsFloat jumpDecay;
    public PlayerPhysicsFloat corneringStiffness;
    public PlayerPhysicsFloat tiltStrength;
    public PlayerPhysicsFloat wobbleMagnitude;
    public PlayerPhysicsFloat wobbleFrequency;
    
    public float maxAngularVelocity = 30f;
    public float stoppedGearThreshold = 4f;
    public float lowGearThreshold = 20f;
    public float highGearThreshold = 45f;

}

[System.Serializable]
public class PlayerPhysicsFloat
{
    public float fast = 0f;
    public float slow = 0f;
    public float stopped = 0f;

    public float GetValue(float t)
    {
        if(t <= 1f) return Mathf.Lerp(stopped, slow, t);
        if(t > 1f && t < 2f) return Mathf.Lerp(slow, fast, t-1f);
        return fast;
    }
}
