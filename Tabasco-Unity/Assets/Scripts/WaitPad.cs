﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitPad : MonoBehaviour
{

    public SpriteRenderer sprite;
    public GameObject target;
    public GameObject occupier;
    public Color unoccupiedColor;
    public Color occupiedColor;
    public Color activeColor;
    private SphereCollider _sphereCollider;


    private void Start()
    {
        //_sphereCollider = GetComponent<SphereCollider>();
        sprite.color = unoccupiedColor;
    }
    public void AddOccupier(GameObject objectToAdd)
    {
        //_sphereCollider.enabled = false;
        occupier = objectToAdd;
        sprite.color = activeColor;
    }

    public void OccupierOnPad(bool isTrue)
    {
        if (isTrue) sprite.color = occupiedColor;
        else if (occupier != null) sprite.color = activeColor;
        else sprite.color = unoccupiedColor;
    }


    public void RemoveOccupier()
    {
        //_sphereCollider.enabled = true;
        occupier = null;
        sprite.color = unoccupiedColor;
    }
}


