﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachToGround : MonoBehaviour
{
    private RaycastHit[] _raycastArray = new RaycastHit[1];
    private ConfigurableJoint _configurableJoint;
    void Awake()
    {
        _configurableJoint = GetComponent<ConfigurableJoint>();
        if (Physics.RaycastNonAlloc(transform.position + (Vector3.up * 10f), Vector3.down, _raycastArray, 100f) > 0)
        {
            _configurableJoint.connectedAnchor = _raycastArray[0].point;
        }

        transform.localScale = Vector3.one * Random.Range(1.5f, 2.5f);
    }
}
