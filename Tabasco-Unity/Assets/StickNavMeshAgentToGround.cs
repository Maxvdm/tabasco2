﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StickNavMeshAgentToGround : MonoBehaviour
{
    private RaycastHit[] _raycastArray = new RaycastHit[1];
    private Transform _transform;
    public LayerMask layerMask;
    public float offset = 1f;
    void Start()
    {
        _transform = transform;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        var position = _transform.position;
        if (Physics.RaycastNonAlloc(position + (Vector3.up * 10f), Vector3.down, _raycastArray, 12f,layerMask) > 0)
        {
            
            position = new Vector3(position.x, _raycastArray[0].point.y + offset,
                position.z);
            _transform.position = position;
        }
    }
}
