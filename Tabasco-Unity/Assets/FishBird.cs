﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishBird : MonoBehaviour
{
    public Transform target;
    public float rotateSpeedMin = 0.8f;
    public float rotateSpeedMax = 10f;
    private float _rotateSpeed;
    public float moveSpeedMin = 0.1f;
    public float moveSpeedMax = 1f; 
    private float _moveSpeed;
    private Transform _transform;
    void Start()
    {
        _transform = transform;
        _rotateSpeed = Random.Range(rotateSpeedMin, rotateSpeedMax);
        _moveSpeed = Random.Range(moveSpeedMin, moveSpeedMax);
    }

    
    void Update()
    {
        _transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(target.position - _transform.position), _rotateSpeed);
        _transform.position += _transform.forward * _moveSpeed;
    }
}
