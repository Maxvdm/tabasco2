﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.iOS;
using UnityEngine;
using UnityEngine.iOS;

public class InitializeGame : MonoBehaviour
{
    public GameObject touchControls;
    void Start()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            touchControls.SetActive(true);
            if (Device.generation == DeviceGeneration.iPhoneX || Device.generation == DeviceGeneration.iPhoneXR || Device.generation == DeviceGeneration.iPhoneXS)
            {
                Screen.SetResolution(1827, 844, true);
            }
            else if (Device.generation == DeviceGeneration.iPadPro1Gen ||
                     Device.generation == DeviceGeneration.iPadPro2Gen ||
                     Device.generation == DeviceGeneration.iPadPro3Gen ||
                     Device.generation == DeviceGeneration.iPadPro11Inch ||
                     Device.generation == DeviceGeneration.iPadPro10Inch1Gen ||
                     Device.generation == DeviceGeneration.iPadPro10Inch2Gen)
            {
                Screen.SetResolution(1440, 1080, true);
            }

        }
        Application.targetFrameRate = 60;
        Cursor.visible = false;
        
    }
}
