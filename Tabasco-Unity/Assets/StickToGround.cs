﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickToGround : MonoBehaviour
{
    private RaycastHit[] _raycastArray = new RaycastHit[1];
    void Start()
    {
        
    }

    void LateUpdate()
    {
        if (Physics.RaycastNonAlloc(transform.position + (Vector3.up * 10f), Vector3.down, _raycastArray, 100f) > 0)
        {
            transform.position = _raycastArray[0].point;
        }
    }
}
