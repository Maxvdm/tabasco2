﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGuider : MonoBehaviour
{
    public float xAmount = 10f;
    public float yAmount = 10f;
    private Transform _transform;
    private Transform _parent;
    private Transform _mainCam;

    void Start()
    {
        _transform = transform;
        if (Camera.main != null) _mainCam = Camera.main.transform;
        _parent = _transform.parent;
    }
    
    void Update()
    {
        
        _transform.position =  _parent.position + Vector3.ProjectOnPlane(_mainCam.TransformDirection(Input.GetAxis("Right Stick X") * xAmount, Input.GetAxis("Right Stick Y") * yAmount, 0f), _mainCam.forward);
    }
}
