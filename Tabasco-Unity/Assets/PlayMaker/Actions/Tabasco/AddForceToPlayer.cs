using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tabasco")]
	public class AddForceToPlayer :  FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(PlayerCharacter))]
		public FsmOwnerDefault target;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("A Vector3 force to add. Optionally override any axis with the X, Y, Z parameters.")]
		public FsmVector3 vector;
		

		[Tooltip("Repeat every frame while the state is active.")]
		public bool everyFrame;

		
		public override void OnEnter()
		{
			DoAddForce();
			
			if (!everyFrame)
			{
				Finish();
			}		
		}

		public override void OnFixedUpdate()
		{
			DoAddForce();
		}


		void DoAddForce()
		{
			target.GameObject.Value.GetComponent<PlayerCharacter>().AddExternalForce(vector.Value);
		}


	}

}