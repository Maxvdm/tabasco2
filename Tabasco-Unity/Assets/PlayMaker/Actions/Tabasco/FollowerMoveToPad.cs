using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tabasco")]
	public class FollowerMoveToPad : FsmStateAction
	{
		public FsmGameObject target;
		//public FsmEvent onSuccess;
		public FsmEvent onFailure;
		
		private WaitPad _waitPad;

		public override void OnEnter()
		{
			//Debug.Log(target.Value);

			
			if (target.Value.GetComponent<WaitPad>() != null)
			{
				_waitPad = target.Value.GetComponent<WaitPad>();
				if (_waitPad.occupier == null || _waitPad.occupier == Owner.gameObject)
				{
					_waitPad.AddOccupier(Owner.gameObject);
					Owner.GetComponent<NavMeshAgent>().SetDestination(target.Value.transform.position);
				}
				else
				{
					Debug.Log(_waitPad.occupier);
					Fsm.Event(onFailure);
					Finish();
				}
			}
			else
			{
				{
					{
						Fsm.Event(onFailure);
						Finish();
					}
				}
			}
		}

		public override void OnUpdate()
		{
		}

		public override void OnExit()
		{
		}
	}
}
