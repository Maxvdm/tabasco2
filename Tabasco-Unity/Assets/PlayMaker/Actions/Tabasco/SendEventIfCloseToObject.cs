using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tabasco")]
	public class SendEventIfCloseToObject : FsmStateAction
	{
		
		public FsmEvent eventToTrigger;
		public FsmGameObject target;
		public FsmFloat distance;

		// Code that runs on entering the state.
		public override void OnEnter()
		{
			
		}

		// Code that runs every frame.
		public override void OnUpdate()
		{
			if (Vector3.Distance(Owner.transform.position, target.Value.transform.position) < distance.Value)
			{
				Fsm.Event(eventToTrigger);
			}
		}

		// Code that runs when exiting the state.
		public override void OnExit()
		{
			
		}


	}

}
