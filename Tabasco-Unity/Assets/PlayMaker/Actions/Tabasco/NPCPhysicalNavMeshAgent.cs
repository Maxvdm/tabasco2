using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tabasco")]
	public class SetNavMeshAgentActive : FsmStateAction
	{
		public FsmBool isEnabled;
		private NavMeshAgent _navMeshAgent;


		// Code that runs on entering the state.
		public override void OnEnter()
		{
			_navMeshAgent = Owner.GetComponent<NavMeshAgent>();
			_navMeshAgent.enabled = isEnabled.Value;
		}

	}

}
