using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tabasco")]
	[Tooltip("Try to move a Tabasco NPC to a point in space")]
	public class NPCMoveToPoint : FsmStateAction
	{
		public FsmGameObject target;
		public bool usePosition;
		public FsmVector3 inputPosition;
		private Animator _animator;
		private NavMeshAgent _navMeshAgent;
		public FsmFloat distanceToKeep;
		public bool repelOnly = false;
		public bool everyFrame = true;
		public int updateEveryFrame;
		public string pathStatus;
		public float targetMovedThreshold = 2f;
		public float distanceToRecalculate = 2f;
		private int updateFrame;
		public bool debuglog;
		
		
		private RaycastHit[] _raycastArray = new RaycastHit[1];
			
		public override void OnEnter()
		{
			_animator = Owner.GetComponent<Animator>();
			_navMeshAgent = Owner.GetComponent<NavMeshAgent>();
			if (!everyFrame)
			{
				OnUpdate();
				Finish();
			}
		}

		public override void OnUpdate()
		{
			pathStatus = "";
			//pathStatus = _navMeshAgent.remainingDistance.ToString();

			
			

			if (!_navMeshAgent.isOnNavMesh) 
				return;
			
			if (Vector3.Distance(_navMeshAgent.destination, target.Value.transform.position) < targetMovedThreshold && 
			    Vector3.Distance(Owner.transform.position, target.Value.transform.position) > distanceToRecalculate) 
				return;
			
			
			if (updateFrame >= updateEveryFrame && !_navMeshAgent.pathPending && _navMeshAgent.pathStatus != NavMeshPathStatus.PathPartial)
			{
				if(debuglog) Debug.Log("new path");
				if(debuglog) Debug.Log(Vector3.Distance(_navMeshAgent.destination, target.Value.transform.position));
				if(debuglog) Debug.Log(Vector3.Distance(Owner.transform.position, target.Value.transform.position));
				if(debuglog) Debug.Log(_navMeshAgent.pathStatus);
				
				
				if (!usePosition)
				{
					var npcPosition = Owner.transform.position;
					var playerPosition = target.Value.transform.position;

					if (Physics.RaycastNonAlloc(target.Value.transform.position + (Vector3.up * 10f), Vector3.down,
						    _raycastArray, 300f) > 0)
					{
						Debug.DrawLine(target.Value.transform.position, _raycastArray[0].point, Color.red);
						playerPosition = _raycastArray[0].point;
					}
					_navMeshAgent.destination = playerPosition;
					Debug.DrawLine(npcPosition, playerPosition, Color.red);
				}
				else
				{
					_navMeshAgent.SetDestination(inputPosition.Value);
				}

				updateFrame = 0;
			}
			else updateFrame++;
			
			


		}

		public override void OnFixedUpdate()
		{
		}
	}

}
