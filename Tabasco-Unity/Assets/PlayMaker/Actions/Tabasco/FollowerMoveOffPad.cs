using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tabasco")]
	public class FollowerMoveOffPad : FsmStateAction
	{
		public FsmGameObject target;
		
		private WaitPad _waitPad;
		
		
		public override void OnEnter()
		{
			
			if (_waitPad != null && _waitPad.occupier == Owner.gameObject)
			{
				_waitPad.RemoveOccupier();
			}
			target.Value = null;

			Finish();
		}


	}

}
