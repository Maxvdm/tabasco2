using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tabasco")]
	public class CleanWaitPad : FsmStateAction
	{
		private WaitPad _waitPad;

		// Code that runs on entering the state.
		public override void OnEnter()
		{
			_waitPad = Fsm.Owner.GetComponent<WaitPad>();
			_waitPad.RemoveOccupier();
			Finish();
		}


	}

}
