using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("AI")]
	public class SetNavMeshAgentBaseOffset : ComponentAction<NavMeshAgent>
	{
		[RequiredField]
		[CheckForComponent(typeof(NavMeshAgent))]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		public FsmFloat baseOffset;
		public bool everyFrame;
		public bool resetOnExit;
		private float defaultValue;
		
		public override void Reset()
		{
			gameObject = null;
			baseOffset = 1f;
		}

		
		public override void OnEnter()
		{
			defaultValue = GetBaseOffset();
			if (!everyFrame)
			{
				SetBaseOffset(baseOffset.Value);
				Finish();	
			}
		}
		
		public override void OnUpdate()
		{
			SetBaseOffset(baseOffset.Value);
		}
		
		public override void OnExit()
		{
			if (resetOnExit) SetBaseOffset(defaultValue);
		}

		private void SetBaseOffset(float value)
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				navMeshAgent.baseOffset = value;
			}
		}
		
		private float GetBaseOffset()
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				return navMeshAgent.baseOffset;
			}
			return 0f;
		}


	}

}
