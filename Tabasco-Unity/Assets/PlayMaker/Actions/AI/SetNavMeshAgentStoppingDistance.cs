using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("AI")]
	public class SetNavMeshAgentStoppingDistance :  ComponentAction<NavMeshAgent>
	{
		
		[RequiredField]
		[CheckForComponent(typeof(NavMeshAgent))]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		public FsmFloat stoppingDistance;
		public bool everyFrame;
		public bool resetOnExit;
		private float defaultValue;


		public override void Reset()
		{
			gameObject = null;
			stoppingDistance = 1f;
		}

		public override void OnEnter()
		{
			defaultValue = GetStoppingDistance();
			if (!everyFrame)
			{
				SetStoppingDistance(stoppingDistance.Value);
				Finish();
			}
		}
		
		public override void OnUpdate()
		{
			SetStoppingDistance(stoppingDistance.Value);
		}
		
		public override void OnExit()
		{
			if (resetOnExit) SetStoppingDistance(defaultValue);
		}

		private void SetStoppingDistance(float value)
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				navMeshAgent.stoppingDistance = value;
			}
		}
		
		private float GetStoppingDistance()
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				return navMeshAgent.stoppingDistance;
			}
			return 0f;
		}

	}

}
