using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("AI")]
	public class SetNavMeshAgentAutoBreaking :  ComponentAction<NavMeshAgent>
	{
		[RequiredField]
		[CheckForComponent(typeof(NavMeshAgent))]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		public FsmBool autoBreaking;
		public bool everyFrame;
		public bool resetOnExit;
		private bool defaultValue;


		public override void Reset()
		{
			gameObject = null;
			autoBreaking = true;
		}

		public override void OnEnter()
		{
			defaultValue = GetAutoBreaking();
			if (!everyFrame)
			{
				SetAutoBreaking(autoBreaking.Value);
			}
		}
		
		public override void OnUpdate()
		{
			SetAutoBreaking(autoBreaking.Value);
		}
		
		public override void OnExit()
		{
			if (resetOnExit) SetAutoBreaking(defaultValue);
		}

		private void SetAutoBreaking(bool value)
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				navMeshAgent.autoBraking = value;
			}
		}
		
		private bool GetAutoBreaking()
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				return navMeshAgent.autoBraking;
			}
			return true;
		}

	}

}
