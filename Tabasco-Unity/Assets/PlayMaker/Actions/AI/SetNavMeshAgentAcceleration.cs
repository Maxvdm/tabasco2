using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("AI")]
	public class SetNavMeshAgentAcceleration : ComponentAction<NavMeshAgent>
	{
		
		[RequiredField]
		[CheckForComponent(typeof(NavMeshAgent))]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		public FsmFloat acceleration;
		public bool everyFrame;
		public bool resetOnExit;
		private float defaultValue;

		
		public override void Reset()
		{
			gameObject = null;
			acceleration = 1f;
		}
		
		public override void OnEnter()
		{
			defaultValue = GetAcceleration();
			if (!everyFrame)
			{
				SetAcceleration(acceleration.Value);
				Finish();
			}

		}
		
		public override void OnUpdate()
		{
			SetAcceleration(acceleration.Value);
		}
		
		public override void OnExit()
		{
			if (resetOnExit) SetAcceleration(defaultValue);
		}

		private void SetAcceleration(float value)
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				navMeshAgent.acceleration = value;
			}
		}
		
		private float GetAcceleration()
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				return navMeshAgent.acceleration;
			}
			return 0f;
		}

	}

}
