using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("AI")]
	public class SetNavMeshAgentSpeed : ComponentAction<NavMeshAgent>
	{
		[RequiredField]
		[CheckForComponent(typeof(NavMeshAgent))]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		public FsmFloat speed;
		public bool everyFrame;
		public bool resetOnExit;
		private float defaultValue;
		
		public override void Reset()
		{
			gameObject = null;
			speed = 1f;
		}

		
		public override void OnEnter()
		{
			defaultValue = GetSpeed();
			if (!everyFrame)
			{
				SetSpeed(speed.Value);
				Finish();	
			}
		}
		
		public override void OnUpdate()
		{
			SetSpeed(speed.Value);
		}
		
		public override void OnExit()
		{
			if (resetOnExit) SetSpeed(defaultValue);
		}

		private void SetSpeed(float value)
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				navMeshAgent.speed = value;
			}
		}
		
		private float GetSpeed()
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				return navMeshAgent.speed;
			}
			return 0f;
		}


	}

}
