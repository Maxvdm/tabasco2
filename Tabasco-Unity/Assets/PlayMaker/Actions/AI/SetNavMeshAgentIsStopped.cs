using UnityEngine;
using UnityEngine.AI;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("AI")]
	public class SetNavMeshAgentIsStopped :  ComponentAction<NavMeshAgent>
	{
		
		[RequiredField]
		[CheckForComponent(typeof(NavMeshAgent))]
		public FsmOwnerDefault gameObject;
		
		[RequiredField]
		public FsmBool isStopped;
		private float defaultValue;


		public override void Reset()
		{
			gameObject = null;
			isStopped = false;
		}

		public override void OnEnter()
		{
			var tempGameObject = Fsm.GetOwnerDefaultTarget(this.gameObject);
			if (UpdateCache(tempGameObject))
			{
				navMeshAgent.isStopped = isStopped.Value;
			}
			Finish();
		}
		
		public override void OnUpdate()
		{
		}
		
		public override void OnExit()
		{
			Finish();
		}

	}

}
