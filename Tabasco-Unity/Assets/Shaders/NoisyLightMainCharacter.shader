// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/NoisyLightMainCharacter"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_LightGradient("Light Gradient", 2D) = "white" {}
		_Ambient("Ambient", Color) = (0,0,0,0)
		_GIColor("GI Color", Color) = (0,0,0,0)
		_NoiseMultiplier("Noise Multiplier", Range( -20 , 20)) = 0
		_FresnelColor("Fresnel Color", Color) = (1,1,1,0)
		_FresnelScale("Fresnel Scale", Float) = 0
		_FresnelPower("Fresnel Power", Float) = 0
		_DarkFresnelColor("Dark Fresnel Color", Color) = (1,1,1,0)
		_DarkFresnelScale("Dark Fresnel Scale", Float) = 0
		_DarkFresnelPower("Dark Fresnel Power", Float) = 0
		_PointLightHardness("Point Light Hardness", Range( 0 , 1)) = 0
		_AlbedoColor("Albedo Color", Color) = (0,0,0,0)
		[HDR]_SpecularColor("Specular Color", Color) = (0,0,0,0)
		_NoiseTexture("Noise Texture", 2D) = "white" {}
		_NoiseTiling("Noise Tiling", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#define ASE_TEXTURE_PARAMS(textureName) textureName

		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
			float2 uv_texcoord;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _DarkFresnelScale;
		uniform float _DarkFresnelPower;
		uniform float4 _DarkFresnelColor;
		uniform sampler2D _LightGradient;
		uniform float _PointLightHardness;
		uniform sampler2D _NoiseTexture;
		uniform float _NoiseTiling;
		uniform float _NoiseMultiplier;
		uniform float4 _AlbedoColor;
		uniform float _FresnelScale;
		uniform float _FresnelPower;
		uniform float4 _FresnelColor;
		uniform float4 _Ambient;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _GIColor;
		uniform float4 _SpecularColor;


		inline float4 TriplanarSamplingSF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode40_g36 = tex2D( _Albedo, uv_Albedo );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float dotResult6_g36 = dot( ase_worldlightDir , ase_normWorldNormal );
			float lerpResult53_g36 = lerp( ( ase_lightColor.a * dotResult6_g36 * ase_lightAtten ) , ( ase_lightAtten * ( ( ( dotResult6_g36 * _PointLightHardness ) + 1.0 ) - _PointLightHardness ) ) , _WorldSpaceLightPos0.w);
			float temp_output_14_0_g36 = saturate( lerpResult53_g36 );
			float2 appendResult33 = (float2(_NoiseTiling , _NoiseTiling));
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float4 triplanar6 = TriplanarSamplingSF( _NoiseTexture, ase_vertex3Pos, ase_vertexNormal, 1.0, appendResult33, 1.0, 0 );
			float temp_output_50_0_g36 = triplanar6.x;
			float temp_output_12_0_g36 = distance( lerpResult53_g36 , 0.5 );
			float lerpResult13_g36 = lerp( ( (-0.2 + (temp_output_50_0_g36 - 0.0) * (0.2 - -0.2) / (1.0 - 0.0)) * _NoiseMultiplier ) , 0.0 , temp_output_12_0_g36);
			float temp_output_16_0_g36 = saturate( ( temp_output_14_0_g36 + lerpResult13_g36 ) );
			float2 appendResult21_g36 = (float2(( 1.0 - temp_output_16_0_g36 ) , 0.0));
			float4 tex2DNode25_g36 = tex2D( _LightGradient, appendResult21_g36 );
			float lerpResult57_g36 = lerp( tex2DNode25_g36.r , temp_output_16_0_g36 , _WorldSpaceLightPos0.w);
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 normalizeResult4_g35 = normalize( ( ase_worldViewDir + ase_worldlightDir ) );
			float dotResult116 = dot( ase_worldNormal , normalizeResult4_g35 );
			float dotResult120 = dot( ase_worldNormal , ase_worldlightDir );
			c.rgb = ( ( _AlbedoColor * ( tex2DNode40_g36 * ( float4( ( lerpResult57_g36 * ase_lightColor.rgb ) , 0.0 ) + ( float4( 0,0,0,0 ) * _GIColor ) ) ) ) + saturate( ( pow( dotResult116 , 10.0 ) * dotResult120 * _SpecularColor * ase_lightAtten ) ) ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV27_g36 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode27_g36 = ( 0.0 + _DarkFresnelScale * pow( 1.0 - fresnelNdotV27_g36, _DarkFresnelPower ) );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float dotResult6_g36 = dot( ase_worldlightDir , ase_normWorldNormal );
			float lerpResult53_g36 = lerp( ( ase_lightColor.a * dotResult6_g36 * 1 ) , ( 1 * ( ( ( dotResult6_g36 * _PointLightHardness ) + 1.0 ) - _PointLightHardness ) ) , _WorldSpaceLightPos0.w);
			float temp_output_14_0_g36 = saturate( lerpResult53_g36 );
			float2 appendResult33 = (float2(_NoiseTiling , _NoiseTiling));
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float4 triplanar6 = TriplanarSamplingSF( _NoiseTexture, ase_vertex3Pos, ase_vertexNormal, 1.0, appendResult33, 1.0, 0 );
			float temp_output_50_0_g36 = triplanar6.x;
			float temp_output_12_0_g36 = distance( lerpResult53_g36 , 0.5 );
			float lerpResult13_g36 = lerp( ( (-0.2 + (temp_output_50_0_g36 - 0.0) * (0.2 - -0.2) / (1.0 - 0.0)) * _NoiseMultiplier ) , 0.0 , temp_output_12_0_g36);
			float temp_output_16_0_g36 = saturate( ( temp_output_14_0_g36 + lerpResult13_g36 ) );
			float2 appendResult21_g36 = (float2(( 1.0 - temp_output_16_0_g36 ) , 0.0));
			float4 tex2DNode25_g36 = tex2D( _LightGradient, appendResult21_g36 );
			float temp_output_55_0_g36 = ( 1.0 - _WorldSpaceLightPos0.w );
			float4 temp_output_44_0_g36 = ( saturate( fresnelNode27_g36 ) * _DarkFresnelColor * ( 1.0 - tex2DNode25_g36.r ) * temp_output_50_0_g36 * temp_output_55_0_g36 );
			o.Albedo = ( temp_output_44_0_g36 * _AlbedoColor ).rgb;
			float fresnelNdotV24_g36 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode24_g36 = ( 0.0 + _FresnelScale * pow( 1.0 - fresnelNdotV24_g36, _FresnelPower ) );
			float4 temp_output_34_0_g36 = ( temp_output_50_0_g36 * saturate( fresnelNode24_g36 ) * temp_output_14_0_g36 * tex2DNode25_g36.r * _FresnelColor * temp_output_55_0_g36 );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode40_g36 = tex2D( _Albedo, uv_Albedo );
			float4 temp_output_93_0 = ( temp_output_34_0_g36 + ( temp_output_34_0_g36 * ( pow( temp_output_50_0_g36 , 9.4 ) * 0.0 ) ) + ( _Ambient * tex2DNode40_g36 ) + temp_output_44_0_g36 );
			o.Emission = temp_output_93_0.rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
317;626;1355;423;1179.234;92.53051;2.063267;True;True
Node;AmplifyShaderEditor.WorldNormalVector;117;-203.3848,67.16628;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;111;-207.5624,265.3577;Float;True;Blinn-Phong Half Vector;-1;;35;91a149ac9d615be429126c95e20753ce;0;0;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;34;-1111.622,-183.3489;Float;False;Property;_NoiseTiling;Noise Tiling;16;0;Create;True;0;0;False;0;0;0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;116;99.40955,97.96293;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;119;-107.8381,515.7887;Float;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DynamicAppendNode;33;-739.0416,-64.46838;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TriplanarNode;6;-349.405,-156.7495;Float;True;Spherical;Object;False;Noise Texture;_NoiseTexture;white;15;Assets/Textures/Oasis/voronoi_random.png;Mid Texture 0;_MidTexture0;white;0;None;Bot Texture 0;_BotTexture0;white;1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.2,0.2;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;122;465.1904,547.0822;Float;False;Property;_SpecularColor;Specular Color;14;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightAttenuation;126;-131.0941,450.1087;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;112;344.4441,104.3933;Float;True;2;0;FLOAT;0;False;1;FLOAT;10;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;120;324.7747,398.4379;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;93;186.7396,-144.0692;Float;False;NoisyLightFunction;0;;36;dae7fed4b42bf4600bbf48ff5aa49aa0;0;1;50;FLOAT;0;False;3;COLOR;48;COLOR;0;COLOR;49
Node;AmplifyShaderEditor.ColorNode;123;700.4027,-203.9469;Float;False;Property;_AlbedoColor;Albedo Color;13;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;121;744.8836,309.4897;Float;False;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;115;701.7302,202.9781;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;125;677.707,-36.82226;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FogAndAmbientColorsNode;102;229.9107,-566.7441;Float;False;unity_AmbientSky;0;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;97;-39.60546,-439.8479;Float;False;5;0;FLOAT;0;False;1;FLOAT;481.73;False;2;FLOAT;129.5;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FogParamsNode;104;-204.9717,-600.5573;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;96;833.5598,-405.1028;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldPosInputsNode;95;-344.8972,-426.0906;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FogAndAmbientColorsNode;100;208.7613,-459.4866;Float;False;unity_AmbientGround;0;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;99;333.529,-359.7827;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;94;542.2638,-311.316;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;976.8802,-366.9449;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;101;601.5347,-506.3173;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;113;819.1332,60.72305;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1092.193,-307.5779;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;Tabasco/NoisyLightMainCharacter;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;116;0;117;0
WireConnection;116;1;111;0
WireConnection;33;0;34;0
WireConnection;33;1;34;0
WireConnection;6;3;33;0
WireConnection;112;0;116;0
WireConnection;120;0;117;0
WireConnection;120;1;119;0
WireConnection;93;50;6;1
WireConnection;121;0;112;0
WireConnection;121;1;120;0
WireConnection;121;2;122;0
WireConnection;121;3;126;0
WireConnection;115;0;121;0
WireConnection;125;0;123;0
WireConnection;125;1;93;49
WireConnection;97;0;95;2
WireConnection;96;0;101;0
WireConnection;96;1;94;0
WireConnection;99;0;97;0
WireConnection;94;0;101;0
WireConnection;94;1;93;0
WireConnection;124;0;93;48
WireConnection;124;1;123;0
WireConnection;101;0;102;0
WireConnection;101;1;100;0
WireConnection;101;2;99;0
WireConnection;113;0;125;0
WireConnection;113;1;115;0
WireConnection;0;0;124;0
WireConnection;0;2;93;0
WireConnection;0;13;113;0
ASEEND*/
//CHKSM=69C5C894B904E28A201AF5AA4F69D4267961C76F