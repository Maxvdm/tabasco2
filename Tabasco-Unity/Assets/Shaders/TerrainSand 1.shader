// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/TerrainSand"
{
	Properties
	{
		_NoiseMultiplier("Noise Multiplier", Float) = 1
		_TerrainSplatFalloff("TerrainSplatFalloff", Float) = 1
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector]_Control("Control", 2D) = "white" {}
		[HideInInspector]_Splat3("Splat3", 2D) = "white" {}
		[HideInInspector]_Splat2("Splat2", 2D) = "white" {}
		[HideInInspector]_Splat1("Splat1", 2D) = "white" {}
		[HideInInspector]_Splat0("Splat0", 2D) = "white" {}
		[HideInInspector]_Smoothness3("Smoothness3", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness1("Smoothness1", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness0("Smoothness0", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness2("Smoothness2", Range( 0 , 1)) = 1
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_Color1("Color 1", Color) = (0,0,0,0)
		_Float10("Float 10", Range( -1 , 1)) = 0.8293508
		[HDR]_EmissiveTint("EmissiveTint", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry-100" "IsEmissive" = "true"  "SplatCount"="4" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		#ifdef UNITY_INSTANCING_ENABLED//ASE Terrain Instancing
			sampler2D _TerrainHeightmapTexture;//ASE Terrain Instancing
			sampler2D _TerrainNormalmapTexture;//ASE Terrain Instancing
		#endif//ASE Terrain Instancing
		UNITY_INSTANCING_BUFFER_START( Terrain )//ASE Terrain Instancing
			UNITY_DEFINE_INSTANCED_PROP( float4, _TerrainPatchInstanceData )//ASE Terrain Instancing
		UNITY_INSTANCING_BUFFER_END( Terrain)//ASE Terrain Instancing
		CBUFFER_START( UnityTerrain)//ASE Terrain Instancing
			#ifdef UNITY_INSTANCING_ENABLED//ASE Terrain Instancing
				float4 _TerrainHeightmapRecipSize;//ASE Terrain Instancing
				float4 _TerrainHeightmapScale;//ASE Terrain Instancing
			#endif//ASE Terrain Instancing
		CBUFFER_END//ASE Terrain Instancing
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float _NoiseMultiplier;
		uniform sampler2D _Control;
		uniform float4 _Control_ST;
		uniform float _TerrainSplatFalloff;
		uniform float _Smoothness0;
		uniform sampler2D _Splat0;
		uniform float4 _Splat0_ST;
		uniform float _Smoothness1;
		uniform sampler2D _Splat1;
		uniform float4 _Splat1_ST;
		uniform float _Smoothness2;
		uniform sampler2D _Splat2;
		uniform float4 _Splat2_ST;
		uniform float _Smoothness3;
		uniform sampler2D _Splat3;
		uniform float4 _Splat3_ST;
		uniform float4 _EmissiveTint;
		uniform sampler2D _TextureSample2;
		uniform float _Float10;
		uniform float4 _Color1;


		void ApplyMeshModification( inout appdata_full v )
		{
			#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
				float2 patchVertex = v.vertex.xy;
				float4 instanceData = UNITY_ACCESS_INSTANCED_PROP(Terrain, _TerrainPatchInstanceData);
				
				float4 uvscale = instanceData.z * _TerrainHeightmapRecipSize;
				float4 uvoffset = instanceData.xyxy * uvscale;
				uvoffset.xy += 0.5f * _TerrainHeightmapRecipSize.xy;
				float2 sampleCoords = (patchVertex.xy * uvscale.xy + uvoffset.xy);
				
				float hm = UnpackHeightmap(tex2Dlod(_TerrainHeightmapTexture, float4(sampleCoords, 0, 0)));
				v.vertex.xz = (patchVertex.xy + instanceData.xy) * _TerrainHeightmapScale.xz * instanceData.z;
				v.vertex.y = hm * _TerrainHeightmapScale.y;
				v.vertex.w = 1.0f;
				
				v.texcoord.xy = (patchVertex.xy * uvscale.zw + uvoffset.zw);
				v.texcoord3 = v.texcoord2 = v.texcoord1 = v.texcoord;
				
				#ifdef TERRAIN_INSTANCED_PERPIXEL_NORMAL
					v.normal = float3(0, 1, 0);
					//data.tc.zw = sampleCoords;
				#else
					float3 nor = tex2Dlod(_TerrainNormalmapTexture, float4(sampleCoords, 0, 0)).xyz;
					v.normal = 2.0f * nor - 1.0f;
				#endif
			#endif
		}


		float4 ClampColors7_g19( float4 RGBA , float smoothness )
		{
			float4 result = (0,0,0,0);
						
			float4 tempThreshold = max(max(RGBA.b,RGBA.g),RGBA.a);
			result.r = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.r);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.a);
			result.b = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.b);
			            
			tempThreshold = max(max(RGBA.b,RGBA.r),RGBA.a);
			result.g = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.g);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.b);
			result.a = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.a);
			            
			return result;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			ApplyMeshModification(v);;
			float localCalculateTangentsStandard16_g18 = ( 0.0 );
			v.tangent.xyz = cross ( v.normal, float3( 0, 0, 1 ) );
			v.tangent.w = -1;
			v.vertex.xyz += localCalculateTangentsStandard16_g18;
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode3_g19 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 _Vector3 = float4(1,1,1,1);
			float2 uv_Control = i.uv_texcoord * _Control_ST.xy + _Control_ST.zw;
			float4 tex2DNode5_g18 = tex2D( _Control, uv_Control );
			float dotResult20_g18 = dot( tex2DNode5_g18 , float4(1,1,1,1) );
			float SplatWeight22_g18 = dotResult20_g18;
			float localSplatClip74_g18 = ( SplatWeight22_g18 );
			float SplatWeight74_g18 = SplatWeight22_g18;
			#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
				clip(SplatWeight74_g18 == 0.0f ? -1 : 1);
			#endif
			float4 temp_cast_20 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g19 = normalize( ( ( (float4(-1,-1,-1,-1) + (tex2DNode3_g19 - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _NoiseMultiplier ) + pow( ( tex2DNode5_g18 / ( localSplatClip74_g18 + 0.001 ) ) , temp_cast_20 ) ) );
			float4 RGBA7_g19 = normalizeResult8_g19;
			float smoothness7_g19 = 0.1;
			float4 localClampColors7_g19 = ClampColors7_g19( RGBA7_g19 , smoothness7_g19 );
			float4 SplatControl26_g18 = localClampColors7_g19;
			float4 temp_output_59_0_g18 = SplatControl26_g18;
			float4 appendResult33_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness0));
			float2 uv_Splat0 = i.uv_texcoord * _Splat0_ST.xy + _Splat0_ST.zw;
			float4 appendResult36_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness1));
			float2 uv_Splat1 = i.uv_texcoord * _Splat1_ST.xy + _Splat1_ST.zw;
			float4 appendResult39_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness2));
			float2 uv_Splat2 = i.uv_texcoord * _Splat2_ST.xy + _Splat2_ST.zw;
			float4 appendResult42_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness3));
			float2 uv_Splat3 = i.uv_texcoord * _Splat3_ST.xy + _Splat3_ST.zw;
			float4 weightedBlendVar9_g18 = temp_output_59_0_g18;
			float4 weightedBlend9_g18 = ( weightedBlendVar9_g18.x*( appendResult33_g18 * tex2D( _Splat0, uv_Splat0 ) ) + weightedBlendVar9_g18.y*( appendResult36_g18 * tex2D( _Splat1, uv_Splat1 ) ) + weightedBlendVar9_g18.z*( appendResult39_g18 * tex2D( _Splat2, uv_Splat2 ) ) + weightedBlendVar9_g18.w*( appendResult42_g18 * tex2D( _Splat3, uv_Splat3 ) ) );
			float4 MixDiffuse28_g18 = weightedBlend9_g18;
			float4 temp_output_78_0 = MixDiffuse28_g18;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float dotResult143 = dot( ase_worldlightDir , ase_normWorldNormal );
			float temp_output_146_0 = ( ase_lightColor.a * ase_lightAtten * dotResult143 );
			float4 temp_output_78_106 = tex2DNode3_g19;
			float4 lerpResult150 = lerp( ( (float4( -1,0,0,0 ) + (temp_output_78_106 - float4( 0,0,0,0 )) * (float4( 1,0,0,0 ) - float4( -1,0,0,0 )) / (float4( 1,0,0,0 ) - float4( 0,0,0,0 ))) * _Float10 ) , float4( 0,0,0,0 ) , distance( temp_output_146_0 , 0.5 ));
			float2 appendResult131 = (float2(( 1.0 - saturate( ( saturate( temp_output_146_0 ) + lerpResult150 ) ) ).rg));
			Unity_GlossyEnvironmentData g133 = UnityGlossyEnvironmentSetup( -0.27, data.worldViewDir, ase_worldNormal, float3(0,0,0));
			float3 indirectSpecular133 = UnityGI_IndirectSpecular( data, 1.0, ase_worldNormal, g133 );
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float fresnelNdotV167 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode167 = ( 0.0 + 0.97 * pow( 1.0 - fresnelNdotV167, 13.43 ) );
			float temp_output_170_0 = ( temp_output_78_106.r * saturate( fresnelNode167 ) );
			float3 normalizeResult4_g20 = normalize( ( ase_worldViewDir + ase_worldlightDir ) );
			float dotResult159 = dot( ase_normWorldNormal , normalizeResult4_g20 );
			c.rgb = ( ( temp_output_78_0 * ( ( tex2D( _TextureSample2, appendResult131 ) * float4( ase_lightColor.rgb , 0.0 ) ) + ( float4( indirectSpecular133 , 0.0 ) * _Color1 ) ) ) + temp_output_170_0 + ( temp_output_170_0 * ( pow( temp_output_78_106.r , 6.57 ) * saturate( dotResult159 ) * 14.0 ) ) ).xyz;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode3_g19 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 _Vector3 = float4(1,1,1,1);
			float2 uv_Control = i.uv_texcoord * _Control_ST.xy + _Control_ST.zw;
			float4 tex2DNode5_g18 = tex2D( _Control, uv_Control );
			float dotResult20_g18 = dot( tex2DNode5_g18 , float4(1,1,1,1) );
			float SplatWeight22_g18 = dotResult20_g18;
			float localSplatClip74_g18 = ( SplatWeight22_g18 );
			float SplatWeight74_g18 = SplatWeight22_g18;
			#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
				clip(SplatWeight74_g18 == 0.0f ? -1 : 1);
			#endif
			float4 temp_cast_5 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g19 = normalize( ( ( (float4(-1,-1,-1,-1) + (tex2DNode3_g19 - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _NoiseMultiplier ) + pow( ( tex2DNode5_g18 / ( localSplatClip74_g18 + 0.001 ) ) , temp_cast_5 ) ) );
			float4 RGBA7_g19 = normalizeResult8_g19;
			float smoothness7_g19 = 0.1;
			float4 localClampColors7_g19 = ClampColors7_g19( RGBA7_g19 , smoothness7_g19 );
			float4 SplatControl26_g18 = localClampColors7_g19;
			float4 temp_output_59_0_g18 = SplatControl26_g18;
			float4 appendResult33_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness0));
			float2 uv_Splat0 = i.uv_texcoord * _Splat0_ST.xy + _Splat0_ST.zw;
			float4 appendResult36_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness1));
			float2 uv_Splat1 = i.uv_texcoord * _Splat1_ST.xy + _Splat1_ST.zw;
			float4 appendResult39_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness2));
			float2 uv_Splat2 = i.uv_texcoord * _Splat2_ST.xy + _Splat2_ST.zw;
			float4 appendResult42_g18 = (float4(1.0 , 1.0 , 1.0 , _Smoothness3));
			float2 uv_Splat3 = i.uv_texcoord * _Splat3_ST.xy + _Splat3_ST.zw;
			float4 weightedBlendVar9_g18 = temp_output_59_0_g18;
			float4 weightedBlend9_g18 = ( weightedBlendVar9_g18.x*( appendResult33_g18 * tex2D( _Splat0, uv_Splat0 ) ) + weightedBlendVar9_g18.y*( appendResult36_g18 * tex2D( _Splat1, uv_Splat1 ) ) + weightedBlendVar9_g18.z*( appendResult39_g18 * tex2D( _Splat2, uv_Splat2 ) ) + weightedBlendVar9_g18.w*( appendResult42_g18 * tex2D( _Splat3, uv_Splat3 ) ) );
			float4 MixDiffuse28_g18 = weightedBlend9_g18;
			float4 temp_output_78_0 = MixDiffuse28_g18;
			o.Albedo = temp_output_78_0.xyz;
			o.Emission = ( _EmissiveTint * temp_output_78_0 ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
		UsePass "Hidden/Nature/Terrain/Utilities/PICKING"
		UsePass "Hidden/Nature/Terrain/Utilities/SELECTION"
	}

	Dependency "BaseMapShader"="ASESampleShaders/SimpleTerrainBase"
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
-17;501;2079;835;6035.13;582.4521;4.80129;True;True
Node;AmplifyShaderEditor.WorldNormalVector;139;-2412.467,1366.701;Float;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;140;-2407.373,1124.927;Float;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;78;-1965.03,137.2833;Float;False;Tabasco Four Splats Terrain;0;;18;a24967c16208e43af80b9a40781b13c9;0;6;59;FLOAT4;0,0,0,0;False;60;FLOAT4;0,0,0,0;False;61;FLOAT3;0,0,0;False;57;FLOAT;0;False;58;FLOAT;0;False;62;FLOAT;0;False;7;COLOR;106;FLOAT4;0;FLOAT3;14;FLOAT;56;FLOAT;45;FLOAT;19;FLOAT3;17
Node;AmplifyShaderEditor.LightColorNode;153;-2184.617,772.3428;Float;True;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.DotProductOpNode;143;-1979.983,1277.1;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;154;-2251.173,1034.579;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;145;-1848.648,2000.361;Float;False;Property;_Float10;Float 10;26;0;Create;True;0;0;False;0;0.8293508;0.191;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;146;-1746.395,1222.234;Float;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;144;-1764.179,1654.273;Float;True;5;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,0,0,0;False;3;COLOR;-1,0,0,0;False;4;COLOR;1,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;147;-1401.178,1763.014;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.DistanceOpNode;148;-1490.931,1523.838;Float;True;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;150;-1084.129,1591.784;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;149;-1364.977,1340.38;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;151;-1092.183,1339.046;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;129;-787.4925,1334.998;Float;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;130;-438.8159,1403.286;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;158;-1059.054,1928.744;Float;True;Blinn-Phong Half Vector;-1;;20;91a149ac9d615be429126c95e20753ce;0;0;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;131;-267.1194,1393.245;Float;False;FLOAT2;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FresnelNode;167;-405.4008,960.541;Float;True;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0.97;False;3;FLOAT;13.43;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;163;-523.0247,1515.178;Float;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.DotProductOpNode;159;-516.1176,1741.709;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;132;-101.3884,1365.869;Float;True;Property;_TextureSample2;Texture Sample 2;22;0;Create;True;0;0;False;0;d2112141ea18647d98b7389cd36a8e84;bde49b316487a4c61b70b807add703f9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IndirectSpecularLight;133;-599.6943,2213.62;Float;True;Tangent;3;0;FLOAT3;0,0,1;False;1;FLOAT;-0.27;False;2;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;134;-132.2486,2246.98;Float;False;Property;_Color1;Color 1;23;0;Create;True;0;0;False;0;0,0,0,0;0.9622641,0.9622641,0.9622641,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;165;-94.47041,1507.577;Float;False;2;0;FLOAT;0;False;1;FLOAT;6.57;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;169;-24.77962,966.1074;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;135;313.5902,1905.86;Float;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;164;-165.0887,1839.31;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;136;207.6359,1253.64;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;166;677.484,1672.494;Float;False;Constant;_Float0;Float 0;8;0;Create;True;0;0;False;0;14;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;170;354.2221,1060.885;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;137;560.4113,1216.737;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;162;262.5568,1527.004;Float;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;31;-356.402,-29.21287;Float;False;Property;_EmissiveTint;EmissiveTint;27;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0.3490566,0.3490566,0.3490566,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;152;599.244,748.7006;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;173;1076.316,1207.096;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;172;910.5663,1280.872;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;141;-2413.171,1695.975;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;138;-2607.319,1649.263;Float;False;Property;_Float8;Float 8;25;0;Create;True;0;0;False;0;0;0.34;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;160;-130.6528,1711.432;Float;False;2;0;FLOAT;0;False;1;FLOAT;130;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;171;789.7905,695.991;Float;False;3;3;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TriplanarNode;142;-2143.14,1554.595;Float;True;Spherical;World;False;Top Texture 3;_TopTexture3;white;24;Assets/Textures/Oasis/voronoi_random.png;Mid Texture 4;_MidTexture4;white;-1;None;Bot Texture 4;_BotTexture4;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.2,0.2;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;44.99978,50.28945;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;971.7191,222.8393;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;Tabasco/TerrainSand;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;-100;False;Opaque;;Geometry;All;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;1;SplatCount=4;False;1;BaseMapShader=ASESampleShaders/SimpleTerrainBase;0;False;-1;-1;0;False;-1;0;0;0;True;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;143;0;140;0
WireConnection;143;1;139;0
WireConnection;146;0;153;2
WireConnection;146;1;154;0
WireConnection;146;2;143;0
WireConnection;144;0;78;106
WireConnection;147;0;144;0
WireConnection;147;1;145;0
WireConnection;148;0;146;0
WireConnection;150;0;147;0
WireConnection;150;2;148;0
WireConnection;149;0;146;0
WireConnection;151;0;149;0
WireConnection;151;1;150;0
WireConnection;129;0;151;0
WireConnection;130;0;129;0
WireConnection;131;0;130;0
WireConnection;163;0;78;106
WireConnection;159;0;139;0
WireConnection;159;1;158;0
WireConnection;132;1;131;0
WireConnection;165;0;163;0
WireConnection;169;0;167;0
WireConnection;135;0;133;0
WireConnection;135;1;134;0
WireConnection;164;0;159;0
WireConnection;136;0;132;0
WireConnection;136;1;153;1
WireConnection;170;0;163;0
WireConnection;170;1;169;0
WireConnection;137;0;136;0
WireConnection;137;1;135;0
WireConnection;162;0;165;0
WireConnection;162;1;164;0
WireConnection;162;2;166;0
WireConnection;152;0;78;0
WireConnection;152;1;137;0
WireConnection;173;0;170;0
WireConnection;173;1;162;0
WireConnection;141;0;138;0
WireConnection;141;1;138;0
WireConnection;160;0;159;0
WireConnection;171;0;152;0
WireConnection;171;1;170;0
WireConnection;171;2;173;0
WireConnection;142;3;141;0
WireConnection;30;0;31;0
WireConnection;30;1;78;0
WireConnection;0;0;78;0
WireConnection;0;2;30;0
WireConnection;0;13;171;0
WireConnection;0;11;78;17
ASEEND*/
//CHKSM=9E655E710678CC8A0BCD6BCCD04896B6C18704F4