// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/NoisyLightObjectSpace"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_AlbedoNoise0("Albedo Noise 0", Color) = (1,1,1,1)
		_AlbedoNoise1("Albedo Noise 1", Color) = (1,1,1,1)
		_AlbedoNoise2("Albedo Noise 2", Color) = (1,1,1,1)
		_NoiseMultiplier("Noise Multiplier", Range( -1 , 100)) = 0
		_LightingGamma("Lighting Gamma", Range( 0 , 10)) = 8
		_WhiteClip("White Clip", Range( 0 , 1)) = 1
		_BlackClip("Black Clip", Range( 0 , 1)) = 0
		_PointLightHardness("Point Light Hardness", Range( 0 , 1)) = 0
		[HDR]_FresnelColor("Fresnel Color", Color) = (0,0,0,0)
		_FresnelPower("Fresnel Power", Float) = 1
		[HDR]_DarkFresnelColor("Dark Fresnel Color", Color) = (0,0,0,0)
		_DarkFresnelPower("Dark Fresnel Power", Float) = 1
		[HDR]_Ambient("Ambient", Color) = (0,0,0,0)
		_GIColor("GI Color", Color) = (0,0,0,0)
		[HideInInspector]unity_Lightmap("unity_Lightmap", 2D) = "white" {}
		_NoiseTexture("Noise Texture", 2D) = "white" {}
		_NoiseTiling("Noise Tiling", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "DisableBatching" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#define ASE_TEXTURE_PARAMS(textureName) textureName

		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
			float2 uv_texcoord;
			float2 uv2_texcoord2;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _NoiseTexture;
		uniform float _NoiseTiling;
		uniform float4 _AlbedoNoise0;
		uniform float4 _AlbedoNoise1;
		uniform float4 _AlbedoNoise2;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _Ambient;
		// uniform sampler2D unity_Lightmap;
		uniform float4 unity_Lightmap_ST;
		uniform float4 _GIColor;
		uniform float _DarkFresnelPower;
		uniform float4 _DarkFresnelColor;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _PointLightHardness;
		uniform float _BlackClip;
		uniform float _WhiteClip;
		uniform float _LightingGamma;
		uniform float _FresnelPower;
		uniform float4 _FresnelColor;
		uniform float _NoiseMultiplier;


		inline float4 TriplanarSamplingSF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV27_g3 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode27_g3 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV27_g3, _DarkFresnelPower ) );
			float2 appendResult33 = (float2(_NoiseTiling , _NoiseTiling));
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float4 triplanar6 = TriplanarSamplingSF( _NoiseTexture, ase_vertex3Pos, ase_vertexNormal, 1.0, appendResult33, 1.0, 0 );
			float4 temp_output_50_0_g3 = triplanar6;
			float SingleChannelNoise94_g3 = temp_output_50_0_g3.r;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float dotResult6_g3 = dot( ase_worldlightDir , normalize( (WorldNormalVector( i , float4( UnpackNormal( tex2D( _Normal, uv_Normal ) ) , 0.0 ).rgb )) ) );
			float lerpResult53_g3 = lerp( ( ase_lightColor.a * saturate( dotResult6_g3 ) ) , ( ( ( dotResult6_g3 * _PointLightHardness ) + 1.0 ) - _PointLightHardness ) , _WorldSpaceLightPos0.w);
			float temp_output_14_0_g3 = saturate( lerpResult53_g3 );
			float lerpResult57_g3 = lerp( pow( saturate( (0.0 + (temp_output_14_0_g3 - _BlackClip) * (1.0 - 0.0) / (_WhiteClip - _BlackClip)) ) , ( 1.0 / ( _LightingGamma + 1E-05 ) ) ) , temp_output_14_0_g3 , _WorldSpaceLightPos0.w);
			float RemappedLIght90_g3 = ( saturate( lerpResult57_g3 ) * ase_lightAtten );
			float fresnelNdotV24_g3 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode24_g3 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV24_g3, _FresnelPower ) );
			float4 break83_g3 = temp_output_50_0_g3;
			float3 appendResult85_g3 = (float3(break83_g3.r , break83_g3.g , break83_g3.b));
			float3 normalizeResult79_g3 = normalize( appendResult85_g3 );
			float3 NormalizedNoise99_g3 = normalizeResult79_g3;
			float3 weightedBlendVar82_g3 = NormalizedNoise99_g3;
			float4 weightedAvg82_g3 = ( ( weightedBlendVar82_g3.x*_AlbedoNoise0 + weightedBlendVar82_g3.y*_AlbedoNoise1 + weightedBlendVar82_g3.z*_AlbedoNoise2 )/( weightedBlendVar82_g3.x + weightedBlendVar82_g3.y + weightedBlendVar82_g3.z ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 temp_output_65_0_g3 = ( weightedAvg82_g3 * tex2D( _Albedo, uv_Albedo ) );
			float temp_output_139_0_g3 = abs( ( RemappedLIght90_g3 - 0.5 ) );
			float lerpResult13_g3 = lerp( ( (-1.0 + (SingleChannelNoise94_g3 - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) * _NoiseMultiplier ) , 0.0 , saturate( ( temp_output_139_0_g3 * 2.0 ) ));
			UnityGI gi67_g3 = gi;
			float3 diffNorm67_g3 = ase_worldNormal;
			gi67_g3 = UnityGI_Base( data, 1, diffNorm67_g3 );
			float3 indirectDiffuse67_g3 = gi67_g3.indirect.diffuse + diffNorm67_g3 * 0.0001;
			c.rgb = ( ( ( ( saturate( fresnelNode27_g3 ) * _DarkFresnelColor * SingleChannelNoise94_g3 * ( 1.0 - RemappedLIght90_g3 ) ) + ( saturate( fresnelNode24_g3 ) * _FresnelColor * SingleChannelNoise94_g3 * RemappedLIght90_g3 ) ) * ( 1.0 - _WorldSpaceLightPos0.w ) ) + ( temp_output_65_0_g3 * ( float4( ( ase_lightColor.rgb * saturate( ( lerpResult13_g3 + (-1.0 + (RemappedLIght90_g3 - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) ) ) ) , 0.0 ) + ( float4( indirectDiffuse67_g3 , 0.0 ) * _GIColor ) ) ) ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 appendResult33 = (float2(_NoiseTiling , _NoiseTiling));
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float4 triplanar6 = TriplanarSamplingSF( _NoiseTexture, ase_vertex3Pos, ase_vertexNormal, 1.0, appendResult33, 1.0, 0 );
			float4 temp_output_50_0_g3 = triplanar6;
			float4 break83_g3 = temp_output_50_0_g3;
			float3 appendResult85_g3 = (float3(break83_g3.r , break83_g3.g , break83_g3.b));
			float3 normalizeResult79_g3 = normalize( appendResult85_g3 );
			float3 NormalizedNoise99_g3 = normalizeResult79_g3;
			float3 weightedBlendVar82_g3 = NormalizedNoise99_g3;
			float4 weightedAvg82_g3 = ( ( weightedBlendVar82_g3.x*_AlbedoNoise0 + weightedBlendVar82_g3.y*_AlbedoNoise1 + weightedBlendVar82_g3.z*_AlbedoNoise2 )/( weightedBlendVar82_g3.x + weightedBlendVar82_g3.y + weightedBlendVar82_g3.z ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 temp_output_65_0_g3 = ( weightedAvg82_g3 * tex2D( _Albedo, uv_Albedo ) );
			o.Albedo = temp_output_65_0_g3.rgb;
			float2 uv2unity_Lightmap = i.uv2_texcoord2 * unity_Lightmap_ST.xy + unity_Lightmap_ST.zw;
			float SingleChannelNoise94_g3 = temp_output_50_0_g3.r;
			o.Emission = ( ( _Ambient * temp_output_65_0_g3 ) + ( UNITY_SAMPLE_TEX2D( unity_Lightmap, uv2unity_Lightmap ) * _GIColor * SingleChannelNoise94_g3 ) ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
37;721;1841;611;1047.493;702.848;1.514644;True;False
Node;AmplifyShaderEditor.RangedFloatNode;34;-950.0956,-219.5599;Float;False;Property;_NoiseTiling;Noise Tiling;19;0;Create;True;0;0;False;0;0;-0.77;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;33;-577.5159,-100.6794;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TriplanarNode;6;-87.00075,-100.6504;Float;True;Spherical;Object;False;Noise Texture;_NoiseTexture;white;18;Assets/Textures/Oasis/voronoi_random.png;Mid Texture 0;_MidTexture0;white;-1;None;Bot Texture 0;_BotTexture0;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.2,0.2;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;83;61.22682,-351.1407;Float;True;Property;_Normal;Normal;1;0;Create;True;0;0;False;0;None;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;82;100.6074,-624.0865;Float;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;81;644.7615,-280.3651;Float;False;NoisyLightFunction;2;;3;dae7fed4b42bf4600bbf48ff5aa49aa0;0;3;130;COLOR;1,1,1,0;False;128;COLOR;0.5,0.5,1,0;False;50;COLOR;0,0,0,0;False;3;COLOR;48;COLOR;0;COLOR;49
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1036.835,-345.653;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;Tabasco/NoisyLightObjectSpace;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;34;0
WireConnection;33;1;34;0
WireConnection;6;3;33;0
WireConnection;81;130;82;0
WireConnection;81;128;83;0
WireConnection;81;50;6;0
WireConnection;0;0;81;48
WireConnection;0;2;81;0
WireConnection;0;13;81;49
ASEEND*/
//CHKSM=F184D359DA879CC2419DB4ED0812D519E7ED10DE