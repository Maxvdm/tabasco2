// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/PropsOasis"
{
	Properties
	{
		_Albedo_0("Albedo_0", 2D) = "white" {}
		_Normal_0("Normal_0", 2D) = "bump" {}
		_Smoothness_0("Smoothness_0", Range( 0 , 1)) = 0
		_Albedo_1("Albedo_1", 2D) = "white" {}
		_Normal_1("Normal_1", 2D) = "bump" {}
		_Smoothness_1("Smoothness_1", Range( 0 , 1)) = 0
		_TopTexture0("Top Texture 0", 2D) = "white" {}
		_NoiseMultiplier("Noise Multiplier", Float) = 1
		_TerrainSplatFalloff("TerrainSplatFalloff", Float) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#define ASE_TEXTURE_PARAMS(textureName) textureName

		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
			float4 vertexColor : COLOR;
		};

		uniform sampler2D _TopTexture0;
		uniform float _NoiseMultiplier;
		uniform float _TerrainSplatFalloff;
		uniform sampler2D _Normal_0;
		uniform sampler2D _Normal_1;
		uniform sampler2D _Albedo_0;
		uniform sampler2D _Albedo_1;
		uniform float _Smoothness_0;
		uniform float _Smoothness_1;


		inline float4 TriplanarSamplingSF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		float4 ClampColors7_g26( float4 RGBA , float smoothness )
		{
			float4 result = (0,0,0,0);
						
			float4 tempThreshold = max(max(RGBA.b,RGBA.g),RGBA.a);
			result.r = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.r);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.a);
			result.b = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.b);
			            
			tempThreshold = max(max(RGBA.b,RGBA.r),RGBA.a);
			result.g = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.g);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.b);
			result.a = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.a);
			            
			return result;
		}


		inline float3 TriplanarSamplingSNF( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.zy * float2( nsign.x, 1.0 ) ) );
			yNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xz * float2( nsign.y, 1.0 ) ) );
			zNorm = ( tex2D( ASE_TEXTURE_PARAMS( topTexMap ), tiling * worldPos.xy * float2( -nsign.z, 1.0 ) ) );
			xNorm.xyz = half3( UnpackNormal( xNorm ).xy * float2( nsign.x, 1.0 ) + worldNormal.zy, worldNormal.x ).zyx;
			yNorm.xyz = half3( UnpackNormal( yNorm ).xy * float2( nsign.y, 1.0 ) + worldNormal.xz, worldNormal.y ).xzy;
			zNorm.xyz = half3( UnpackNormal( zNorm ).xy * float2( -nsign.z, 1.0 ) + worldNormal.xy, worldNormal.z ).xyz;
			return normalize( xNorm.xyz * projNormal.x + yNorm.xyz * projNormal.y + zNorm.xyz * projNormal.z );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float4 triplanar14_g26 = TriplanarSamplingSF( _TopTexture0, ase_worldPos, ase_worldNormal, 1.0, float2( 0.1,0.1 ), 1.0, 0 );
			float4 _Vector3 = float4(1,1,1,1);
			float2 appendResult74 = (float2(i.vertexColor.rg));
			float2 normalizeResult44 = normalize( appendResult74 );
			float4 temp_cast_2 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g26 = normalize( ( ( (float4(-1,-1,-1,-1) + (triplanar14_g26 - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _NoiseMultiplier ) + pow( float4( normalizeResult44, 0.0 , 0.0 ) , temp_cast_2 ) ) );
			float4 RGBA7_g26 = normalizeResult8_g26;
			float smoothness7_g26 = 0.1;
			float4 localClampColors7_g26 = ClampColors7_g26( RGBA7_g26 , smoothness7_g26 );
			float4 temp_output_73_0 = localClampColors7_g26;
			float3 ase_worldTangent = WorldNormalVector( i, float3( 1, 0, 0 ) );
			float3 ase_worldBitangent = WorldNormalVector( i, float3( 0, 1, 0 ) );
			float3x3 ase_worldToTangent = float3x3( ase_worldTangent, ase_worldBitangent, ase_worldNormal );
			float3 triplanar34 = TriplanarSamplingSNF( _Normal_0, ase_worldPos, ase_worldNormal, 1.0, float2( 0.1,0.1 ), 1.0, 0 );
			float3 tanTriplanarNormal34 = mul( ase_worldToTangent, triplanar34 );
			float3 triplanar37 = TriplanarSamplingSNF( _Normal_1, ase_worldPos, ase_worldNormal, 1.0, float2( 0.1,0.1 ), 1.0, 0 );
			float3 tanTriplanarNormal37 = mul( ase_worldToTangent, triplanar37 );
			float4 weightedBlendVar69 = temp_output_73_0;
			float3 weightedBlend69 = ( weightedBlendVar69.x*tanTriplanarNormal34 + weightedBlendVar69.y*tanTriplanarNormal37 + weightedBlendVar69.z*float3( 0,0,0 ) + weightedBlendVar69.w*float3( 0,0,0 ) );
			o.Normal = weightedBlend69;
			float4 triplanar33 = TriplanarSamplingSF( _Albedo_0, ase_worldPos, ase_worldNormal, 1.0, float2( 0.1,0.1 ), 1.0, 0 );
			float4 triplanar39 = TriplanarSamplingSF( _Albedo_1, ase_worldPos, ase_worldNormal, 1.0, float2( 0.1,0.1 ), 1.0, 0 );
			float4 weightedBlendVar54 = temp_output_73_0;
			float4 weightedAvg54 = ( ( weightedBlendVar54.x*triplanar33 + weightedBlendVar54.y*triplanar39 + weightedBlendVar54.z*float4( 0,0,0,0 ) + weightedBlendVar54.w*float4( 0,0,0,0 ) )/( weightedBlendVar54.x + weightedBlendVar54.y + weightedBlendVar54.z + weightedBlendVar54.w ) );
			o.Albedo = weightedAvg54.xyz;
			float4 weightedBlendVar70 = temp_output_73_0;
			float weightedAvg70 = ( ( weightedBlendVar70.x*_Smoothness_0 + weightedBlendVar70.y*_Smoothness_1 + weightedBlendVar70.z*0.0 + weightedBlendVar70.w*0.0 )/( weightedBlendVar70.x + weightedBlendVar70.y + weightedBlendVar70.z + weightedBlendVar70.w ) );
			o.Smoothness = weightedAvg70;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma only_renderers metal d3d11_9x 
		#pragma surface surf Standard keepalpha fullforwardshadows nometa noforwardadd 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				surfIN.vertexColor = IN.color;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
726;648;1832;533;1754.372;-248.0376;1.967623;True;True
Node;AmplifyShaderEditor.VertexColorNode;41;-1637.356,1130.876;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;74;-829.7184,1152.374;Float;False;FLOAT2;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NormalizeNode;44;-628.8096,1217.342;Float;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;73;-265.4529,1023.078;Float;True;TerrainSplatTriplanarNoise;8;;26;d02286e7d8b6b453e85a5501bc320a5e;0;1;12;FLOAT4;0,0,0,0;False;2;FLOAT4;13;FLOAT4;0
Node;AmplifyShaderEditor.TriplanarNode;33;-1054.495,-19.00796;Float;True;Spherical;World;False;Albedo_0;_Albedo_0;white;0;None;Mid Texture 1;_MidTexture1;white;-1;None;Bot Texture 1;_BotTexture1;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.1,0.1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;39;-1065.453,590.0286;Float;True;Spherical;World;False;Albedo_1;_Albedo_1;white;5;None;Mid Texture 3;_MidTexture3;white;-1;None;Bot Texture 3;_BotTexture3;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.1,0.1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;37;-1051.856,843.9419;Float;True;Spherical;World;True;Normal_1;_Normal_1;bump;6;None;Mid Texture 4;_MidTexture4;white;-1;None;Bot Texture 4;_BotTexture4;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.1,0.1;False;4;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;34;-1048.715,221.0618;Float;True;Spherical;World;True;Normal_0;_Normal_0;bump;3;None;Mid Texture 2;_MidTexture2;white;-1;None;Bot Texture 2;_BotTexture2;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.1,0.1;False;4;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;36;-906.7552,439.7517;Float;False;Property;_Smoothness_0;Smoothness_0;4;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-1208.206,1018.061;Float;False;Property;_Smoothness_1;Smoothness_1;7;0;Create;True;0;0;False;0;0;0.488;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;62;-331.7425,1788.865;Float;False;Constant;_Vector0;Vector 0;9;0;Create;True;0;0;False;0;0,1,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;66;724.5304,1877.518;Float;False;NormalCreate;1;;27;e12f7ae19d416b942820e3932b56220f;0;4;1;SAMPLER2D;;False;2;FLOAT2;0,0;False;3;FLOAT;0.5;False;4;FLOAT;2;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;63;327.599,1945.053;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;42;-2023.475,1529.807;Float;True;Property;_Mask;Mask;13;0;Create;True;0;0;False;0;c99a47428deff194d9544b76b4b77d01;c99a47428deff194d9544b76b4b77d01;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WeightedBlendNode;54;400.7418,421.0662;Float;True;5;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TFHCRemapNode;57;-1215.218,1772.584;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.5;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;79;686.0034,1038.784;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.UnpackScaleNormalNode;68;511.0335,710.2277;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NormalizeNode;64;594.9179,2039.355;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldNormalVector;61;-372.3906,1708.504;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;78;397.1031,908.7333;Float;True;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SaturateNode;52;-915.1451,1798.583;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SummedBlendNode;69;85.29165,672.278;Float;False;5;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DotProductOpNode;46;-1573.776,1704.41;Float;True;2;0;FLOAT3;0,1,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;77;389.6189,1171.808;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-1431.44,1274.873;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DepthFade;59;-1639.234,1579.602;Float;False;True;False;False;2;1;FLOAT3;0,0,0;False;0;FLOAT;7.39;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;56;-1897.918,1954.125;Float;False;Constant;_Vector1;Vector 1;9;0;Create;True;0;0;False;0;0,1,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.LerpOp;48;-915.9966,1350.683;Float;True;3;0;COLOR;0,0,1,0;False;1;COLOR;0,1,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;67;-1681.311,2024.78;Float;False;Property;_DirectionalFalloff;Directional Falloff;12;0;Create;True;0;0;False;0;0.31;0.38;-10;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;47;-2069.002,1819.775;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WeightedBlendNode;70;116.4892,835.1526;Float;True;5;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;867.8821,545.4445;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Tabasco/PropsOasis;False;False;False;False;False;False;False;False;False;False;True;True;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;False;False;False;False;False;True;True;False;False;False;False;False;False;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;74;0;41;0
WireConnection;44;0;74;0
WireConnection;73;12;44;0
WireConnection;63;0;62;0
WireConnection;63;1;61;0
WireConnection;63;2;52;0
WireConnection;54;0;73;0
WireConnection;54;1;33;0
WireConnection;54;2;39;0
WireConnection;57;0;46;0
WireConnection;57;3;67;0
WireConnection;79;0;78;0
WireConnection;79;1;77;0
WireConnection;68;0;69;0
WireConnection;64;0;63;0
WireConnection;52;0;57;0
WireConnection;69;0;73;0
WireConnection;69;1;34;0
WireConnection;69;2;37;0
WireConnection;46;1;47;0
WireConnection;43;0;41;0
WireConnection;43;1;42;0
WireConnection;48;0;43;0
WireConnection;48;2;52;0
WireConnection;70;0;73;0
WireConnection;70;1;36;0
WireConnection;70;2;38;0
WireConnection;0;0;54;0
WireConnection;0;1;69;0
WireConnection;0;4;70;0
ASEEND*/
//CHKSM=B90AF01F9F67605BB51ED407767C24A8025D1948