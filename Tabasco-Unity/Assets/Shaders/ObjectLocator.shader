// Upgrade NOTE: upgraded instancing buffer 'ObjectLocator' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ObjectLocator"
{
	Properties
	{
		_IntersectPosition("_IntersectPosition", Vector) = (0,0,0,0)
		_TransitionDIstance("Transition DIstance", Float) = 4
		_TransitionFalloff("Transition Falloff", Float) = 21
		_Color0("Color 0", Color) = (0.09152129,0.09433961,0.06897472,0)
		_Color1("Color 1", Color) = (0.09433961,0.09011213,0.08588465,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float4 _Color0;
		uniform float4 _Color1;
		uniform float _TransitionDIstance;
		uniform float _TransitionFalloff;

		UNITY_INSTANCING_BUFFER_START(ObjectLocator)
			UNITY_DEFINE_INSTANCED_PROP(float3, _IntersectPosition)
#define _IntersectPosition_arr ObjectLocator
		UNITY_INSTANCING_BUFFER_END(ObjectLocator)

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 _IntersectPosition_Instance = UNITY_ACCESS_INSTANCED_PROP(_IntersectPosition_arr, _IntersectPosition);
			float clampResult8_g2 = clamp( pow( ( distance( ase_worldPos , _IntersectPosition_Instance ) / _TransitionDIstance ) , _TransitionFalloff ) , 0.0 , 1.0 );
			float temp_output_26_0 = clampResult8_g2;
			float4 lerpResult11 = lerp( _Color0 , _Color1 , temp_output_26_0);
			o.Albedo = lerpResult11.rgb;
			float4 color29 = IsGammaSpace() ? float4(0,0,0,0) : float4(0,0,0,0);
			float4 lerpResult27 = lerp( _Color0 , color29 , temp_output_26_0);
			o.Emission = lerpResult27.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16800
1182;222;1305;655;1517.737;417.6936;1.60015;True;True
Node;AmplifyShaderEditor.RangedFloatNode;18;-1132.901,346.3779;Float;True;Property;_TransitionDIstance;Transition DIstance;2;0;Create;True;0;0;False;0;4;6.58;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1059.295,424.7853;Float;False;Property;_TransitionFalloff;Transition Falloff;3;0;Create;True;0;0;False;0;21;8.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;26;-872.0161,304.475;Float;False;DistanceBlendObject;0;;2;2f8005ac2f0ed470f8ca3e375990756e;0;2;16;FLOAT;0;False;17;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;8;-907.4304,-219.2741;Float;False;Property;_Color0;Color 0;4;0;Create;True;0;0;False;0;0.09152129,0.09433961,0.06897472,0;1,0.3820753,0.8761182,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;29;-574.4491,389.5819;Float;False;Constant;_Color2;Color 2;5;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;9;-745.4633,65.69467;Float;False;Property;_Color1;Color 1;5;0;Create;True;0;0;False;0;0.09433961,0.09011213,0.08588465,0;0.7830389,0.7768779,0.8113207,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;11;-303.6237,22.62036;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;27;-252.819,218.366;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;23;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ObjectLocator;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;28;-1683.353,-588.1096;Float;False;100;100;5;0;;1,1,1,1;0;0
WireConnection;26;16;18;0
WireConnection;26;17;19;0
WireConnection;11;0;8;0
WireConnection;11;1;9;0
WireConnection;11;2;26;0
WireConnection;27;0;8;0
WireConnection;27;1;29;0
WireConnection;27;2;26;0
WireConnection;23;0;11;0
WireConnection;23;2;27;0
ASEEND*/
//CHKSM=05AD31B1E9B754718B1F7D89903C45A0C7699B9C