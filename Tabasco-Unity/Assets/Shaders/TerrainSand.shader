// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/TerrainSand"
{
	Properties
	{
		_SplatMapNoiseMultiplier("Splat Map Noise Multiplier", Float) = 1
		_TerrainSplatFalloff("TerrainSplatFalloff", Float) = 1
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector]_Control("Control", 2D) = "white" {}
		[HideInInspector]_Splat3("Splat3", 2D) = "white" {}
		[HideInInspector]_Splat2("Splat2", 2D) = "white" {}
		[HideInInspector]_Splat1("Splat1", 2D) = "white" {}
		[HideInInspector]_Splat0("Splat0", 2D) = "white" {}
		[HideInInspector]_Normal0("Normal0", 2D) = "white" {}
		[HideInInspector]_Normal1("Normal1", 2D) = "white" {}
		[HideInInspector]_Normal2("Normal2", 2D) = "white" {}
		[HideInInspector]_Normal3("Normal3", 2D) = "white" {}
		[HideInInspector]_Smoothness3("Smoothness3", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness1("Smoothness1", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness0("Smoothness0", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness2("Smoothness2", Range( 0 , 1)) = 1
		_TextureArray0("Texture Array 0", 2DArray ) = "" {}
		_ColorMapNoiseCorrection("ColorMapNoiseCorrection", Vector) = (0,0,0,0)
		_ColorMapNoise("ColorMapNoise", Range( 0 , 0.1)) = 0.01
		_ColorMapBounds("ColorMapBounds", Vector) = (-1000,1000,-1000,1000)
		_AlbedoNoise0("Albedo Noise 0", Color) = (1,1,1,1)
		_AlbedoNoise1("Albedo Noise 1", Color) = (1,1,1,1)
		_AlbedoNoise2("Albedo Noise 2", Color) = (1,1,1,1)
		_NoiseMultiplier("Noise Multiplier", Range( -1 , 1)) = 0
		[HDR]_Ambient("Ambient", Color) = (0,0,0,0)
		_GIColor("GI Color", Color) = (0,0,0,0)
		[HDR]_FresnelColor("Fresnel Color", Color) = (0,0,0,0)
		_FresnelPower("Fresnel Power", Float) = 1
		[HDR]_DarkFresnelColor("Dark Fresnel Color", Color) = (0,0,0,0)
		_DarkFresnelPower("Dark Fresnel Power", Float) = 1
		_PointLightHardness("Point Light Hardness", Range( 0 , 1)) = 0
		_LightingGamma("Lighting Gamma", Range( 0 , 10)) = 8
		_WhiteClip("White Clip", Range( 0 , 1)) = 1
		_BlackClip("Black Clip", Range( 0 , 1)) = 0
		[HideInInspector]unity_Lightmap("unity_Lightmap", 2D) = "white" {}
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Pass
		{
			ColorMask 0
			ZWrite On
		}

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry-100" "IsEmissive" = "true"  "SplatCount"="4" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float2 uv2_texcoord2;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float4 _AlbedoNoise0;
		uniform float4 _AlbedoNoise1;
		uniform float4 _AlbedoNoise2;
		uniform float _SplatMapNoiseMultiplier;
		uniform sampler2D _Control;
		uniform float4 _Control_ST;
		uniform float _TerrainSplatFalloff;
		uniform float _Smoothness0;
		uniform UNITY_DECLARE_TEX2DARRAY( _TextureArray0 );
		uniform float2 _ColorMapNoiseCorrection;
		uniform float _ColorMapNoise;
		uniform float4 _ColorMapBounds;
		uniform float _Smoothness1;
		uniform float _Smoothness2;
		uniform float _Smoothness3;
		uniform float4 _Ambient;
		// uniform sampler2D unity_Lightmap;
		uniform float4 unity_Lightmap_ST;
		uniform float4 _GIColor;
		uniform float _DarkFresnelPower;
		uniform float4 _DarkFresnelColor;
		uniform sampler2D _Normal0;
		uniform sampler2D _Splat0;
		uniform float4 _Splat0_ST;
		uniform sampler2D _Normal1;
		uniform sampler2D _Splat1;
		uniform float4 _Splat1_ST;
		uniform sampler2D _Normal2;
		uniform sampler2D _Splat2;
		uniform float4 _Splat2_ST;
		uniform sampler2D _Normal3;
		uniform sampler2D _Splat3;
		uniform float4 _Splat3_ST;
		uniform float _PointLightHardness;
		uniform float _BlackClip;
		uniform float _WhiteClip;
		uniform float _LightingGamma;
		uniform float _FresnelPower;
		uniform float4 _FresnelColor;
		uniform float _NoiseMultiplier;


		float4 ClampColors7_g12( float4 RGBA , float smoothness )
		{
			float4 result = (0,0,0,0);
						
			float4 tempThreshold = max(max(RGBA.b,RGBA.g),RGBA.a);
			result.r = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.r);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.a);
			result.b = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.b);
			            
			tempThreshold = max(max(RGBA.b,RGBA.r),RGBA.a);
			result.g = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.g);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.b);
			result.a = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.a);
			            
			return result;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV27_g71 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode27_g71 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV27_g71, _DarkFresnelPower ) );
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode3_g12 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 temp_output_151_13_g11 = tex2DNode3_g12;
			float4 temp_output_50_0_g71 = temp_output_151_13_g11;
			float SingleChannelNoise94_g71 = temp_output_50_0_g71.r;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float4 _Vector3 = float4(1,1,1,1);
			float2 uv_Control = i.uv_texcoord * _Control_ST.xy + _Control_ST.zw;
			float4 tex2DNode5_g11 = tex2D( _Control, uv_Control );
			float dotResult20_g11 = dot( tex2DNode5_g11 , float4(1,1,1,1) );
			float SplatWeight22_g11 = dotResult20_g11;
			float localSplatClip74_g11 = ( SplatWeight22_g11 );
			float SplatWeight74_g11 = SplatWeight22_g11;
			#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
				clip(SplatWeight74_g11 == 0.0f ? -1 : 1);
			#endif
			float4 temp_cast_19 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g12 = normalize( ( ( (float4(-1,-1,-1,-1) + (tex2DNode3_g12 - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _SplatMapNoiseMultiplier ) + pow( ( tex2DNode5_g11 / ( localSplatClip74_g11 + 0.001 ) ) , temp_cast_19 ) ) );
			float4 RGBA7_g12 = normalizeResult8_g12;
			float smoothness7_g12 = 0.1;
			float4 localClampColors7_g12 = ClampColors7_g12( RGBA7_g12 , smoothness7_g12 );
			float4 SplatControl26_g11 = localClampColors7_g12;
			float4 temp_output_59_0_g11 = SplatControl26_g11;
			float2 uv0_Splat0 = i.uv_texcoord * _Splat0_ST.xy + _Splat0_ST.zw;
			float2 uv0_Splat1 = i.uv_texcoord * _Splat1_ST.xy + _Splat1_ST.zw;
			float2 uv0_Splat2 = i.uv_texcoord * _Splat2_ST.xy + _Splat2_ST.zw;
			float2 uv0_Splat3 = i.uv_texcoord * _Splat3_ST.xy + _Splat3_ST.zw;
			float4 weightedBlendVar8_g11 = temp_output_59_0_g11;
			float4 weightedBlend8_g11 = ( weightedBlendVar8_g11.x*tex2D( _Normal0, ( 200.0 * uv0_Splat0 ) ) + weightedBlendVar8_g11.y*tex2D( _Normal1, uv0_Splat1 ) + weightedBlendVar8_g11.z*tex2D( _Normal2, uv0_Splat2 ) + weightedBlendVar8_g11.w*tex2D( _Normal3, uv0_Splat3 ) );
			float dotResult6_g71 = dot( ase_worldlightDir , normalize( (WorldNormalVector( i , float4( UnpackNormal( weightedBlend8_g11 ) , 0.0 ).rgb )) ) );
			float lerpResult53_g71 = lerp( ( ase_lightColor.a * saturate( dotResult6_g71 ) ) , ( ( ( dotResult6_g71 * _PointLightHardness ) + 1.0 ) - _PointLightHardness ) , _WorldSpaceLightPos0.w);
			float temp_output_14_0_g71 = saturate( lerpResult53_g71 );
			float lerpResult57_g71 = lerp( pow( saturate( (0.0 + (temp_output_14_0_g71 - _BlackClip) * (1.0 - 0.0) / (_WhiteClip - _BlackClip)) ) , ( 1.0 / _LightingGamma ) ) , temp_output_14_0_g71 , _WorldSpaceLightPos0.w);
			float RemappedLIght90_g71 = ( lerpResult57_g71 * ase_lightAtten );
			float fresnelNdotV24_g71 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode24_g71 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV24_g71, _FresnelPower ) );
			float4 break83_g71 = temp_output_50_0_g71;
			float3 appendResult85_g71 = (float3(break83_g71.r , break83_g71.g , break83_g71.b));
			float3 normalizeResult79_g71 = normalize( appendResult85_g71 );
			float3 NormalizedNoise99_g71 = normalizeResult79_g71;
			float3 weightedBlendVar82_g71 = NormalizedNoise99_g71;
			float4 weightedAvg82_g71 = ( ( weightedBlendVar82_g71.x*_AlbedoNoise0 + weightedBlendVar82_g71.y*_AlbedoNoise1 + weightedBlendVar82_g71.z*_AlbedoNoise2 )/( weightedBlendVar82_g71.x + weightedBlendVar82_g71.y + weightedBlendVar82_g71.z ) );
			float4 appendResult33_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness0));
			float4 break138_g11 = temp_output_151_13_g11;
			float2 appendResult126_g11 = (float2(( (-_ColorMapNoise + (break138_g11.r - _ColorMapNoiseCorrection.x) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.x)) + (0.0 + (ase_worldPos.x - _ColorMapBounds.x) * (1.0 - 0.0) / (_ColorMapBounds.y - _ColorMapBounds.x)) ) , ( (0.0 + (ase_worldPos.z - _ColorMapBounds.z) * (1.0 - 0.0) / (_ColorMapBounds.w - _ColorMapBounds.z)) + (-_ColorMapNoise + (break138_g11.g - _ColorMapNoiseCorrection.y) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.y)) )));
			float4 texArray127_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 0.0)  );
			float4 appendResult36_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness1));
			float4 texArray132_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 1.0)  );
			float4 appendResult39_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness2));
			float4 texArray133_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 2.0)  );
			float4 appendResult42_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness3));
			float4 texArray134_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 3.0)  );
			float4 weightedBlendVar9_g11 = temp_output_59_0_g11;
			float4 weightedBlend9_g11 = ( weightedBlendVar9_g11.x*( appendResult33_g11 * texArray127_g11 ) + weightedBlendVar9_g11.y*( appendResult36_g11 * texArray132_g11 ) + weightedBlendVar9_g11.z*( appendResult39_g11 * texArray133_g11 ) + weightedBlendVar9_g11.w*( appendResult42_g11 * texArray134_g11 ) );
			float4 MixDiffuse28_g11 = weightedBlend9_g11;
			float4 temp_output_65_0_g71 = ( weightedAvg82_g71 * MixDiffuse28_g11 );
			float temp_output_11_0_g71 = ( (-1.0 + (SingleChannelNoise94_g71 - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) * _NoiseMultiplier );
			float lerpResult13_g71 = lerp( temp_output_11_0_g71 , 0.0 , ( distance( RemappedLIght90_g71 , 0.5 ) * 1.0 ));
			UnityGI gi67_g71 = gi;
			float3 diffNorm67_g71 = ase_worldNormal;
			gi67_g71 = UnityGI_Base( data, 1, diffNorm67_g71 );
			float3 indirectDiffuse67_g71 = gi67_g71.indirect.diffuse + diffNorm67_g71 * 0.0001;
			c.rgb = ( ( ( ( saturate( fresnelNode27_g71 ) * _DarkFresnelColor * SingleChannelNoise94_g71 * ( 1.0 - RemappedLIght90_g71 ) ) + ( saturate( fresnelNode24_g71 ) * _FresnelColor * SingleChannelNoise94_g71 * RemappedLIght90_g71 ) ) * ( 1.0 - _WorldSpaceLightPos0.w ) ) + ( temp_output_65_0_g71 * ( float4( ( ase_lightColor.rgb * saturate( ( lerpResult13_g71 + (-1.0 + (RemappedLIght90_g71 - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) ) ) ) , 0.0 ) + ( float4( indirectDiffuse67_g71 , 0.0 ) * _GIColor ) ) ) ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode3_g12 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 temp_output_151_13_g11 = tex2DNode3_g12;
			float4 temp_output_50_0_g71 = temp_output_151_13_g11;
			float4 break83_g71 = temp_output_50_0_g71;
			float3 appendResult85_g71 = (float3(break83_g71.r , break83_g71.g , break83_g71.b));
			float3 normalizeResult79_g71 = normalize( appendResult85_g71 );
			float3 NormalizedNoise99_g71 = normalizeResult79_g71;
			float3 weightedBlendVar82_g71 = NormalizedNoise99_g71;
			float4 weightedAvg82_g71 = ( ( weightedBlendVar82_g71.x*_AlbedoNoise0 + weightedBlendVar82_g71.y*_AlbedoNoise1 + weightedBlendVar82_g71.z*_AlbedoNoise2 )/( weightedBlendVar82_g71.x + weightedBlendVar82_g71.y + weightedBlendVar82_g71.z ) );
			float4 _Vector3 = float4(1,1,1,1);
			float2 uv_Control = i.uv_texcoord * _Control_ST.xy + _Control_ST.zw;
			float4 tex2DNode5_g11 = tex2D( _Control, uv_Control );
			float dotResult20_g11 = dot( tex2DNode5_g11 , float4(1,1,1,1) );
			float SplatWeight22_g11 = dotResult20_g11;
			float localSplatClip74_g11 = ( SplatWeight22_g11 );
			float SplatWeight74_g11 = SplatWeight22_g11;
			#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
				clip(SplatWeight74_g11 == 0.0f ? -1 : 1);
			#endif
			float4 temp_cast_5 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g12 = normalize( ( ( (float4(-1,-1,-1,-1) + (tex2DNode3_g12 - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _SplatMapNoiseMultiplier ) + pow( ( tex2DNode5_g11 / ( localSplatClip74_g11 + 0.001 ) ) , temp_cast_5 ) ) );
			float4 RGBA7_g12 = normalizeResult8_g12;
			float smoothness7_g12 = 0.1;
			float4 localClampColors7_g12 = ClampColors7_g12( RGBA7_g12 , smoothness7_g12 );
			float4 SplatControl26_g11 = localClampColors7_g12;
			float4 temp_output_59_0_g11 = SplatControl26_g11;
			float4 appendResult33_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness0));
			float4 break138_g11 = temp_output_151_13_g11;
			float3 ase_worldPos = i.worldPos;
			float2 appendResult126_g11 = (float2(( (-_ColorMapNoise + (break138_g11.r - _ColorMapNoiseCorrection.x) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.x)) + (0.0 + (ase_worldPos.x - _ColorMapBounds.x) * (1.0 - 0.0) / (_ColorMapBounds.y - _ColorMapBounds.x)) ) , ( (0.0 + (ase_worldPos.z - _ColorMapBounds.z) * (1.0 - 0.0) / (_ColorMapBounds.w - _ColorMapBounds.z)) + (-_ColorMapNoise + (break138_g11.g - _ColorMapNoiseCorrection.y) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.y)) )));
			float4 texArray127_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 0.0)  );
			float4 appendResult36_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness1));
			float4 texArray132_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 1.0)  );
			float4 appendResult39_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness2));
			float4 texArray133_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 2.0)  );
			float4 appendResult42_g11 = (float4(1.0 , 1.0 , 1.0 , _Smoothness3));
			float4 texArray134_g11 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g11, 3.0)  );
			float4 weightedBlendVar9_g11 = temp_output_59_0_g11;
			float4 weightedBlend9_g11 = ( weightedBlendVar9_g11.x*( appendResult33_g11 * texArray127_g11 ) + weightedBlendVar9_g11.y*( appendResult36_g11 * texArray132_g11 ) + weightedBlendVar9_g11.z*( appendResult39_g11 * texArray133_g11 ) + weightedBlendVar9_g11.w*( appendResult42_g11 * texArray134_g11 ) );
			float4 MixDiffuse28_g11 = weightedBlend9_g11;
			float4 temp_output_65_0_g71 = ( weightedAvg82_g71 * MixDiffuse28_g11 );
			o.Albedo = temp_output_65_0_g71.rgb;
			float2 uv2unity_Lightmap = i.uv2_texcoord2 * unity_Lightmap_ST.xy + unity_Lightmap_ST.zw;
			float SingleChannelNoise94_g71 = temp_output_50_0_g71.r;
			o.Emission = ( ( _Ambient * temp_output_65_0_g71 ) + ( UNITY_SAMPLE_TEX2D( unity_Lightmap, uv2unity_Lightmap ) * _GIColor * SingleChannelNoise94_g71 ) ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows nofog dithercrossfade 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}

	Dependency "BaseMapShader"="ASESampleShaders/SimpleTerrainBase"
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
37;538;1841;794;1639.756;619.823;2.072011;True;False
Node;AmplifyShaderEditor.FunctionNode;277;-785.7308,97.66614;Float;False;Tabasco Four Splats Terrain;0;;11;a24967c16208e43af80b9a40781b13c9;0;6;59;FLOAT4;0,0,0,0;False;60;FLOAT4;0,0,0,0;False;61;FLOAT3;0,0,0;False;57;FLOAT;0;False;58;FLOAT;0;False;62;FLOAT;0;False;7;COLOR;106;FLOAT4;0;FLOAT3;14;FLOAT;56;FLOAT;45;FLOAT;19;FLOAT3;17
Node;AmplifyShaderEditor.FunctionNode;279;57.61391,108.8834;Float;False;NoisyLightFunction;26;;71;dae7fed4b42bf4600bbf48ff5aa49aa0;0;3;130;COLOR;1,1,1,0;False;128;COLOR;0.5,0.5,1,0;False;50;COLOR;0,0,0,0;False;3;COLOR;48;COLOR;0;COLOR;49
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;599.6942,16.89167;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;Tabasco/TerrainSand;False;False;False;False;False;False;False;False;False;True;False;False;True;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;True;0;Opaque;0.5;True;True;-100;False;Opaque;;Geometry;All;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;1;SplatCount=4;False;1;BaseMapShader=ASESampleShaders/SimpleTerrainBase;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;279;130;277;0
WireConnection;279;128;277;14
WireConnection;279;50;277;106
WireConnection;0;0;279;48
WireConnection;0;2;279;0
WireConnection;0;13;279;49
ASEEND*/
//CHKSM=688C86E12CC5F1C055B54F53FA9B2782B8130225