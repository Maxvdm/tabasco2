﻿Shader "Utility/Blur" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
	}

	CGINCLUDE
		#include "UnityCG.cginc"

		sampler2D _MainTex, _SubtractiveTexture, _TerrainTexture;
		float4 _MainTex_TexelSize;

		float _BlurRadius;

		struct VertexData {
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct Interpolators {
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
		};

		Interpolators VertexProgram (VertexData v) {
			Interpolators i;
			i.pos = UnityObjectToClipPos(v.vertex);
			i.uv = v.uv;
			return i;
		}

	ENDCG

	SubShader {
		Cull Off
		ZTest Always
		ZWrite Off


		Pass { // 0 bokeh Pass
			CGPROGRAM
				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram
				
			static const float gaussianWeights[10] = {
				0.1596215016,
				0.1473483106,
				0.1159101366,
				0.07770020205,
				0.0443815392,
				0.02160561746,
				0.008962330206,
				0.003170824414,
				0.0009602496649,
				0.00005001300338
				};
					
			float4 FragmentProgram (Interpolators i) : SV_Target {
			
			
				float blurMask = 0;
				for (int u = -9; u <= 9; u++) {
						for (int v = -9; v <= 9; v++) {
							float2 o = float2(u, v) * _MainTex_TexelSize.xy * _BlurRadius;
							blurMask += tex2D(_MainTex, i.uv + o).g * gaussianWeights[abs(u)] * gaussianWeights[abs(v)];
						}
					}
				
				float color = 0;
					for (int u = -9; u <= 9; u++) {
						for (int v = -9; v <= 9; v++) {
							float2 o = float2(u, v) * _MainTex_TexelSize.xy * _BlurRadius * blurMask;
							color += tex2D(_MainTex, i.uv + o).r * gaussianWeights[abs(u)] * gaussianWeights[abs(v)];
						}
					}
				return float4(color, tex2D(_MainTex, i.uv).g,0, 1);
				}
			ENDCG
		}
		
		Pass { // 1 subtractive pass
			CGPROGRAM
				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram
						
			float4 FragmentProgram (Interpolators i) : SV_Target {
			    float4 output = 0;
			    float2 subtractiveUV = i.uv;
			    subtractiveUV.x = 1.0 - subtractiveUV.x;
			    float4 additiveHeight = tex2D(_MainTex, i.uv);
			    float4 subtractiveHeight = tex2D(_SubtractiveTexture, subtractiveUV);
			    
			    output += step(additiveHeight.r, subtractiveHeight.r) * additiveHeight;
			    output += step(subtractiveHeight.r, additiveHeight.r) * subtractiveHeight;
				return float4(output.r,output.g, 0, 1);
			}
			ENDCG
		}
		
		Pass { // 2 min pass
			CGPROGRAM
				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram
						
			float4 FragmentProgram (Interpolators i) : SV_Target {
			
			    float4 additiveHeight = tex2D(_MainTex, i.uv);
			    float4 terrainHeight = tex2D(_TerrainTexture, i.uv);
			   
			    float4 output = additiveHeight;
			    output.r = 0;
			    output.r += step(additiveHeight.r, terrainHeight.r) * additiveHeight;
			    output.r += step(terrainHeight.r, additiveHeight.r) * terrainHeight;
			    return output;
			}
			ENDCG
		}
	}
}