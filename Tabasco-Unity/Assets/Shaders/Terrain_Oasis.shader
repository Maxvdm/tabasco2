// Upgrade NOTE: upgraded instancing buffer 'TabascoTerrainOasis' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/TerrainOasis"
{
	Properties
	{
		_NoiseMultiplier("Noise Multiplier", Float) = 1
		_TerrainSplatFalloff("TerrainSplatFalloff", Float) = 1
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector]_Control("Control", 2D) = "white" {}
		[HideInInspector]_Splat3("Splat3", 2D) = "white" {}
		[HideInInspector]_Splat2("Splat2", 2D) = "white" {}
		[HideInInspector]_Splat1("Splat1", 2D) = "white" {}
		[HideInInspector]_Splat0("Splat0", 2D) = "white" {}
		[HideInInspector]_Smoothness3("Smoothness3", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness1("Smoothness1", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness0("Smoothness0", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness2("Smoothness2", Range( 0 , 1)) = 1
		_IntersectPosition("_IntersectPosition", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry-100"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap
		#pragma only_renderers metal d3d11_9x 
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows novertexlights nolightmap  nometa noforwardadd vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		#ifdef UNITY_INSTANCING_ENABLED//ASE Terrain Instancing
			sampler2D _TerrainHeightmapTexture;//ASE Terrain Instancing
			sampler2D _TerrainNormalmapTexture;//ASE Terrain Instancing
		#endif//ASE Terrain Instancing
		UNITY_INSTANCING_BUFFER_START( Terrain )//ASE Terrain Instancing
			UNITY_DEFINE_INSTANCED_PROP( float4, _TerrainPatchInstanceData )//ASE Terrain Instancing
		UNITY_INSTANCING_BUFFER_END( Terrain)//ASE Terrain Instancing
		CBUFFER_START( UnityTerrain)//ASE Terrain Instancing
			#ifdef UNITY_INSTANCING_ENABLED//ASE Terrain Instancing
				float4 _TerrainHeightmapRecipSize;//ASE Terrain Instancing
				float4 _TerrainHeightmapScale;//ASE Terrain Instancing
			#endif//ASE Terrain Instancing
		CBUFFER_END//ASE Terrain Instancing
		uniform sampler2D _TextureSample0;
		uniform float _NoiseMultiplier;
		uniform sampler2D _Control;
		uniform float _TerrainSplatFalloff;
		uniform float _Smoothness0;
		uniform sampler2D _Splat0;
		uniform float _Smoothness1;
		uniform sampler2D _Splat1;
		uniform float _Smoothness2;
		uniform sampler2D _Splat2;
		uniform float _Smoothness3;
		uniform sampler2D _Splat3;

		UNITY_INSTANCING_BUFFER_START(TabascoTerrainOasis)
			UNITY_DEFINE_INSTANCED_PROP(float4, _TextureSample0_ST)
#define _TextureSample0_ST_arr TabascoTerrainOasis
			UNITY_DEFINE_INSTANCED_PROP(float4, _Control_ST)
#define _Control_ST_arr TabascoTerrainOasis
			UNITY_DEFINE_INSTANCED_PROP(float4, _Splat0_ST)
#define _Splat0_ST_arr TabascoTerrainOasis
			UNITY_DEFINE_INSTANCED_PROP(float4, _Splat1_ST)
#define _Splat1_ST_arr TabascoTerrainOasis
			UNITY_DEFINE_INSTANCED_PROP(float4, _Splat2_ST)
#define _Splat2_ST_arr TabascoTerrainOasis
			UNITY_DEFINE_INSTANCED_PROP(float4, _Splat3_ST)
#define _Splat3_ST_arr TabascoTerrainOasis
			UNITY_DEFINE_INSTANCED_PROP(float3, _IntersectPosition)
#define _IntersectPosition_arr TabascoTerrainOasis
		UNITY_INSTANCING_BUFFER_END(TabascoTerrainOasis)


		void ApplyMeshModification( inout appdata_full v )
		{
			#if defined(UNITY_INSTANCING_ENABLED) && !defined(SHADER_API_D3D11_9X)
				float2 patchVertex = v.vertex.xy;
				float4 instanceData = UNITY_ACCESS_INSTANCED_PROP(Terrain, _TerrainPatchInstanceData);
				
				float4 uvscale = instanceData.z * _TerrainHeightmapRecipSize;
				float4 uvoffset = instanceData.xyxy * uvscale;
				uvoffset.xy += 0.5f * _TerrainHeightmapRecipSize.xy;
				float2 sampleCoords = (patchVertex.xy * uvscale.xy + uvoffset.xy);
				
				float hm = UnpackHeightmap(tex2Dlod(_TerrainHeightmapTexture, float4(sampleCoords, 0, 0)));
				v.vertex.xz = (patchVertex.xy + instanceData.xy) * _TerrainHeightmapScale.xz * instanceData.z;
				v.vertex.y = hm * _TerrainHeightmapScale.y;
				v.vertex.w = 1.0f;
				
				v.texcoord.xy = (patchVertex.xy * uvscale.zw + uvoffset.zw);
				v.texcoord3 = v.texcoord2 = v.texcoord1 = v.texcoord;
				
				#ifdef TERRAIN_INSTANCED_PERPIXEL_NORMAL
					v.normal = float3(0, 1, 0);
					//data.tc.zw = sampleCoords;
				#else
					float3 nor = tex2Dlod(_TerrainNormalmapTexture, float4(sampleCoords, 0, 0)).xyz;
					v.normal = 2.0f * nor - 1.0f;
				#endif
			#endif
		}


		float4 ClampColors7_g59( float4 RGBA )
		{
			float4 result = (0,0,0,0);
			result.r = step(max(max(RGBA.b,RGBA.g),RGBA.a),RGBA.r);
			result.b = step(max(max(RGBA.g,RGBA.r),RGBA.a),RGBA.b);
			result.g = step(max(max(RGBA.b,RGBA.r),RGBA.a),RGBA.g);
			result.a = step(max(max(RGBA.g,RGBA.r),RGBA.b),RGBA.a);
			return result;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			ApplyMeshModification(v);;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 _IntersectPosition_Instance = UNITY_ACCESS_INSTANCED_PROP(_IntersectPosition_arr, _IntersectPosition);
			float4 _TextureSample0_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_TextureSample0_ST_arr, _TextureSample0_ST);
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST_Instance.xy + _TextureSample0_ST_Instance.zw;
			float4 _Vector3 = float4(1,1,1,1);
			float4 _Control_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_Control_ST_arr, _Control_ST);
			float2 uv_Control = i.uv_texcoord * _Control_ST_Instance.xy + _Control_ST_Instance.zw;
			float4 tex2DNode5_g58 = tex2D( _Control, uv_Control );
			float dotResult20_g58 = dot( tex2DNode5_g58 , float4(1,1,1,1) );
			float SplatWeight22_g58 = dotResult20_g58;
			float localSplatClip74_g58 = ( SplatWeight22_g58 );
			float SplatWeight74_g58 = SplatWeight22_g58;
			#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
				clip(SplatWeight74_g58 == 0.0f ? -1 : 1);
			#endif
			float4 temp_cast_5 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g59 = normalize( ( ( (float4(-1,-1,-1,-1) + (tex2D( _TextureSample0, uv_TextureSample0 ) - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _NoiseMultiplier ) + pow( ( tex2DNode5_g58 / ( localSplatClip74_g58 + 0.001 ) ) , temp_cast_5 ) ) );
			float4 RGBA7_g59 = normalizeResult8_g59;
			float4 localClampColors7_g59 = ClampColors7_g59( RGBA7_g59 );
			float4 SplatControl26_g58 = localClampColors7_g59;
			float4 temp_output_59_0_g58 = SplatControl26_g58;
			float4 appendResult33_g58 = (float4(1.0 , 1.0 , 1.0 , _Smoothness0));
			float4 _Splat0_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_Splat0_ST_arr, _Splat0_ST);
			float2 uv_Splat0 = i.uv_texcoord * _Splat0_ST_Instance.xy + _Splat0_ST_Instance.zw;
			float4 appendResult36_g58 = (float4(1.0 , 1.0 , 1.0 , _Smoothness1));
			float4 _Splat1_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_Splat1_ST_arr, _Splat1_ST);
			float2 uv_Splat1 = i.uv_texcoord * _Splat1_ST_Instance.xy + _Splat1_ST_Instance.zw;
			float4 appendResult39_g58 = (float4(1.0 , 1.0 , 1.0 , _Smoothness2));
			float4 _Splat2_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_Splat2_ST_arr, _Splat2_ST);
			float2 uv_Splat2 = i.uv_texcoord * _Splat2_ST_Instance.xy + _Splat2_ST_Instance.zw;
			float4 appendResult42_g58 = (float4(1.0 , 1.0 , 1.0 , _Smoothness3));
			float4 _Splat3_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_Splat3_ST_arr, _Splat3_ST);
			float2 uv_Splat3 = i.uv_texcoord * _Splat3_ST_Instance.xy + _Splat3_ST_Instance.zw;
			float4 weightedBlendVar9_g58 = temp_output_59_0_g58;
			float4 weightedBlend9_g58 = ( weightedBlendVar9_g58.x*( appendResult33_g58 * tex2D( _Splat0, uv_Splat0 ) ) + weightedBlendVar9_g58.y*( appendResult36_g58 * tex2D( _Splat1, uv_Splat1 ) ) + weightedBlendVar9_g58.z*( appendResult39_g58 * tex2D( _Splat2, uv_Splat2 ) ) + weightedBlendVar9_g58.w*( appendResult42_g58 * tex2D( _Splat3, uv_Splat3 ) ) );
			float4 MixDiffuse28_g58 = weightedBlend9_g58;
			float4 temp_output_165_0 = MixDiffuse28_g58;
			o.Albedo = temp_output_165_0.xyz;
			o.Alpha = 1;
		}

		ENDCG
		UsePass "Hidden/Nature/Terrain/Utilities/PICKING"
		UsePass "Hidden/Nature/Terrain/Utilities/SELECTION"
	}

	Dependency "BaseMapShader"="ASESampleShaders/SimpleTerrainBase"
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
0;655;1552;633;4001.374;485.3227;2.595;True;True
Node;AmplifyShaderEditor.FunctionNode;2;-329.0367,990.8287;Float;False;DistanceFromCameraBlend;22;;54;3f4db41bee22a4e9c800760b9f0355b8;0;2;10;FLOAT;4.05;False;8;FLOAT;4.38;False;1;FLOAT;11
Node;AmplifyShaderEditor.PowerNode;145;-2444.914,599.4673;Float;True;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BreakToComponentsNode;27;298.0515,819.9723;Float;False;FLOAT;1;0;FLOAT;0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;7;-673.3593,1022.562;Float;False;Property;_FallOff;FallOff;26;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-932.1833,1235.871;Float;False;Property;_Distance;Distance;25;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;150;-1525.431,439.1847;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;151;-1832.856,298.4549;Float;False;2;0;FLOAT;0;False;1;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;108;-2876.013,791.709;Float;False;Property;_Float6;Float 6;24;0;Create;True;0;0;False;0;1;0.17;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;4;151.075,815.8639;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;165;-2988.773,182.4872;Float;False;Tabasco Four Splats Terrain;0;;58;a24967c16208e43af80b9a40781b13c9;0;6;59;FLOAT4;0,0,0,0;False;60;FLOAT4;0,0,0,0;False;61;FLOAT3;0,0,0;False;57;FLOAT;0;False;58;FLOAT;0;False;62;FLOAT;0;False;6;FLOAT4;0;FLOAT3;14;FLOAT;56;FLOAT;45;FLOAT;19;FLOAT;17
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-1073.652,24.09117;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Tabasco/TerrainOasis;False;False;False;False;False;True;True;False;False;False;True;True;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;-100;False;Opaque;;Geometry;All;False;False;False;False;False;True;True;False;False;False;False;False;False;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;1;=;False;1;BaseMapShader=ASESampleShaders/SimpleTerrainBase;0;False;-1;-1;0;False;-1;0;0;0;True;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;10;6;0
WireConnection;2;8;7;0
WireConnection;145;0;165;0
WireConnection;145;1;108;0
WireConnection;27;0;4;0
WireConnection;150;0;151;0
WireConnection;150;1;165;45
WireConnection;4;2;2;11
WireConnection;0;0;165;0
ASEEND*/
//CHKSM=39E86DAD683B5881FE33CD58EA318ECCAFFD5FD4