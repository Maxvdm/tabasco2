// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/NoisyLightTerrain"
{
	Properties
	{
		_NoiseMultiplier("Noise Multiplier", Float) = 1
		_TerrainSplatFalloff("TerrainSplatFalloff", Float) = 1
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector]_Control("Control", 2D) = "white" {}
		[HideInInspector]_Splat3("Splat3", 2D) = "white" {}
		[HideInInspector]_Splat2("Splat2", 2D) = "white" {}
		[HideInInspector]_Splat1("Splat1", 2D) = "white" {}
		[HideInInspector]_Splat0("Splat0", 2D) = "white" {}
		[HideInInspector]_Smoothness3("Smoothness3", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness1("Smoothness1", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness0("Smoothness0", Range( 0 , 1)) = 1
		[HideInInspector]_Smoothness2("Smoothness2", Range( 0 , 1)) = 1
		_TextureArray0("Texture Array 0", 2DArray ) = "" {}
		_ColorMapNoiseCorrection("ColorMapNoiseCorrection", Vector) = (0,0,0,0)
		_ColorMapNoise("ColorMapNoise", Range( 0 , 0.1)) = 0.01
		_ColorMapBounds("ColorMapBounds", Vector) = (-1000,1000,-1000,1000)
		_PointLightHardness("Point Light Hardness", Range( 0 , 1)) = 0
		_LightGradient("Light Gradient", 2D) = "white" {}
		_GIColor("GI Color", Color) = (0,0,0,0)
		_Float10("Float 10", Range( -1 , 1)) = 0.8293508
		[HDR]_EmissiveTint("EmissiveTint", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  "SplatCount"="4" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float4 _EmissiveTint;
		uniform float _NoiseMultiplier;
		uniform sampler2D _Control;
		uniform float4 _Control_ST;
		uniform float _TerrainSplatFalloff;
		uniform float _Smoothness0;
		uniform sampler2D _Splat0;
		uniform UNITY_DECLARE_TEX2DARRAY( _TextureArray0 );
		uniform float2 _ColorMapNoiseCorrection;
		uniform float _ColorMapNoise;
		uniform float4 _ColorMapBounds;
		uniform float _Smoothness1;
		uniform sampler2D _Splat1;
		uniform float4 _Splat1_ST;
		uniform float _Smoothness2;
		uniform sampler2D _Splat2;
		uniform float4 _Splat2_ST;
		uniform float _Smoothness3;
		uniform sampler2D _Splat3;
		uniform float4 _Splat3_ST;
		uniform sampler2D _LightGradient;
		uniform float _PointLightHardness;
		uniform float _Float10;
		uniform float4 _GIColor;


		float4 ClampColors7_g63( float4 RGBA , float smoothness )
		{
			float4 result = (0,0,0,0);
						
			float4 tempThreshold = max(max(RGBA.b,RGBA.g),RGBA.a);
			result.r = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.r);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.a);
			result.b = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.b);
			            
			tempThreshold = max(max(RGBA.b,RGBA.r),RGBA.a);
			result.g = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.g);
			            
			tempThreshold = max(max(RGBA.g,RGBA.r),RGBA.b);
			result.a = smoothstep(tempThreshold - smoothness, tempThreshold + smoothness, RGBA.a);
			            
			return result;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float localCalculateTangentsStandard16_g62 = ( 0.0 );
			v.tangent.xyz = cross ( v.normal, float3( 0, 0, 1 ) );
			v.tangent.w = -1;
			float3 temp_cast_0 = (localCalculateTangentsStandard16_g62).xxx;
			v.vertex.xyz += temp_cast_0;
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode3_g63 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 _Vector3 = float4(1,1,1,1);
			float2 uv_Control = i.uv_texcoord * _Control_ST.xy + _Control_ST.zw;
			float4 tex2DNode5_g62 = tex2D( _Control, uv_Control );
			float dotResult20_g62 = dot( tex2DNode5_g62 , float4(1,1,1,1) );
			float SplatWeight22_g62 = dotResult20_g62;
			float localSplatClip74_g62 = ( SplatWeight22_g62 );
			float SplatWeight74_g62 = SplatWeight22_g62;
			#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
				clip(SplatWeight74_g62 == 0.0f ? -1 : 1);
			#endif
			float4 temp_cast_23 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g63 = normalize( ( ( (float4(-1,-1,-1,-1) + (tex2DNode3_g63 - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _NoiseMultiplier ) + pow( ( tex2DNode5_g62 / ( localSplatClip74_g62 + 0.001 ) ) , temp_cast_23 ) ) );
			float4 RGBA7_g63 = normalizeResult8_g63;
			float smoothness7_g63 = 0.1;
			float4 localClampColors7_g63 = ClampColors7_g63( RGBA7_g63 , smoothness7_g63 );
			float4 SplatControl26_g62 = localClampColors7_g63;
			float4 temp_output_59_0_g62 = SplatControl26_g62;
			float4 appendResult33_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness0));
			float3 ase_worldPos = i.worldPos;
			float2 appendResult115_g62 = (float2(ase_worldPos.x , ase_worldPos.z));
			float4 temp_output_146_13_g62 = tex2DNode3_g63;
			float4 break138_g62 = temp_output_146_13_g62;
			float2 appendResult126_g62 = (float2(( (-_ColorMapNoise + (break138_g62.r - _ColorMapNoiseCorrection.x) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.x)) + (0.0 + (ase_worldPos.x - _ColorMapBounds.x) * (1.0 - 0.0) / (_ColorMapBounds.y - _ColorMapBounds.x)) ) , ( (0.0 + (ase_worldPos.z - _ColorMapBounds.z) * (1.0 - 0.0) / (_ColorMapBounds.w - _ColorMapBounds.z)) + (-_ColorMapNoise + (break138_g62.g - _ColorMapNoiseCorrection.y) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.y)) )));
			float4 texArray127_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 0.0)  );
			float4 appendResult36_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness1));
			float2 uv_Splat1 = i.uv_texcoord * _Splat1_ST.xy + _Splat1_ST.zw;
			float4 texArray132_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 1.0)  );
			float4 appendResult39_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness2));
			float2 uv_Splat2 = i.uv_texcoord * _Splat2_ST.xy + _Splat2_ST.zw;
			float4 texArray133_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 2.0)  );
			float4 appendResult42_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness3));
			float2 uv_Splat3 = i.uv_texcoord * _Splat3_ST.xy + _Splat3_ST.zw;
			float4 texArray134_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 3.0)  );
			float4 weightedBlendVar9_g62 = temp_output_59_0_g62;
			float4 weightedBlend9_g62 = ( weightedBlendVar9_g62.x*( appendResult33_g62 * tex2D( _Splat0, appendResult115_g62 ) * texArray127_g62 ) + weightedBlendVar9_g62.y*( appendResult36_g62 * tex2D( _Splat1, uv_Splat1 ) * texArray132_g62 ) + weightedBlendVar9_g62.z*( appendResult39_g62 * tex2D( _Splat2, uv_Splat2 ) * texArray133_g62 ) + weightedBlendVar9_g62.w*( appendResult42_g62 * tex2D( _Splat3, uv_Splat3 ) * texArray134_g62 ) );
			float4 MixDiffuse28_g62 = weightedBlend9_g62;
			float4 temp_output_1_0 = MixDiffuse28_g62;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float dotResult4 = dot( ase_worldlightDir , ase_normWorldNormal );
			float lerpResult52 = lerp( dotResult4 , ( ( ( dotResult4 * _PointLightHardness ) + 1.0 ) - _PointLightHardness ) , _WorldSpaceLightPos0.w);
			float temp_output_8_0 = ( ase_lightColor.a * 0 * lerpResult52 );
			float4 temp_output_1_106 = temp_output_146_13_g62;
			float lerpResult12 = lerp( ( (-1.0 + (temp_output_1_106.r - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) * _Float10 ) , 0.0 , distance( temp_output_8_0 , 0.5 ));
			float temp_output_14_0 = saturate( ( saturate( temp_output_8_0 ) + lerpResult12 ) );
			float2 appendResult17 = (float2(( 1.0 - temp_output_14_0 ) , 0.0));
			float4 temp_cast_35 = (temp_output_14_0).xxxx;
			float4 lerpResult56 = lerp( tex2D( _LightGradient, appendResult17 ) , temp_cast_35 , _WorldSpaceLightPos0.w);
			float4 temp_output_28_0 = ( lerpResult56 * float4( ase_lightColor.rgb , 0.0 ) );
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float fresnelNdotV22 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode22 = ( 0.0 + 0.56 * pow( 1.0 - fresnelNdotV22, 13.42 ) );
			float4 temp_output_33_0 = ( temp_output_1_106.r * saturate( fresnelNode22 ) * (0.0 + (0 - 0.0) * (1.0 - 0.0) / (1.0 - 0.0)) * ( 1.0 - _WorldSpaceLightPos0.w ) * temp_output_28_0 );
			float3 normalizeResult4_g64 = normalize( ( ase_worldViewDir + ase_worldlightDir ) );
			float dotResult20 = dot( ase_normWorldNormal , normalizeResult4_g64 );
			c.rgb = ( ( temp_output_1_0 * ( temp_output_28_0 + ( float4( 0,0,0,0 ) * _GIColor ) ) ) + temp_output_33_0 + ( temp_output_33_0 * ( pow( temp_output_1_106.r , 6.57 ) * saturate( dotResult20 ) * 14.0 ) ) ).xyz;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode3_g63 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 temp_output_146_13_g62 = tex2DNode3_g63;
			float4 temp_output_1_106 = temp_output_146_13_g62;
			o.Albedo = temp_output_1_106.rgb;
			float4 _Vector3 = float4(1,1,1,1);
			float2 uv_Control = i.uv_texcoord * _Control_ST.xy + _Control_ST.zw;
			float4 tex2DNode5_g62 = tex2D( _Control, uv_Control );
			float dotResult20_g62 = dot( tex2DNode5_g62 , float4(1,1,1,1) );
			float SplatWeight22_g62 = dotResult20_g62;
			float localSplatClip74_g62 = ( SplatWeight22_g62 );
			float SplatWeight74_g62 = SplatWeight22_g62;
			#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
				clip(SplatWeight74_g62 == 0.0f ? -1 : 1);
			#endif
			float4 temp_cast_6 = (_TerrainSplatFalloff).xxxx;
			float4 normalizeResult8_g63 = normalize( ( ( (float4(-1,-1,-1,-1) + (tex2DNode3_g63 - float4( 0,0,0,0 )) * (_Vector3 - float4(-1,-1,-1,-1)) / (_Vector3 - float4( 0,0,0,0 ))) * _NoiseMultiplier ) + pow( ( tex2DNode5_g62 / ( localSplatClip74_g62 + 0.001 ) ) , temp_cast_6 ) ) );
			float4 RGBA7_g63 = normalizeResult8_g63;
			float smoothness7_g63 = 0.1;
			float4 localClampColors7_g63 = ClampColors7_g63( RGBA7_g63 , smoothness7_g63 );
			float4 SplatControl26_g62 = localClampColors7_g63;
			float4 temp_output_59_0_g62 = SplatControl26_g62;
			float4 appendResult33_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness0));
			float3 ase_worldPos = i.worldPos;
			float2 appendResult115_g62 = (float2(ase_worldPos.x , ase_worldPos.z));
			float4 break138_g62 = temp_output_146_13_g62;
			float2 appendResult126_g62 = (float2(( (-_ColorMapNoise + (break138_g62.r - _ColorMapNoiseCorrection.x) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.x)) + (0.0 + (ase_worldPos.x - _ColorMapBounds.x) * (1.0 - 0.0) / (_ColorMapBounds.y - _ColorMapBounds.x)) ) , ( (0.0 + (ase_worldPos.z - _ColorMapBounds.z) * (1.0 - 0.0) / (_ColorMapBounds.w - _ColorMapBounds.z)) + (-_ColorMapNoise + (break138_g62.g - _ColorMapNoiseCorrection.y) * (_ColorMapNoise - -_ColorMapNoise) / (1.0 - _ColorMapNoiseCorrection.y)) )));
			float4 texArray127_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 0.0)  );
			float4 appendResult36_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness1));
			float2 uv_Splat1 = i.uv_texcoord * _Splat1_ST.xy + _Splat1_ST.zw;
			float4 texArray132_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 1.0)  );
			float4 appendResult39_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness2));
			float2 uv_Splat2 = i.uv_texcoord * _Splat2_ST.xy + _Splat2_ST.zw;
			float4 texArray133_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 2.0)  );
			float4 appendResult42_g62 = (float4(1.0 , 1.0 , 1.0 , _Smoothness3));
			float2 uv_Splat3 = i.uv_texcoord * _Splat3_ST.xy + _Splat3_ST.zw;
			float4 texArray134_g62 = UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(appendResult126_g62, 3.0)  );
			float4 weightedBlendVar9_g62 = temp_output_59_0_g62;
			float4 weightedBlend9_g62 = ( weightedBlendVar9_g62.x*( appendResult33_g62 * tex2D( _Splat0, appendResult115_g62 ) * texArray127_g62 ) + weightedBlendVar9_g62.y*( appendResult36_g62 * tex2D( _Splat1, uv_Splat1 ) * texArray132_g62 ) + weightedBlendVar9_g62.z*( appendResult39_g62 * tex2D( _Splat2, uv_Splat2 ) * texArray133_g62 ) + weightedBlendVar9_g62.w*( appendResult42_g62 * tex2D( _Splat3, uv_Splat3 ) * texArray134_g62 ) );
			float4 MixDiffuse28_g62 = weightedBlend9_g62;
			float4 temp_output_1_0 = MixDiffuse28_g62;
			o.Emission = ( _EmissiveTint * temp_output_1_0 ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}

	Dependency "BaseMapShader"="ASESampleShaders/SimpleTerrainBase"
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
381;516;1362;548;1647.847;761.1863;1.73483;True;True
Node;AmplifyShaderEditor.WorldNormalVector;3;-4296.776,130.3128;Float;True;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;2;-4248.456,-49.32301;Float;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;4;-3956.037,47.41504;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-4840.129,796.7167;Float;False;Property;_PointLightHardness;Point Light Hardness;26;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;-4496.667,731.0658;Float;False;2;2;0;FLOAT;0.5;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-4353.192,790.447;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;53;-4745.058,1107.45;Float;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleSubtractOpNode;49;-4177.645,805.0762;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;1;-4816.325,-693.2729;Float;False;Tabasco Four Splats Terrain;0;;62;a24967c16208e43af80b9a40781b13c9;0;6;59;FLOAT4;0,0,0,0;False;60;FLOAT4;0,0,0,0;False;61;FLOAT3;0,0,0;False;57;FLOAT;0;False;58;FLOAT;0;False;62;FLOAT;0;False;7;COLOR;106;FLOAT4;0;FLOAT3;14;FLOAT;56;FLOAT;45;FLOAT;19;FLOAT;17
Node;AmplifyShaderEditor.BreakToComponentsNode;54;-4507.91,-281.2286;Float;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.LightAttenuation;46;-3927.042,-78.48502;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;5;-4025.699,-401.9072;Float;True;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.LerpOp;52;-3882.899,619.5248;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-3587.478,47.98398;Float;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-3689.73,826.111;Float;False;Property;_Float10;Float 10;29;0;Create;True;0;0;False;0;0.8293508;0.439;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;6;-3674.273,482.323;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-3242.26,588.7639;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;9;-3332.013,349.588;Float;True;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;12;-2969.563,466.0498;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;11;-3206.06,166.13;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;13;-2883.965,158.3656;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;14;-2628.575,160.748;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;15;-2403.799,160.9653;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;17;-2214.35,178.9035;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;21;-2030.368,132.7156;Float;True;Property;_LightGradient;Light Gradient;27;0;Create;True;0;0;True;0;bde49b316487a4c61b70b807add703f9;bde49b316487a4c61b70b807add703f9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;16;-2900.136,754.494;Float;True;Blinn-Phong Half Vector;-1;;64;91a149ac9d615be429126c95e20753ce;0;0;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;56;-1931.886,-29.18514;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.FresnelNode;22;-2509.507,-427.9956;Float;True;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0.56;False;3;FLOAT;13.42;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;19;-1973.331,1069.696;Float;False;Property;_GIColor;GI Color;28;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;23;-2364.107,340.928;Float;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.DotProductOpNode;20;-2357.2,567.459;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;18;-2698.396,-204.7375;Float;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.TFHCRemapNode;30;-2413.076,-112.9063;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-1633.446,79.38998;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;24;-2369.14,-169.37;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;25;-2006.171,665.0601;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;27;-1935.553,333.3271;Float;False;2;0;FLOAT;0;False;1;FLOAT;6.57;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;29;-2046.133,-293.1276;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1527.492,731.61;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-1163.598,498.244;Float;False;Constant;_Float7;Float 7;8;0;Create;True;0;0;False;0;14;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-1667.131,-198.35;Float;False;5;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;34;-1280.671,42.48703;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-1665.343,352.7541;Float;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-1241.838,-425.5493;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ColorNode;35;-1198.436,-869.3624;Float;False;Property;_EmissiveTint;EmissiveTint;31;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-1070.718,-120.9608;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;42;-930.516,106.6219;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;41;-1464.306,-8.687023;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;40;-1971.735,537.182;Float;False;2;0;FLOAT;0;False;1;FLOAT;130;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-4448.401,475.0129;Float;False;Property;_Float8;Float 8;32;0;Create;True;0;0;False;0;0;0.34;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;39;-954.8321,-342.6939;Float;False;3;3;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;45;-4254.253,521.725;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TriplanarNode;38;-4160.802,353.763;Float;True;Spherical;World;False;Top Texture 3;_TopTexture3;white;30;None;Mid Texture 4;_MidTexture4;white;-1;None;Bot Texture 4;_BotTexture4;white;-1;None;Triplanar Sampler;False;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;0.2,0.2;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-746.9258,-691.1846;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-214.4382,-458.54;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;Tabasco/NoisyLightTerrain;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;1;SplatCount=4;False;1;BaseMapShader=ASESampleShaders/SimpleTerrainBase;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;4;0;2;0
WireConnection;4;1;3;0
WireConnection;50;0;4;0
WireConnection;50;1;48;0
WireConnection;51;0;50;0
WireConnection;49;0;51;0
WireConnection;49;1;48;0
WireConnection;54;0;1;106
WireConnection;52;0;4;0
WireConnection;52;1;49;0
WireConnection;52;2;53;2
WireConnection;8;0;5;2
WireConnection;8;2;52;0
WireConnection;6;0;54;0
WireConnection;10;0;6;0
WireConnection;10;1;7;0
WireConnection;9;0;8;0
WireConnection;12;0;10;0
WireConnection;12;2;9;0
WireConnection;11;0;8;0
WireConnection;13;0;11;0
WireConnection;13;1;12;0
WireConnection;14;0;13;0
WireConnection;15;0;14;0
WireConnection;17;0;15;0
WireConnection;21;1;17;0
WireConnection;56;0;21;0
WireConnection;56;1;14;0
WireConnection;56;2;53;2
WireConnection;23;0;1;106
WireConnection;20;0;3;0
WireConnection;20;1;16;0
WireConnection;28;0;56;0
WireConnection;28;1;5;1
WireConnection;24;0;18;2
WireConnection;25;0;20;0
WireConnection;27;0;23;0
WireConnection;29;0;22;0
WireConnection;26;1;19;0
WireConnection;33;0;23;0
WireConnection;33;1;29;0
WireConnection;33;2;30;0
WireConnection;33;3;24;0
WireConnection;33;4;28;0
WireConnection;34;0;28;0
WireConnection;34;1;26;0
WireConnection;32;0;27;0
WireConnection;32;1;25;0
WireConnection;32;2;31;0
WireConnection;37;0;1;0
WireConnection;37;1;34;0
WireConnection;36;0;33;0
WireConnection;36;1;32;0
WireConnection;41;0;28;0
WireConnection;40;0;20;0
WireConnection;39;0;37;0
WireConnection;39;1;33;0
WireConnection;39;2;36;0
WireConnection;45;0;44;0
WireConnection;45;1;44;0
WireConnection;38;3;45;0
WireConnection;43;0;35;0
WireConnection;43;1;1;0
WireConnection;0;0;1;106
WireConnection;0;2;43;0
WireConnection;0;13;39;0
WireConnection;0;11;1;17
ASEEND*/
//CHKSM=FBAF679C2E6E4ADE45A1C151C40F75A361F0DA38