// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tabasco/WorldYGradient"
{
	Properties
	{
		_minHeight("minHeight", Float) = 0
		_BottomColor("Bottom Color", Color) = (0,0,0,0)
		_TopColor("Top Color", Color) = (1,1,1,0)
		_maxHeight("maxHeight", Float) = 1
		[HDR]_EmissionColor("Emission Color", Color) = (0,0,0,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float4 _BottomColor;
		uniform float4 _TopColor;
		uniform float _minHeight;
		uniform float _maxHeight;
		uniform float4 _EmissionColor;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float4 lerpResult7 = lerp( _BottomColor , _TopColor , saturate( (0.0 + (ase_worldPos.y - _minHeight) * (1.0 - 0.0) / (_maxHeight - _minHeight)) ));
			o.Albedo = lerpResult7.rgb;
			o.Emission = ( lerpResult7 * _EmissionColor ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
-2;868;2079;468;2202.47;203.3098;1.67;True;True
Node;AmplifyShaderEditor.WorldPosInputsNode;2;-1225.5,-16;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;4;-1218.5,150;Float;False;Property;_minHeight;minHeight;0;0;Create;True;0;0;False;0;0;163.8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-1202.5,294;Float;False;Property;_maxHeight;maxHeight;3;0;Create;True;0;0;False;0;1;311.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;3;-857.5,134;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;6;-559.55,206.37;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;8;-715.5,-152;Float;False;Property;_BottomColor;Bottom Color;1;0;Create;True;0;0;False;0;0,0,0,0;0.7169812,0.01014594,0.5012249,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;9;-710.23,22.66;Float;False;Property;_TopColor;Top Color;2;0;Create;True;0;0;False;0;1,1,1,0;0.004271993,0.744088,0.9056604,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;10;-693.9001,388.4001;Float;False;Property;_EmissionColor;Emission Color;4;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0.9622642,0.9622642,0.9622642,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;7;-359.9501,135;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-166.44,322.3802;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Tabasco/WorldYGradient;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;2;2
WireConnection;3;1;4;0
WireConnection;3;2;5;0
WireConnection;6;0;3;0
WireConnection;7;0;8;0
WireConnection;7;1;9;0
WireConnection;7;2;6;0
WireConnection;11;0;7;0
WireConnection;11;1;10;0
WireConnection;0;0;7;0
WireConnection;0;2;11;0
ASEEND*/
//CHKSM=015D50108630559742BEB2A5EFFC7952B689FD96