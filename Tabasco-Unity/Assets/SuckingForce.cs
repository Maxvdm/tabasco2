﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuckingForce : MonoBehaviour
{
    public float force;
    public float forceRadius;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        other.attachedRigidbody.AddExplosionForce(-force, transform.position, forceRadius, 0f, ForceMode.Force);
    }
}
